<?php

namespace Modules\Api\ServicesDb\Branch\Repositories;

use App\Models\Branch\Branch;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\Api\ServicesDb\Branch\Repositories\BranchRepositoryInterface;

/**
 * Class EloquentBranchRepository.
 */
class EloquentBranchRepository implements BranchRepositoryInterface
{
  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection
  {
    return Branch::where('use_shop', '=', true)->get($columns);
  }
}
