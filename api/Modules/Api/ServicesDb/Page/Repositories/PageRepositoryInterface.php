<?php

namespace Modules\Api\ServicesDb\Page\Repositories;

use App\Models\Page\Page;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface PageRepositoryInterface
 * @package Modules\Api\ServicesDb\Page\Repositories
 */
interface PageRepositoryInterface
{
  /**
   * @param  array  $params
   * @return Collection
   */
  public function getAll(array $params = []): Collection;

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'type' => string,
   *      ],
   * ]
   * @return Page
   */
  public function one(array $params = []): Page;
}
