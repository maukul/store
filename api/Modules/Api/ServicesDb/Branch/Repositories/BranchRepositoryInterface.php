<?php

namespace Modules\Api\ServicesDb\Branch\Repositories;

use Illuminate\Support\Collection;

/**
 * Interface BranchRepositoryInterface
 * @package Modules\Api\ServicesDb\Branch\Repositories
 */
interface BranchRepositoryInterface
{
  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection;
}
