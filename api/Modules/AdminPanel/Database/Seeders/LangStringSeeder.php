<?php

namespace Modules\AdminPanel\Database\Seeders;

use App\Models\LangString\LangString;
use Illuminate\Database\Seeder;

class LangStringSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    LangString::firstOrCreate(['system_name' => 'test'], [
      'system_name' => 'test',
      'ru' => ['value' => 'test'],
      'uk' => ['value' => 'test'],
      'en' => ['value' => 'test'],
    ]);
  }
}
