<?php

namespace Modules\AdminPanel\ServicesDb\Admin;

use App\Models\Admin;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use Modules\AdminPanel\ServicesDb\Admin\Repositories\AdminRepositoryInterface;

class AdminService
{
  /**
   * @var AdminRepositoryInterface
   */
  protected $adminRepository;

  /**
   * AdminService constructor.
   * @param  AdminRepositoryInterface  $adminRepository
   */
  public function __construct(
    AdminRepositoryInterface $adminRepository
  ) {
    $this->adminRepository = $adminRepository;
  }

  /**
   * Get in tables admins.
   * @param  array  $params
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    return $this->adminRepository->search(
      array_merge(
        $params,
        []
      )
    );
  }

  /**
   * Get admin by id.
   *
   * @param $id
   * @return Admin
   */
  public function getById($id): Admin
  {
    return $this->adminRepository->getById($id);
  }

  /**
   * Get admin all.
   *
   * @param $id
   * @return Admin
   */
  public function getAll(array $column = ['*']): Admin
  {
    return $this->adminRepository->getAll($column);
  }

  /**
   * Store to DB if there are no errors.
   *
   * @param  array  $data
   * @return  Admin|array
   */
  public function create($data): Admin|array
  {
    $result = [];
    try {
      $create = [
        'name' => $data['name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password']),
      ];

      $result = $this->adminRepository->create($create, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to create admin data');
      }
    }

    return $result;
  }

  /**
   * Update admin data
   * Store to DB if there are no errors.
   *
   * @param  array  $data
   * @return bool
   */
  public function update($id, $data): bool
  {
    $admin = false;
    DB::beginTransaction();
    try {
      $update = [
        'name' => $data['name'],
        'email' => $data['email'],
      ];
      if (!empty($data['password'])) {
        $update['password'] = Hash::make($data['password']);
      }

      $admin = $this->adminRepository->update($id, $update, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to update admin data');
      }
    }

    DB::commit();

    return $admin;
  }

  /**
   * Delete admin by id.
   *
   * @param $id
   * @return bool
   */
  public function deleteById($id): bool
  {
    $admin = false;
    DB::beginTransaction();

    try {
      $admin = $this->adminRepository->deleteById($id);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to delete admin data');
      }
    }

    DB::commit();

    return $admin;
  }
}
