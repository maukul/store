<?php

namespace Modules\AdminPanel\ServicesDb\LangString\Repositories;

use App\Models\LangString\LangString;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Modules\AdminPanel\ServicesDb\LangString\Repositories\LangStringRepositoryInterface;
use Throwable;

/**
 * Class EloquentLangStringRepository.
 */
class EloquentLangStringRepository implements LangStringRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    $sortType = $params['sort']['type'] ?? 'desc';
    if (!in_array($sortType, ['asc', 'desc'])) {
      $sortType = 'asc';
    }
    return LangString::where(function ($query) use ($params) {
      foreach ($params['columnFilters'] ?? [] as $name => $value) {
        $query->where($name, 'like', '%'.$value.'%');
      }
    })->orderBy($params['sort']['field'] ?? 'system_name', $sortType)->paginate($params['perPage'] ?? 10);
  }

  /**
   * @param  int  $id
   * @return LangString
   */
  public function getById(int $id): LangString
  {
    return LangString::findOrFail($id);
  }

  /**
   * @param  array|string[]  $columns
   * @return LangString
   */
  public function getAll(array $columns = ['*']): LangString
  {
    return LangString::get($columns);
  }

  /**
   * @param  array  $create
   * @param  array  $data
   * @return LangString
   * @throws Throwable
   */
  public function create(array $create, array $data = []): LangString
  {
    return DB::transaction(function () use ($create, $data) {
      $model = LangString::create($create);

      foreach (config('translatable.locales') as $locale) {
        if (!empty($data['image'][$locale])) {
          $model->addMedia($data['image'][$locale])
            ->sanitizingFileName(function ($fileName) {
              return translitFileName($fileName);
            })
            ->toMediaCollection('main-'.$locale, 'media');
        }
      }

      return $model;
    });
  }

  /**
   * @param $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update($id, array $update, array $data): bool
  {
    $model = LangString::findOrFail($id);
    $model->update($update);

    foreach (config('translatable.locales') as $locale) {
      if (!empty($data['image'][$locale])) {
        $model->clearMediaCollection('main-'.$locale);
        $model->addMedia($data['image'][$locale])
          ->sanitizingFileName(function ($fileName) {
            return translitFileName($fileName);
          })
          ->toMediaCollection('main-'.$locale, 'media');
      }
    }

    return $model->save();
  }

  /**
   * @param  int  $id
   * @return bool
   * @throws Exception
   */
  public function deleteById($id): bool
  {
    $model = LangString::findOrFail($id);
    $model->delete();

    return true;
  }
}
