<?php

namespace Modules\Api\ServicesDb\Product\Repositories;

use App\Models\Product\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface ProductRepositoryInterface
 * @package Modules\Api\ServicesDb\Product\Repositories
 */
interface ProductRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'id' => int,
   *          'branch' => int,
   *          'types' => array,
   *          'name' => string,
   *      ],
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator;

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'id' => int,
   *          'branch' => int,
   *          'types' => array,
   *          'name' => string,
   *      ],
   * ]
   * @return array
   */
  public function count(array $params = []): array;

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'branch' => int,
   *          'slug' => string,
   *      ],
   * ]
   * @return Product
   */
  public function slug(array $params = []): Product;

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'branch' => int,
   *          'types' => array,
   *      ],
   * ]
   * @return Collection
   */
  public function type(array $params = []): Collection;
}
