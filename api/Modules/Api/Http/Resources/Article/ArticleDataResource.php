<?php

namespace Modules\Api\Http\Resources\Article;

use App\Models\Article\Article;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleDataResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $article Article */
    $article = $this->resource;

    $imageThumbMedia = $article->getFirstMedia('image_thumb', 'thumb');
    $imageThumb = [
      'url' => $imageThumbMedia->getFullUrl(),
    ];

    return [
      'id' => $article->id,
      'name' => $article->name,
      'slug' => $article->slug,
      'image' => $imageThumb,
    ];
  }
}
