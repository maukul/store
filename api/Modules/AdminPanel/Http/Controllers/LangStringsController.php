<?php

namespace Modules\AdminPanel\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response as InertiaResponse;
use Modules\AdminPanel\Http\Requests\LangStrings\LangStringsEditRequest;
use Illuminate\Http\RedirectResponse;
use Modules\AdminPanel\Http\Resources\LangString\LangStringResource;
use Modules\AdminPanel\ServicesDb\LangString\LangStringService;
use Route;

class LangStringsController extends BaseController
{
  /**
   * @var LangStringService
   */
  protected $langStrings;

  /**
   * LangStringsController constructor.
   *
   * @param  LangStringService  $langStrings
   */
  public function __construct(
    LangStringService $langStrings
  ) {
    parent::__construct();
    $this->langStrings = $langStrings;

    $this->seo()->setTitle(trans('adminpanel::lang_strings.title.'.(optional(Route::getCurrentRoute())->getActionMethod() ?? 'general')));
  }

  /**
   * Display a listing of the resource.
   *
   * @param  Request  $request
   * @return InertiaResponse
   */
  public function index(Request $request): InertiaResponse
  {
    $langStringsData = $this->langStrings->search($request->all());
    $tableData = LangStringResource::collection($langStringsData);
    return Inertia::render('LangStrings/Index', [
      'tableData' => $tableData,
      'request' => $request->all(),
      'tableParams' => [
        'columnFilters' => $request->input('columnFilters', []),
        'sort' => $request->input('sort', ['field' => '', 'type' => '']),
        'page' => $tableData->resource->currentPage() ?? 1,
        'perPage' => $tableData->resource->perPage() ?? 10,
      ],
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param $id
   * @return InertiaResponse
   */
  public function edit($id): InertiaResponse
  {
    $langString = $this->langStrings->getById((int) $id);
    $langStringData = new LangStringResource($langString);
    return Inertia::render('LangStrings/Edit', [
      'data' => $langStringData,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  LangStringsEditRequest  $request
   * @param $id
   * @return RedirectResponse
   */
  public function update(LangStringsEditRequest $request, $id): RedirectResponse
  {
    $this->langStrings->update((int) $id, $request->getFormData());
    return redirect()->back()->with('message', trans('adminpanel::lang_strings.message.success.update'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param $id
   * @return RedirectResponse
   */
  public function destroy($id): RedirectResponse
  {
    $this->langStrings->deleteById($id);
    return redirect()->route('admin-panel.lang-strings.index')
      ->with('message', trans('adminpanel::lang_strings.message.success.destroy'));
  }
}
