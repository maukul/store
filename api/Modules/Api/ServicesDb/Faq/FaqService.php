<?php

namespace Modules\Api\ServicesDb\Faq;

use Illuminate\Support\Collection;
use Modules\Api\ServicesDb\Faq\Repositories\FaqRepositoryInterface;

class FaqService
{
  /**
   * @var FaqRepositoryInterface
   */
  protected $faqRepository;

  /**
   * FaqService constructor.
   * @param  FaqRepositoryInterface  $faqRepository
   */
  public function __construct(
    FaqRepositoryInterface $faqRepository
  ) {
    $this->faqRepository = $faqRepository;
  }

  /**
   * Get faq all.
   *
   * @param  array  $params  = [
   *      'sort' => string
   * ]
   * @param  array|string[]  $column
   * @return Collection
   */
  public function getAll(array $params, array $column = ['*']): Collection
  {
    return $this->faqRepository->getAll($params, $column);
  }
}
