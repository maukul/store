<?php

namespace Modules\Api\Http\Resources\Product;

use App\Models\Product\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductDataResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $product Product */
    $product = $this->resource;

    return [
      'name' => $product->name,
      'slug' => $product->slug,
      'serial_number' => $product->serial_number,
      'vacation' => (boolean) $product->vacation,
      'image' => [
        'url_thumb' => $product->getFirstMediaUrl('main', 'thumb'),
        'url_main' => $product->getFirstMediaUrl('main', 'main'),
        'url_original' => $product->getFirstMediaUrl('main', 'original'),
      ],
      'brand' => [
        'name' => $product->brand->name,
        'slug' => '', // TODO brand slug
      ],
      'min_count' => rand(1, 100)
    ];
  }
}
