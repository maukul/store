<?php

namespace Modules\AdminPanel\ServicesDb\SeoPage;

use App\Models\SeoPage\SeoPage;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Modules\AdminPanel\ServicesDb\SeoPage\Repositories\SeoPageRepositoryInterface;

class SeoPageService
{
  /**
   * @var SeoPageRepositoryInterface
   */
  protected $seoPageRepository;

  /**
   * SeoPageService constructor.
   * @param  SeoPageRepositoryInterface  $seoPageRepository
   */
  public function __construct(
    SeoPageRepositoryInterface $seoPageRepository
  ) {
    $this->seoPageRepository = $seoPageRepository;
  }

  /**
   * Get in tables seoPages.
   *
   * @param  array  $params
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    return $this->seoPageRepository->search(
      array_merge(
        $params,
        []
      )
    );
  }

  /**
   * Get seoPage by id.
   *
   * @param  int  $id
   * @return SeoPage
   */
  public function getById(int $id): SeoPage
  {
    return $this->seoPageRepository->getById($id);
  }

  /**
   * Get seoPage all.
   *
   * @param  array|string[]  $column
   * @return SeoPage
   */
  public function getAll(array $column = ['*']): SeoPage
  {
    return $this->seoPageRepository->getAll($column);
  }

  /**
   * Store to DB if there are no errors.
   *
   * @param  array  $data
   * @return SeoPage
   */
  public function create($data): SeoPage
  {
    try {
      $create = [
        'system_name' => Str::slug($data['system_name'], '_'),
      ];
      foreach (config('translatable.locales') as $locale) {
        $create[$locale]['name'] = $data['name'][$locale];
        $create[$locale]['title'] = $data['title'][$locale];
        $create[$locale]['keywords'] = $data['keywords'][$locale];
        $create[$locale]['description'] = $data['description'][$locale];
      }

      $result = $this->seoPageRepository->create($create, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to create seoPage data');
      }
    }

    return $result;
  }

  /**
   * Update seoPage data
   * Store to DB if there are no errors.
   *
   * @param $id
   * @param $data
   * @return bool
   */
  public function update($id, $data): bool
  {
    $seoPage = false;
    DB::beginTransaction();

    try {
      $update = [
        'system_name' => Str::slug($data['system_name'], '_'),
      ];
      foreach (config('translatable.locales') as $locale) {
        $update[$locale]['name'] = $data['name'][$locale];
        $update[$locale]['title'] = $data['title'][$locale];
        $update[$locale]['keywords'] = $data['keywords'][$locale];
        $update[$locale]['description'] = $data['description'][$locale];
      }

      $seoPage = $this->seoPageRepository->update($id, $update, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to update seoPage data');
      }
    }

    DB::commit();

    return $seoPage;
  }

  /**
   * Delete seoPage by id.
   *
   * @param $id
   * @return bool
   */
  public function deleteById($id): bool
  {
    $seoPage = false;
    DB::beginTransaction();

    try {
      $seoPage = $this->seoPageRepository->deleteById($id);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to delete seoPage data');
      }
    }

    DB::commit();

    return $seoPage;
  }
}
