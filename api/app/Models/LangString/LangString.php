<?php

namespace App\Models\LangString;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LangString\LangString
 *
 * @property int $id
 * @property string $system_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\LangString\LangStringTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LangString\LangStringTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|LangString listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|LangString newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LangString newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LangString notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|LangString orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|LangString orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|LangString orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|LangString query()
 * @method static \Illuminate\Database\Eloquent\Builder|LangString translated()
 * @method static \Illuminate\Database\Eloquent\Builder|LangString translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|LangString whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LangString whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LangString whereSystemName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LangString whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|LangString whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|LangString whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LangString withTranslation()
 * @mixin \Eloquent
 */
class LangString extends Model implements TranslatableContract
{
  use Translatable;

  /**
   * The translated attributes that are mass assignable.
   *
   * @var array
   */
  public $translatedAttributes = [
    'value',
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'lang_strings';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'system_name',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'system_name' => 'string',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Get hash table.
   *
   * @return string
   */
  public static function checksum(): string
  {
    return \DB::table('lang_string_translations')->select(\DB::raw("md5(coalesce(md5('value'::text), ' ')) as hash"))->first()->hash;
  }

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}

