<?php

namespace Modules\Api\ServicesDb\Article\Repositories;

use App\Models\Article\Article;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Modules\Api\ServicesDb\Article\Repositories\ArticleRepositoryInterface;
use App;

/**
 * Class EloquentArticleRepository.
 */
class EloquentArticleRepository implements ArticleRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'cat_id' => int,
   *      ],
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    $modelArticle = Article::query();

    $filters = $params['filters'] ?? [];
    if (!empty($filters)) {
      if (!empty($filters['cat_id'])) {
        $modelArticle->whereHas('categories', function ($query) use ($filters) {
          $query->where('category_id', $filters['cat_id']);
        });
      }
    }

    $modelArticle->orderByDesc('created_at');

    return $modelArticle->paginate(16);
  }

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'slug' => string,
   *      ],
   * ]
   * @return Article
   */
  public function one(array $params = []): Article
  {
    $modelArticle = Article::query();

    $filters = $params['filters'] ?? [];
    if (!empty($filters)) {
      if (!empty($filters['slug'])) {
        $modelArticle->whereTranslationLike('slug', $filters['slug'], App::getLocale());
      }
    }

    return $modelArticle->first();
  }
}
