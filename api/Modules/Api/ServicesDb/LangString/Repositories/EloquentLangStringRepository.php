<?php

namespace Modules\Api\ServicesDb\LangString\Repositories;

use App\Models\LangString\LangString;
use Illuminate\Support\Collection;
use Modules\Api\ServicesDb\LangString\Repositories\LangStringRepositoryInterface;

/**
 * Class EloquentLangStringRepository.
 */
class EloquentLangStringRepository implements LangStringRepositoryInterface
{
  /**
   * @return string
   */
  public function hash(): string
  {
    return LangString::checksum();
  }

  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection
  {
    return LangString::get($columns);
  }
}
