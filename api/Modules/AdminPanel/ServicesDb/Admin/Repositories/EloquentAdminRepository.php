<?php

namespace Modules\AdminPanel\ServicesDb\Admin\Repositories;

use App\Models\Admin;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Modules\AdminPanel\ServicesDb\Admin\Repositories\AdminRepositoryInterface;
use Throwable;

/**
 * Class EloquentAdminRepository.
 */
class EloquentAdminRepository implements AdminRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    $sortType = $params['sort']['type'] ?? 'desc';
    if (!in_array($sortType, ['asc', 'desc'])) {
      $sortType = 'asc';
    }
    return Admin::where(function ($query) use ($params) {
      foreach ($params['columnFilters'] ?? [] as $name => $value) {
        $query->where($name, 'like', '%'.$value.'%');
      }
    })->orderBy($params['sort']['field'] ?? 'name', $sortType)->paginate($params['perPage'] ?? 10, [
      'id',
      'name',
      'email',
      'created_at',
    ]);
  }

  /**
   * @param  int  $id
   * @return Admin
   */
  public function getById(int $id): Admin
  {
    return Admin::findOrFail($id);
  }

  /**
   * @param  array|string[]  $columns
   * @return Admin
   */
  public function getAll(array $columns = ['*']): Admin
  {
    return Admin::get($columns);
  }

  /**
   * @param  array  $create
   * @param  array  $data
   * @return Admin
   * @throws Throwable
   */
  public function create(array $create, array $data = []): Admin
  {
    return DB::transaction(function () use ($create) {
      return Admin::create($create);
    });
  }

  /**
   * @param  int  $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update(int $id, array $update, array $data): bool
  {
    $model = Admin::findOrFail($id);
    $model->update($update);

    return true;
  }

  /**
   * @param  int  $id
   * @return bool
   */
  public function deleteById(int $id): bool
  {
    $model = Admin::findOrFail($id);

    return $model->delete();
  }
}
