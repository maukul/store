<?php

namespace Modules\AdminPanel\Http\Resources\Faq;

use App\Models\Faq\Faq;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FaqResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $faq Faq */
    $faq = $this->resource;

    return [
      'id' => $faq->id,
      'name' => $faq->name,
      'translations' => $faq->getTranslationsArray(),
      'date' => $faq->created_at->format('d.m.Y'),
    ];
  }
}
