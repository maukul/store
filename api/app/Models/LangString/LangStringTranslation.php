<?php

namespace App\Models\LangString;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LangString\LangStringTranslation
 *
 * @property int $id
 * @property int $lang_string_id
 * @property string $locale
 * @property string $value
 * @method static \Illuminate\Database\Eloquent\Builder|LangStringTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LangStringTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LangStringTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|LangStringTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LangStringTranslation whereLangStringId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LangStringTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LangStringTranslation whereValue($value)
 * @mixin \Eloquent
 */
class LangStringTranslation extends Model
{
  /**
   * The timestamps.
   *
   * @var bool
   */
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'lang_string_translations';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'value',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'value' => 'string',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}

