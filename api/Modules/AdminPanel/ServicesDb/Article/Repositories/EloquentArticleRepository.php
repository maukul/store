<?php

namespace Modules\AdminPanel\ServicesDb\Article\Repositories;

use App\Models\Article\Article;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\AdminPanel\ServicesDb\Article\Repositories\ArticleRepositoryInterface;
use Throwable;

/**
 * Class EloquentArticleRepository.
 */
class EloquentArticleRepository implements ArticleRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    $sortType = $params['sort']['type'] ?? 'desc';
    if (!in_array($sortType, ['asc', 'desc'])) {
      $sortType = 'asc';
    }
    return Article::where(function ($query) use ($params) {
      foreach ($params['columnFilters'] ?? [] as $name => $value) {
        $query->where($name, 'like', '%'.$value.'%');
      }
    })->orderByTranslation($params['sort']['field'] ?? 'name', $sortType)->paginate($params['perPage'] ?? 10);
  }

  /**
   * @param  int  $id
   * @return Article
   */
  public function getById(int $id): Article
  {
    return Article::findOrFail($id);
  }

  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection
  {
    return Article::get($columns);
  }

  /**
   * @param  array  $create
   * @param  array  $data
   * @return Article
   * @throws Throwable
   */
  public function create(array $create, array $data = []): Article
  {
    return DB::transaction(function () use ($create, $data) {
      $model = Article::create($create);
      if (!empty($data['categories'])) {
        $model->categories()->sync($data['categories']);
      }
      if (!empty($data['recommend'])) {
        $model->categories()->sync($data['recommend']);
      }

      if (!empty($data['image_thumb'])) {
        $model->addMedia($data['image_thumb'])
          ->sanitizingFileName(function ($fileName) {
            return translitFileName($fileName);
          })
          ->toMediaCollection('image_thumb', 'media');
      }
      foreach ($data['images_gallery'] as $image) {
        $model->addMedia($image)
          ->sanitizingFileName(function ($fileName) {
            return translitFileName($fileName);
          })
          ->toMediaCollection('gallery', 'media');
      }

      return $model;
    });
  }

  /**
   * @param $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update($id, array $update, array $data): bool
  {
    $model = Article::findOrFail($id);
    $model->update($update);
    if (!empty($data['categories'])) {
      $model->categories()->sync($data['categories']);
    }
    if (!empty($data['recommend'])) {
      $model->categories()->sync($data['recommend']);
    }
    if (!empty($data['image_thumb'])) {
      $model->clearMediaCollection('image_thumb');
      $model->addMedia($data['image_thumb'], 'media')
        ->sanitizingFileName(function ($fileName) {
          return translitFileName($fileName);
        })
        ->toMediaCollection('image_thumb', 'media');
    }

    foreach ($data['images_gallery'] as $image) {
      $model->addMedia($image, 'media')
        ->sanitizingFileName(function ($fileName) {
          return translitFileName($fileName);
        })
        ->toMediaCollection('gallery', 'media');
    }

    return $model->save();
  }

  /**
   * @param  int  $id
   * @return bool
   * @throws Exception
   */
  public function deleteById($id): bool
  {
    $model = Article::findOrFail($id);
    $model->delete();

    return true;
  }
}
