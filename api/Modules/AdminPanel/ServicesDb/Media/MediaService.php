<?php

namespace Modules\AdminPanel\ServicesDb\Media;

use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use Modules\AdminPanel\ServicesDb\Media\Repositories\MediaRepositoryInterface;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaService
{
  /**
   * @var MediaRepositoryInterface
   */
  protected $mediaRepository;

  /**
   * MediaService constructor.
   * @param  MediaRepositoryInterface  $mediaRepository
   */
  public function __construct(
    MediaRepositoryInterface $mediaRepository
  ) {
    $this->mediaRepository = $mediaRepository;
  }

  /**
   * Delete media by id.
   *
   * @param  int  $id
   * @return bool
   */
  public function deleteById(int $id): bool
  {
    $media = false;
    DB::beginTransaction();

    try {
      $media = $this->mediaRepository->deleteById($id);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to media media data');
      }
    }

    DB::commit();

    return $media;
  }
}
