<?php

namespace Modules\AdminPanel\Http\Requests\LangStrings;

use Modules\AdminPanel\Http\Requests\FormRequest;

class LangStringsEditRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize(): bool
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array
  {
    return [
      'system_name' => [
        'required',
        'string',
      ],
      'value.*' => [
        'required',
        'string',
        'max:200',
      ],
    ];
  }

  /**
   * Get custom messages for validator errors.
   *
   * @return array
   */
  public function messages(): array
  {
    return [];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes(): array
  {
    $return = [];

    $return['system_name'] = trans('adminpanel::lang_strings.form.system_name.title');

    foreach (config('translatable.locales') as $item) {
      $return['value.'.$item] = trans('adminpanel::lang_strings.form.value.title').' ('.$item.')';
    }
    return $return;
  }
}
