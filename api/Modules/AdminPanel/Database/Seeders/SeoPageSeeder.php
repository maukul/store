<?php

namespace Modules\AdminPanel\Database\Seeders;

use App\Models\SeoPage\SeoPage;
use Illuminate\Database\Seeder;

class SeoPageSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    SeoPage::firstOrCreate(['system_name' => 'test'], [
      'system_name' => 'test',
      'ru' => [
        'name' => 'test',
        'title' => 'test',
        'keywords' => 'test',
        'description' => 'test',
      ],
      'uk' => [
        'name' => 'test',
        'title' => 'test',
        'keywords' => 'test',
        'description' => 'test',
      ],
      'en' => [
        'name' => 'test',
        'title' => 'test',
        'keywords' => 'test',
        'description' => 'test',
      ],
    ]);
  }
}
