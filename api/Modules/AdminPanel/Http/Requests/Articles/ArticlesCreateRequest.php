<?php

namespace Modules\AdminPanel\Http\Requests\Articles;

use Modules\AdminPanel\Http\Requests\FormRequest;

class ArticlesCreateRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize(): bool
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array
  {
    $rules = [
      'name.*' => [
        'required',
        'string',
        'max:90',
      ],
      'title.*' => [
        'required',
        'string',
        'max:255',
      ],
      'keywords.*' => [
        'nullable',
        'string',
        'max:255',
      ],
      'description.*' => [
        'nullable',
        'string',
        'max:255',
      ],
      'content.*' => [
        'nullable',
        'string',
        'max:50000',
      ],
      'image_thumb' => [
        'required',
        'file'
      ],
      'images_gallery.*' => [
        'nullable',
        'file'
      ],
    ];

    return $rules;
  }

  /**
   * Get custom messages for validator errors.
   *
   * @return array
   */
  public function messages(): array
  {
    return [];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes(): array
  {
    $return = [
      'image_thumb' => trans('adminpanel::articles.form.image_thumb.title'),
      'images_gallery' => trans('adminpanel::articles.form.images_gallery.title'),
    ];

    foreach (config('translatable.locales') as $item) {
      $return['name.'.$item] = trans('adminpanel::articles.form.name.title').' ('.$item.')';
      $return['title.'.$item] = trans('adminpanel::articles.form.title.title').' ('.$item.')';
      $return['keywords.'.$item] = trans('adminpanel::articles.form.keywords.title').' ('.$item.')';
      $return['description.'.$item] = trans('adminpanel::articles.form.description.title').' ('.$item.')';
      $return['content.'.$item] = trans('adminpanel::articles.form.content.title').' ('.$item.')';
    }
    return $return;
  }
}
