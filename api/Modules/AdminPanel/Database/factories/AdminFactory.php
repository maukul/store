<?php

namespace Modules\AdminPanel\Database\factories;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AdminFactory extends Factory
{
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = Admin::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    return [
      'name' => $this->faker->name,
      'email' => $this->faker->unique()->safeEmail,
      'password' => '$2y$10kjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.u$92IXUNpheWG/igi',
      'remember_token' => Str::random(10),
    ];
  }
}
