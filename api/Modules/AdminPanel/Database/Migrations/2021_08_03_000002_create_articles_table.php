<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('articles', function (Blueprint $table) {
      // Таблиця статей блога
      $table->id();
      $table->timestamps();
      $table->softDeletes();
    });

    Schema::create('article_translations', function (Blueprint $table) {
      // Таблиця перекладів статей блога
      $table->increments('id');
      $table->bigInteger('article_id')->unsigned();
      $table->string('locale')->index();
      $table->string('name', 90)->comment('Поле назви');
      $table->string('title', 255)->comment('Поле тайтл');
      $table->string('keywords', 255)->nullable()->comment('Поле ключові слова');
      $table->string('description', 255)->nullable()->comment('Поле ключові слова');
      $table->text('content')->nullable()->comment('Поле контент');
      $table->string('slug', 90)->comment('Url категорії для для статей блога');

      $table->unique(['article_id', 'locale']);
      $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('article_translations');
    Schema::dropIfExists('articles');
  }
}
