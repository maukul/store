<?php

namespace Modules\AdminPanel\ServicesDb\Banner\Repositories;

use App\Models\Banner\Banner;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Modules\AdminPanel\ServicesDb\Banner\Repositories\BannerRepositoryInterface;
use Throwable;

/**
 * Class EloquentBannerRepository.
 */
class EloquentBannerRepository implements BannerRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    $sortType = $params['sort']['type'] ?? 'desc';
    if (!in_array($sortType, ['asc', 'desc'])) {
      $sortType = 'asc';
    }
    return Banner::where(function ($query) use ($params) {
      foreach ($params['columnFilters'] ?? [] as $name => $value) {
        $query->where($name, 'like', '%'.$value.'%');
      }
    })->orderByTranslation($params['sort']['field'] ?? 'name', $sortType)->paginate($params['perPage'] ?? 10);
  }

  /**
   * @param  int  $id
   * @return Banner
   */
  public function getById(int $id): Banner
  {
    return Banner::findOrFail($id);
  }

  /**
   * @param  array|string[]  $columns
   * @return Banner
   */
  public function getAll(array $columns = ['*']): Banner
  {
    return Banner::get($columns);
  }

  /**
   * @param  array  $create
   * @param  array  $data
   * @return Banner
   * @throws Throwable
   */
  public function create(array $create, array $data = []): Banner
  {
    return DB::transaction(function () use ($create, $data) {
      $model = Banner::create($create);

      foreach (config('translatable.locales') as $locale) {
        if (!empty($data['image'][$locale])) {
          $model->addMedia($data['image'][$locale])
            ->sanitizingFileName(function ($fileName) {
              return translitFileName($fileName);
            })
            ->toMediaCollection('main-'.$locale, 'media');
        }
      }

      return $model;
    });
  }

  /**
   * @param $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update($id, array $update, array $data): bool
  {
    $model = Banner::findOrFail($id);
    $model->update($update);

    foreach (config('translatable.locales') as $locale) {
      if (!empty($data['image'][$locale])) {
        $model->clearMediaCollection('main-'.$locale);
        $model->addMedia($data['image'][$locale])
          ->sanitizingFileName(function ($fileName) {
            return translitFileName($fileName);
          })
          ->toMediaCollection('main-'.$locale, 'media');
      }
    }

    return $model->save();
  }

  /**
   * @param  int  $id
   * @return bool
   * @throws Exception
   */
  public function deleteById($id): bool
  {
    $model = Banner::findOrFail($id);
    $model->delete();

    return true;
  }
}
