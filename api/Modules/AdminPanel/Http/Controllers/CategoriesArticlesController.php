<?php

namespace Modules\AdminPanel\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Modules\AdminPanel\Http\Requests\CategoriesArticles\CategoriesArticlesCreateRequest;
use Modules\AdminPanel\Http\Requests\CategoriesArticles\CategoriesArticlesEditRequest;
use Inertia\Response as InertiaResponse;
use Illuminate\Http\RedirectResponse;
use Modules\AdminPanel\Http\Resources\CategoriesArticles\CategoriesArticlesResource;
use Modules\AdminPanel\ServicesDb\CategoriesArticles\CategoriesArticlesService;
use Route;

class CategoriesArticlesController extends BaseController
{
  /**
   * @var CategoriesArticlesService
   */
  protected $categoriesArticles;

  /**
   * CategoriesArticlesController constructor.
   *
   * @param  CategoriesArticlesService  $categoriesArticles
   */
  public function __construct(
    CategoriesArticlesService $categoriesArticles
  ) {
    parent::__construct();
    $this->categoriesArticles = $categoriesArticles;

    $this->seo()->setTitle(trans('adminpanel::categories_articles.title.'.(optional(Route::getCurrentRoute())->getActionMethod() ?? 'general')));
  }

  /**
   * Display a listing of the resource.
   *
   * @param  Request  $request
   * @return InertiaResponse
   */
  public function index(Request $request): InertiaResponse
  {
    $categoriesArticlesData = $this->categoriesArticles->search($request->all());
    $tableData = CategoriesArticlesResource::collection($categoriesArticlesData);
    return Inertia::render('CategoriesArticles/Index', [
      'tableData' => $tableData,
      'request' => $request->all(),
      'tableParams' => [
        'columnFilters' => $request->input('columnFilters', []),
        'sort' => $request->input('sort', ['field' => '', 'type' => '']),
        'page' => $tableData->resource->currentPage() ?? 1,
        'perPage' => $tableData->resource->perPage() ?? 10,
      ],
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return InertiaResponse
   */
  public function create(): InertiaResponse
  {
    return Inertia::render('CategoriesArticles/Create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  CategoriesArticlesCreateRequest  $request
   * @return RedirectResponse
   */
  public function store(CategoriesArticlesCreateRequest $request): RedirectResponse
  {
    $model = $this->categoriesArticles->create($request->getFormData());
    return redirect()->route('admin-panel.categories-articles.edit', $model->id)
      ->with('message', trans('adminpanel::categories_articles.message.success.store'));
  }

  /**
   * Show the form for editing the specified resource.
   * @param $id
   * @return InertiaResponse
   */
  public function edit($id): InertiaResponse
  {
    $categoriesArticles = $this->categoriesArticles->getById((int) $id);
    $categoriesArticlesData = new CategoriesArticlesResource($categoriesArticles);
    return Inertia::render('CategoriesArticles/Edit', [
      'data' => $categoriesArticlesData,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  CategoriesArticlesEditRequest  $request
   * @param $id
   * @return RedirectResponse
   */
  public function update(CategoriesArticlesEditRequest $request, $id): RedirectResponse
  {
    $this->categoriesArticles->update((int) $id, $request->getFormData());
    return redirect()->back()->with('message', trans('adminpanel::categories_articles.message.success.update'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param $id
   * @return RedirectResponse
   */
  public function destroy($id): RedirectResponse
  {
    $this->categoriesArticles->deleteById((int) $id);
    return redirect()->route('admin-panel.categories-articles.index')
      ->with('message', trans('adminpanel::categories_articles.message.success.destroy'));
  }
}
