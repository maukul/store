<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Api\Http\Resources\Page\PageResource;
use Modules\Api\Http\Resources\Page\PagesResource;
use Modules\Api\ServicesDb\Page\PageService;

class PagesController extends BaseController
{
  /**
   * @var PageService
   */
  protected $articles;

  /**
   * PagesController constructor.
   * @param  PageService  $articles
   */
  public function __construct(
    PageService $articles
  ) {
    parent::__construct();
    $this->articles = $articles;
  }

  /**
   * @param  Request  $request
   * @return AnonymousResourceCollection
   */
  public function index(Request $request): AnonymousResourceCollection
  {
    $articlesData = $this->articles->getAll($request->all());
    return PagesResource::collection($articlesData);
  }

  /**
   * @param  Request  $request
   * @return PageResource
   */
  public function slug(Request $request): PageResource
  {
    $articlesData = $this->articles->one($request->all());
    return new PageResource($articlesData);
  }
}
