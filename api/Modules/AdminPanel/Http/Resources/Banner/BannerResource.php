<?php

namespace Modules\AdminPanel\Http\Resources\Banner;

use App\Models\Banner\Banner;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $banner Banner */
    $banner = $this->resource;

    $image = [];

    foreach (config('translatable.locales') as $locale) {
      $imageModel = $banner->getFirstMedia('main-'.$locale);
      if ($imageModel) {
        $image[$locale] = [
          'id' => $imageModel->id,
          'url' => $imageModel->getFullUrl(),
        ];
      }
    }

    return [
      'id' => $banner->id,
      'type' => $banner->type,
      'position' => $banner->position,
      'name' => $banner->name,
      'url' => $banner->url,
      'image' => $banner->getFirstMediaUrl('main-ru'),
      'imageTranslations' => $image,
      'translations' => $banner->getTranslationsArray(),
      'date' => $banner->created_at->format('d.m.Y'),
    ];
  }
}
