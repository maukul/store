<?php

namespace Modules\Api\ServicesDb\Product;

use App\Models\Product\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Modules\Api\ServicesDb\Product\Repositories\ProductRepositoryInterface;

class ProductService
{
  /**
   * @var ProductRepositoryInterface
   */
  protected $ProductRepository;

  /**
   * ProductService constructor.
   * @param  ProductRepositoryInterface  $ProductRepository
   */
  public function __construct(
    ProductRepositoryInterface $ProductRepository
  ) {
    $this->ProductRepository = $ProductRepository;
  }

  /**
   * Get Product search.
   *
   * @param  array  $params  = [
   *      'filters' => [
   *          'id' => int,
   *          'branch' => int,
   *          'types' => array,
   *          'name' => string,
   *      ],
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    return $this->ProductRepository->search($params);
  }

  /**
   * Get Product search.
   *
   * @param  array  $params  = [
   *      'filters' => [
   *          'id' => int,
   *          'branch' => int,
   *          'types' => array,
   *          'name' => string,
   *      ],
   * ]
   * @return array
   */
  public function count(array $params = []): array
  {
    return $this->ProductRepository->count($params);
  }

  /**
   * Get Product search.
   *
   * @param  array  $params  = [
   *      'filters' => [
   *          'branch' => int,
   *          'slug' => string,
   *      ],
   * ]
   * @return Product
   */
  public function slug(array $params = []): Product
  {
    return $this->ProductRepository->slug($params);
  }

  /**
   * Get Product type.
   *
   * @param  array  $params  = [
   *      'filters' => [
   *          'branch' => int,
   *          'types' => array,
   *      ],
   * ]
   * @return Collection
   */
  public function type(array $params = []): Collection
  {
    return $this->ProductRepository->type($params);
  }
}
