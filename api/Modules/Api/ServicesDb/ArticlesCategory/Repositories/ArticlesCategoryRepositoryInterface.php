<?php

namespace Modules\Api\ServicesDb\ArticlesCategory\Repositories;

use App\Models\CategoriesArticles\CategoriesArticles;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface ArticlesCategoryRepositoryInterface
 * @package Modules\Api\ServicesDb\ArticlesCategory\Repositories
 */
interface ArticlesCategoryRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => string,
   * ]
   * @return Collection
   */
  public function getAll(array $params = []): Collection;

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'type' => string,
   *      ],
   * ]
   * @return CategoriesArticles
   */
  public function slug(array $params = []): CategoriesArticles;
}
