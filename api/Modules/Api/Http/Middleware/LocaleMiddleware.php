<?php

namespace Modules\Api\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class LocaleMiddleware
{
  /**
   * Handle an incoming request.
   *
   * @param  Request  $request
   * @param  Closure  $next
   * @return mixed
   */
  public function handle(Request $request, Closure $next)
  {
    $requestLocale = $request->input('locale', 'uk');
    $requestLocale = str_replace('ua', 'uk', $requestLocale);
    if (in_array($requestLocale, config('translatable.locales'))) {
      App::setLocale($requestLocale);
    }
    $request->route()->forgetParameter('locale');

    return $next($request);
  }
}
