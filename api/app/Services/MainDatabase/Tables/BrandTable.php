<?php

namespace App\Services\MainDatabase\Tables;

use App\Services\MainDatabase\BaseMainDatabase;
use Illuminate\Support\Facades\Http;

/**
 * Class BrandTable
 * @package App\Services\MainDatabase\Tables
 */
final class BrandTable extends BaseMainDatabase
{
  /**
   * BrandTable constructor.
   */
  public function __construct()
  {
    $this->select();
  }

  /**
   * The function select
   *
   * @param  array  $params
   * @return bool
   */
  protected function select(array $params = []): bool
  {
    $response = Http::withBasicAuth(config('main_database.username'), config('main_database.password'))
      ->get(config('main_database.url').'statement=select kodkli, inet_shop, ntm ,ru_name, eng_name, ua_name, fix, main from dc000091');

    $this->originalDate = $response->json()['d']['results'];
    $data = [];
    foreach ($response->json()['d']['results'] as $item) {
      $return = [];
      $return['out_id'] = $item['kodkli'];

      $return['show'] = $item['inet_shop'] == 1;
      $return['show_label'] = $item['ntm'] == 1;
      $return['main_position'] = $item['fix'];
      $return['show_main'] = $item['main'];

      if (!empty($item['ua_name'])) {
        $return['translations']['uk']['name'] = $item['ua_name'];
      }
      if (!empty($item['ru_name'])) {
        $return['translations']['ru']['name'] = $item['ru_name'];
      }
      if (!empty($item['eng_name'])) {
        $return['translations']['en']['name'] = $item['eng_name'];
      }

      if (!empty($return['translations'])) {
        $data[] = $return;
      }
    }
    $this->data = $data;
    return true;
  }
}
