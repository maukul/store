<?php

namespace App\Services\MainDatabase;

/**
 * Class BaseMainDatabase
 * @package App\Service\MainDatabase
 */
abstract class BaseMainDatabase
{
  /**
   * The original date
   *
   * @var array
   */
  protected $originalDate = [];
  /**
   * The date
   *
   * @var array
   */
  protected $data = [];
  /**
   * The params
   *
   * @var array
   */
  protected $params = [];

  /**
   * The get original date
   *
   * @return array
   */
  public function getOriginalDate(): array
  {
    return $this->originalDate;
  }

  /**
   * The get date
   *
   * @return array
   */
  public function getData(): array
  {
    return $this->data;
  }

  /**
   * The get params
   *
   * @return array
   */
  public function getParams(): array
  {
    return $this->params;
  }

  /**
   * The function select
   *
   * @param  array  $params
   */
  protected function select(array $params = []): bool
  {
    return false;
  }
}
