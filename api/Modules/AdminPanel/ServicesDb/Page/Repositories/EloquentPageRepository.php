<?php

namespace Modules\AdminPanel\ServicesDb\Page\Repositories;

use App\Models\Page\Page;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Modules\AdminPanel\ServicesDb\Page\Repositories\PageRepositoryInterface;
use Throwable;

/**
 * Class EloquentPageRepository.
 */
class EloquentPageRepository implements PageRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    $sortType = $params['sort']['type'] ?? 'desc';
    if (!in_array($sortType, ['asc', 'desc'])) {
      $sortType = 'asc';
    }
    return Page::where(function ($query) use ($params) {
      foreach ($params['columnFilters'] ?? [] as $name => $value) {
        $query->where($name, 'like', '%'.$value.'%');
      }
    })->orderByTranslation($params['sort']['field'] ?? 'name', $sortType)->paginate($params['perPage'] ?? 10);
  }

  /**
   * @param  int  $id
   * @return Page
   */
  public function getById(int $id): Page
  {
    return Page::findOrFail($id);
  }

  /**
   * @param  array|string[]  $columns
   * @return Page
   */
  public function getAll(array $columns = ['*']): Page
  {
    return Page::get($columns);
  }

  /**
   * @param  array  $create
   * @param  array  $data
   * @return Page
   * @throws Throwable
   */
  public function create(array $create, array $data = []): Page
  {
    return DB::transaction(function () use ($create, $data) {
      $model = Page::create($create);

      if ($data['type'] == 'about') {
        foreach ($data['images_gallery'] as $image) {
          $model->addMedia($image, 'media')
            ->sanitizingFileName(function ($fileName) {
              return translitFileName($fileName);
            })
            ->toMediaCollection('gallery', 'media');
        }
      }

      return $model;
    });
  }

  /**
   * @param $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update($id, array $update, array $data): bool
  {
    $model = Page::findOrFail($id);
    $model->update($update);

    if ($data['type'] == 'about') {
      foreach ($data['images_gallery'] as $image) {
        $model->addMedia($image, 'media')
          ->sanitizingFileName(function ($fileName) {
            return translitFileName($fileName);
          })
          ->toMediaCollection('gallery', 'media');
      }
    }

    return $model->save();
  }

  /**
   * @param  int  $id
   * @return bool
   * @throws Exception
   */
  public function deleteById($id): bool
  {
    $model = Page::findOrFail($id);
    $model->delete();

    return true;
  }
}
