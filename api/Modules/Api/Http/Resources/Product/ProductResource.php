<?php

namespace Modules\Api\Http\Resources\Product;

use App\Models\Product\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $product Product */
    $product = $this->resource;

    if ($request->has('filters.branch')) {
      $leftovers = $product->branchLeftovers()->where('branch_id', '=',
        $request->input('filters.branch'))->first();
      $totalRemainder = $leftovers->total_remainder ?? 0;
    } else {
      $totalRemainder = $product->branchLeftovers->sum('total_remainder');
    }

    $result = [
      'id' => $product->id,
      'name' => $product->name,
      'slug' => $product->slug,
      'serial_number' => $product->serial_number,
      'vacation' => (boolean) $product->vacation,
      'image' => [
        'url_thumb' => $product->getFirstMediaUrl('main', 'thumb'),
        'url_main' => $product->getFirstMediaUrl('main', 'main'),
        'url_original' => $product->getFirstMediaUrl('main', 'original'),
      ],
      'brand' => [
        'name' => $product->brand->name,
        'slug' => '', // TODO brand slug
      ],
      'leftover' => [
        'total_remainder' => $totalRemainder,
      ],
      'min_count' => rand(1, 100)
    ];

    if ($request->has('relationships.options')) {
      $result['relationships']['options']['data'] = ProductDataResource::collection($product->options()->limit(16)->get());
      $result['relationships']['options']['meta']['count'] = $product->options()->count();
    }
    if ($request->has('relationships.analogs')) {
      $result['relationships']['analogs']['data'] = ProductDataResource::collection($product->analogs()->limit(16)->get());
      $result['relationships']['analogs']['meta']['count'] = $product->options()->count();
    }
    if ($request->has('relationships.related')) {
      $result['relationships']['related']['data'] = ProductDataResource::collection($product->related()->limit(16)->get());
      $result['relationships']['related']['meta']['count'] = $product->options()->count();
    }

    return $result;
  }
}
