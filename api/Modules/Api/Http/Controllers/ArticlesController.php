<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Api\Http\Resources\Article\ArticleResource;
use Modules\Api\ServicesDb\Article\ArticleService;

class ArticlesController extends BaseController
{
  /**
   * @var ArticleService
   */
  protected $articles;

  /**
   * ArticlesController constructor.
   * @param  ArticleService  $articles
   */
  public function __construct(
    ArticleService $articles
  ) {
    parent::__construct();
    $this->articles = $articles;
  }

  /**
   * @param  Request  $request
   * @return AnonymousResourceCollection
   */
  public function index(Request $request): AnonymousResourceCollection
  {
    $articlesData = $this->articles->search($request->all());
    return ArticleResource::collection($articlesData);
  }

  /**
   * @param  Request  $request
   * @return ArticleResource
   */
  public function slug(Request $request): ArticleResource
  {
    $articlesData = $this->articles->one($request->all());
    return new ArticleResource($articlesData);
  }
}
