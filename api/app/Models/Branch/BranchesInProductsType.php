<?php

namespace App\Models\Branch;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Branch\BranchesInProductsType
 *
 * @property int $id
 * @property int $product_id Поле яке посилається на товар в головній базі аналог procedure [IM_main_top,IM_main_season,IM_main_new,IM_main_again,IM_main_sale](filial).kodm
 * @property int $branch_id Поле яке посилається на філіал в головній базі аналог procedure [IM_main_top,IM_main_season,IM_main_new,IM_main_again,IM_main_sale](filial).filial
 * @property string $type Поле тип товару відповідно до процедури  [IM_main_top,IM_main_season,IM_main_new,IM_main_again,IM_main_sale]
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Branch\Branch|null $branch
 * @property-read Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsType query()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsType whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsType whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsType whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BranchesInProductsType extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'branches_in_products_types';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_id',
    'branch_id',
    'type',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'product_id' => 'integer',
    'branch_id' => 'integer',
    'type' => 'string',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /**
   * @return HasOne<Product>|Product
   */
  public function product(): HasOne
  {
    return $this->hasOne(Product::class);
  }

  /**
   * @return HasOne<Branch>|Branch
   */
  public function branch(): HasOne
  {
    return $this->hasOne(Branch::class);
  }


  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
