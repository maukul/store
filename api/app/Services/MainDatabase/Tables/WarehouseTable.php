<?php

namespace App\Services\MainDatabase\Tables;

use App\Models\Branch\Branch;
use App\Services\MainDatabase\BaseMainDatabase;
use Illuminate\Support\Facades\Http;

/**
 * Class WarehouseTable
 * @package App\Services\MainDatabase\Tables
 */
final class WarehouseTable extends BaseMainDatabase
{
  /**
   * WarehouseTable constructor.
   */
  public function __construct()
  {
    $this->select();
  }

  /**
   * @param  array  $params
   * @return bool
   */
  protected function select(array $params = []): bool
  {
    $response = Http::withBasicAuth(config('main_database.username'), config('main_database.password'))
      ->get(config('main_database.url').'statement=select filial,kodkli, type, vipiska from dc000128 where vipiska=1 and type=36607');

    $this->originalDate = $response->json()['d']['results'];
    $data = [];
    foreach ($response->json()['d']['results'] as $item) {
      $return = [];
      $return['out_id'] = $item['kodkli'];
      $return['type'] = $item['type'];
      $return['discharge_goods'] = $item['vipiska'];
      $branch = Branch::where('out_id', '=', $item['filial'])->first();
      $return['branch_id'] = $branch->id ?? null;
      if (!empty($return['branch_id'])) {
        $data[] = $return;
      }
    }
    $this->data = $data;
    return true;
  }
}
