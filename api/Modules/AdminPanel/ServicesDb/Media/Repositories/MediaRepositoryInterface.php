<?php

namespace Modules\AdminPanel\ServicesDb\Media\Repositories;

/**
 * Interface MediaRepositoryInterface
 * @package Modules\AdminPanel\ServicesDb\Media\Repositories
 */
interface MediaRepositoryInterface
{
  /**
   * @param  int  $id
   * @return bool
   */
  public function deleteById(int $id): bool;
}
