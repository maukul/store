<?php

namespace Modules\Api\ServicesDb\Article\Repositories;

use App\Models\Article\Article;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface ArticleRepositoryInterface
 * @package Modules\Api\ServicesDb\Article\Repositories
 */
interface ArticleRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'cat_id' => int,
   *      ],
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator;

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'slug' => string,
   *      ],
   * ]
   * @return Article
   */
  public function one(array $params = []): Article;
}
