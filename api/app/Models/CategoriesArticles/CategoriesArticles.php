<?php

namespace App\Models\CategoriesArticles;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\AdminPanel\Database\factories\CategoriesArticlesFactory;

/**
 * App\Models\CategoriesArticles\CategoriesArticles
 *
 * @property int $id
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\CategoriesArticles\CategoriesArticlesTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CategoriesArticles\CategoriesArticlesTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Modules\AdminPanel\Database\factories\CategoriesArticlesFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles query()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles translated()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticles withTranslation()
 * @mixin \Eloquent
 */
class CategoriesArticles extends Model implements TranslatableContract
{
  use HasFactory,
    Translatable;

  /**
   * The translated attributes that are mass assignable.
   *
   * @var array
   */
  public $translatedAttributes = [
    'name',
    'title',
    'keywords',
    'description',
    'slug',
  ];
  /**
   * The translated foreign key.
   *
   * @var array
   */
  public $translationForeignKey = 'category_id';
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'categories_articles';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'position',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'position' => 'integer',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Appointment factory AdminFactory
   *
   * @return CategoriesArticlesFactory
   */
  protected static function newFactory(): CategoriesArticlesFactory
  {
    return CategoriesArticlesFactory::new();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
