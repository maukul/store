<?php

namespace Modules\Api\Http\Controllers;

use Modules\Api\Http\Resources\SeoPage\SeoPageResource;
use Modules\Api\ServicesDb\SeoPage\SeoPageService;

class SeoPagesController extends BaseController
{
  /**
   * @var SeoPageService
   */
  protected $seoPages;

  /**
   * SeoPagesController constructor.
   * @param  SeoPageService  $seoPages
   */
  public function __construct(
    SeoPageService $seoPages
  ) {
    parent::__construct();
    $this->seoPages = $seoPages;
  }

  /**
   * @param $systemName
   * @return SeoPageResource
   */
  public function one($systemName): SeoPageResource
  {
    $seoPageData = $this->seoPages->getBySystemName($systemName);
    return new SeoPageResource($seoPageData);
  }
}
