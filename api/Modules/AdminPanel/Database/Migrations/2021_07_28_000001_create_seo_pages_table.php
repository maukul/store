<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeoPagesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('seo_pages', function (Blueprint $table) {
      // Таблиця сео даних для сторінок
      $table->id();
      $table->string('system_name', 25)->comment('Поле системної назви');
      $table->timestamps();
      $table->softDeletes();
    });

    Schema::create('seo_page_translations', function (Blueprint $table) {
      $table->increments('id');
      $table->bigInteger('seo_page_id')->unsigned();
      $table->string('locale')->index();
      $table->string('name', 90)->comment('Поле назви');
      $table->string('title', 255)->comment('Поле тайтл');
      $table->string('keywords', 255)->nullable()->comment('Поле ключові слова');
      $table->string('description', 255)->nullable()->comment('Поле ключові слова');

      $table->unique(['seo_page_id', 'locale']);
      $table->foreign('seo_page_id')->references('id')->on('seo_pages')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('seo_page_translations');
    Schema::dropIfExists('seo_pages');
  }
}
