import Vue from 'vue'
import {VBToggle} from 'bootstrap-vue'
import {VBTooltip} from 'bootstrap-vue'
import {VBModal} from 'bootstrap-vue'

Vue.directive('b-modal', VBModal)
Vue.directive('b-toggle', VBToggle)
Vue.directive('b-tooltip', VBTooltip)

import {BFormInput} from 'bootstrap-vue'

Vue.component('b-form-input', BFormInput)
