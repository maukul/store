<?php

namespace Modules\AdminPanel\Http\Resources\Admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AdminResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $admin Admin */
    $admin = $this->resource;

    return [
      'id' => $admin->id,
      'name' => $admin->name,
      'email' => $admin->email,
      'date' => $admin->created_at->format('d.m.Y'),
    ];
  }
}
