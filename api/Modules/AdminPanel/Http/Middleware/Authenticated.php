<?php

namespace Modules\AdminPanel\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class Authenticated
{
  /**
   * Handle an incoming request.
   *
   * @param  Request  $request
   * @param  Closure  $next
   * @return Application|RedirectResponse|Redirector|mixed
   */
  public function handle(Request $request, Closure $next)
  {
    if (!Auth::guard('admins')->check()) {
      return redirect(route('admin-panel.login'));
    }

    return $next($request);
  }
}
