<?php
return [
  'title' => [
    'general' => 'Статьи блогу',
    'index' => 'Список статей блогу',
    'create' => 'Создать статьей блогу',
    'edit' => 'Редактировать статьей блогу',
  ],
  'form' => [
    'position' => [
      'title' => 'Позиция',
      'placeholder' => 'Позиция',
    ],
    'name' => [
      'title' => 'Название',
      'placeholder' => 'Название',
    ],
    'title' => [
      'title' => 'Мета тайтл',
      'placeholder' => 'Мета тайтл',
    ],
    'keywords' => [
      'title' => 'Мета ключевые слова',
      'placeholder' => 'Мета ключевые слова',
    ],
    'description' => [
      'title' => 'Мета описание',
      'placeholder' => 'Мета описание',
    ],
    'slug' => [
      'title' => 'Url (ссылки)',
      'placeholder' => 'Url (ссылки)',
    ],
    'categories' => [
      'title' => 'Категории',
      'placeholder' => 'Категории',
    ],
    'recommend' => [
      'title' => 'Рекомендуемиє статьи',
      'placeholder' => 'Рекомендуемиє статьи',
    ],
    'content' => [
      'title' => 'Контент',
      'placeholder' => 'Контент',
    ],
    'image_thumb' => [
      'title' => 'Головне фото',
      'placeholder' => 'Головне фото',
    ],
    'images_gallery' => [
      'title' => 'Фото галереи',
      'placeholder' => 'Фото галереи',
    ],
  ],
  'table' => [
    'columns' => [
      'name' => 'Название',
    ],
  ],
  'message' => [
    'success' => [
      'store' => 'Создание статьи блогу прошло успешно.',
      'update' => 'Редактирование статьи блогу прошло успешно.',
      'destroy' => 'Удаление статьи блогу прошло успешно.',
    ],
  ],
  'text' => [
    'add_articles' => 'Добавить статьей блогу'
  ]
];
