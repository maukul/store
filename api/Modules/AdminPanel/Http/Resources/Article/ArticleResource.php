<?php

namespace Modules\AdminPanel\Http\Resources\Article;

use App\Models\Article\Article;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $article Article */
    $article = $this->resource;

    $images = [];
    foreach ($article->getMedia('gallery') as $image) {
      $images[] = [
        'id' => $image->id,
        'url' => $image->getFullUrl(),
        'name' => $image->file_name,
      ];
    }

    $image = $article->getFirstMedia('image_thumb');

    return [
      'id' => $article->id,
      'name' => $article->name,
      'translations' => $article->getTranslationsArray(),
      'image' => [
        'url' => $image->getFullUrl(),
        'name' => $image->file_name,
      ],
      'images' => $images,
      'categoriesId' => $article->categories()->pluck('category_id'),
      'recommendId' => $article->recommend()->pluck('recommended_article_id'),
      'date' => $article->created_at->format('d.m.Y'),
    ];
  }
}
