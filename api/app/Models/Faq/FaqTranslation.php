<?php

namespace App\Models\Faq;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Faq\FaqTranslation
 *
 * @property int $id
 * @property int $faq_id
 * @property string $locale
 * @property string $name Поле назви
 * @property string $question Поле вопросов
 * @property string $reply Поле ответ
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereFaqId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FaqTranslation whereReply($value)
 * @mixin \Eloquent
 */
class FaqTranslation extends Model
{
  /**
   * The timestamps.
   *
   * @var bool
   */
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'faq_translations';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'question',
    'reply',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'name' => 'string',
    'question' => 'string',
    'reply' => 'string',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
