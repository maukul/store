<?php

namespace Modules\AdminPanel\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response as InertiaResponse;
use Modules\AdminPanel\Http\Requests\Pages\PagesEditRequest;
use Illuminate\Http\RedirectResponse;
use Modules\AdminPanel\Http\Resources\Page\PageResource;
use Modules\AdminPanel\ServicesDb\Page\PageService;
use Route;

class PagesController extends BaseController
{
  /**
   * @var PageService
   */
  protected $pages;

  /**
   * PagesController constructor.
   *
   * @param  PageService  $pages
   */
  public function __construct(
    PageService $pages
  ) {
    parent::__construct();
    $this->pages = $pages;

    $this->seo()->setTitle(trans('adminpanel::pages.title.'.(optional(Route::getCurrentRoute())->getActionMethod() ?? 'general')));
  }

  /**
   * Display a listing of the resource.
   *
   * @param  Request  $request
   * @return InertiaResponse
   */
  public function index(Request $request): InertiaResponse
  {
    $pagesData = $this->pages->search($request->all());
    $tableData = PageResource::collection($pagesData);
    return Inertia::render('Pages/Index', [
      'tableData' => $tableData,
      'request' => $request->all(),
      'tableParams' => [
        'columnFilters' => $request->input('columnFilters', []),
        'sort' => $request->input('sort', ['field' => '', 'type' => '']),
        'page' => $tableData->resource->currentPage() ?? 1,
        'perPage' => $tableData->resource->perPage() ?? 10,
      ],
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param $id
   * @return InertiaResponse
   */
  public function edit($id): InertiaResponse
  {
    $page = $this->pages->getById((int) $id);
    $pageData = new PageResource($page);
    return Inertia::render('Pages/Edit-'.$page->type, [
      'data' => $pageData,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  PagesEditRequest  $request
   * @param $id
   * @return RedirectResponse
   */
  public function update(PagesEditRequest $request, $id): RedirectResponse
  {
    $this->pages->update((int) $id, $request->getFormData());
    return redirect()->back()->with('message', trans('adminpanel::pages.message.success.update'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param $id
   * @return RedirectResponse
   */
  public function destroy($id): RedirectResponse
  {
    $this->pages->deleteById($id);
    return redirect()->route('admin-panel.static-pages.index')
      ->with('message', trans('adminpanel::pages.message.success.destroy'));
  }
}
