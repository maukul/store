<?php

namespace Modules\AdminPanel\Http\Controllers;

use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;

class BaseController extends Controller
{
  /**
   * BaseController constructor.
   */
  public function __construct()
  {
  }

  use AuthorizesRequests,
    DispatchesJobs,
    ValidatesRequests,
    SEOTools;
}
