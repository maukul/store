<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesInArticlesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('categories_in_articles', function (Blueprint $table) {
      // Таблиця категорий для статей блога
      $table->id();
      $table->bigInteger('category_id')->unsigned();
      $table->bigInteger('article_id')->unsigned();
      $table->foreign('category_id')->references('id')->on('categories_articles')->onDelete('cascade');
      $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('categories_in_articles');
  }
}
