<?php

namespace Modules\Api\Http\Resources\SeoPage;

use App\Models\SeoPage\SeoPage;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SeoPageResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $seoPage SeoPage */
    $seoPage = $this->resource;

    return [
      'title' => $seoPage->title,
      'keywords' => $seoPage->keywords,
      'description' => $seoPage->description,
    ];
  }
}
