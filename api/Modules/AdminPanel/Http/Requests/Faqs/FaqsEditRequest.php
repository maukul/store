<?php

namespace Modules\AdminPanel\Http\Requests\Faqs;

use Modules\AdminPanel\Http\Requests\FormRequest;

class FaqsEditRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize(): bool
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array
  {
    $rules = [
      'name.*' => [
        'required',
        'string',
        'max:90',
      ],
      'question.*' => [
        'required',
        'string',
        'max:255',
      ],
      'reply.*' => [
        'nullable',
        'string',
        'max:50000',
      ],
    ];

    return $rules;
  }

  /**
   * Get custom messages for validator errors.
   *
   * @return array
   */
  public function messages(): array
  {
    return [];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes(): array
  {
    $return = [];

    foreach (config('translatable.locales') as $item) {
      $return['name.'.$item] = trans('adminpanel::faqs.form.name.title').' ('.$item.')';
      $return['question.'.$item] = trans('adminpanel::faqs.form.question.title').' ('.$item.')';
      $return['reply.'.$item] = trans('adminpanel::faqs.form.reply.title').' ('.$item.')';
    }
    return $return;
  }
}
