<?php

namespace Modules\Api\ServicesDb\Page;

use Modules\Api\ServicesDb\Page\Repositories\PageRepositoryInterface;

class PageService
{
  /**
   * @var PageRepositoryInterface
   */
  protected $PageRepository;

  /**
   * PageService constructor.
   * @param  PageRepositoryInterface  $PageRepository
   */
  public function __construct(
    PageRepositoryInterface $PageRepository
  ) {
    $this->PageRepository = $PageRepository;
  }

  /**
   * @param  array  $params
   * @return mixed
   */
  public function getAll(array $params = [])
  {
    return $this->PageRepository->getAll($params);
  }

  /**
   * @param  array  $params
   * @return mixed
   */
  public function one(array $params = [])
  {
    return $this->PageRepository->one($params);
  }
}
