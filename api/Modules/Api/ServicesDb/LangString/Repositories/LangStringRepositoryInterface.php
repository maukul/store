<?php

namespace Modules\Api\ServicesDb\LangString\Repositories;

use Illuminate\Support\Collection;

/**
 * Interface LangStringRepositoryInterface
 * @package Modules\Api\ServicesDb\LangString\Repositories
 */
interface LangStringRepositoryInterface
{
  /**
   * @return string
   */
  public function hash(): string;

  /**
   * @param  array|string[]  $column
   * @return Collection
   */
  public function getAll(array $column = ['*']): Collection;
}
