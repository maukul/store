<?php

namespace Modules\Api\Http\Resources\ArticlesCategory;

use App\Models\CategoriesArticles\CategoriesArticles;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticlesCategoryOneResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $articlesCategory CategoriesArticles */
    $articlesCategory = $this->resource;

    return [
      'name' => $articlesCategory->name,
      'title' => $articlesCategory->title,
      'keywords' => $articlesCategory->keywords,
      'description' => $articlesCategory->description,
      'slug' => $articlesCategory->slug,
    ];
  }
}
