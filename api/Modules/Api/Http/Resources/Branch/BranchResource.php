<?php

namespace Modules\Api\Http\Resources\Branch;

use App\Models\Branch\Branch;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $branch Branch */
    $branch = $this->resource;

    return [
      'id' => $branch->id,
      'name' => $branch->name,
      'phones' => $branch->phones()->get(['phone']),
    ];
  }
}
