<?php
return [
  'title' => [
    'general' => 'Банеры',
    'index' => 'Список банеров',
    'create' => 'Создать банер',
    'edit' => 'Редактировать банер',
  ],
  'form' => [
    'type' => [
      'title' => 'Тип',
      'placeholder' => 'Тип',
      'options' => [
        'slider' => 'Слайдер',
        'banner' => 'Банер',
      ],
    ],
    'name' => [
      'title' => 'Название',
      'placeholder' => 'Название',
    ],
    'url' => [
      'title' => 'Url (ссылки)',
      'placeholder' => 'Url (ссылки)',
    ],
    'position' => [
      'title' => 'Позиция',
      'placeholder' => 'Позиция',
    ],
    'image' => [
      'title' => 'Фото',
      'placeholder' => 'Фото',
    ],
  ],
  'table' => [
    'columns' => [
      'name' => 'Имя',
      'image' => 'Фото',
    ],
  ],
  'message' => [
    'success' => [
      'store' => 'Создание банер прошло успешно.',
      'update' => 'Редактирование банер прошло успешно.',
      'destroy' => 'Удаление банер прошло успешно.',
    ],
  ],
  'text' => [
    'add_banners' => 'Добавить банер'
  ]
];
