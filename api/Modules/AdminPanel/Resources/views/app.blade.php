<!DOCTYPE html>
<html class="loading" lang="{{ app()->getLocale() }}" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
  <title>{{ $page['props']['metaInfo']['title'] }}</title>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600"
        rel="stylesheet">

  <!-- CSS -->
  <link rel="stylesheet" href="{{ mix('css/all.css','admin/dist') }}">
</head>
<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static " data-open="click"
      data-menu="vertical-menu-modern" data-col="">

@routes('admin')
@inertia


<script>
  window.default_locale = "{{ config('app.locale') }}";
  window.fallback_locale = "{{ config('app.fallback_locale') }}";
  window.messages = @json($messages);
</script>

<script src="{{ mix('js/all.js','admin/dist') }}" defer></script>

<script src="{{ mix('js/manifest.js','admin/dist') }}" defer></script>
<script src="{{ mix('js/vendor.js','admin/dist') }}" defer></script>
<script src="{{ mix('js/app.js','admin/dist') }}" defer></script>

</body>
</html>
