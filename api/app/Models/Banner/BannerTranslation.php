<?php

namespace App\Models\Banner;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Banner\BannerTranslation
 *
 * @property int $id
 * @property int $banner_id
 * @property string $locale
 * @property string $name
 * @property string|null $url
 * @method static \Illuminate\Database\Eloquent\Builder|BannerTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerTranslation whereBannerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerTranslation whereUrl($value)
 * @mixin \Eloquent
 */
class BannerTranslation extends Model
{
  /**
   * The timestamps.
   *
   * @var bool
   */
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'banner_translations';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'url'
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'name' => 'string',
    'url' => 'string',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}

