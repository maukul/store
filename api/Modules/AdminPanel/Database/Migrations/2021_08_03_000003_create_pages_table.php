<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('pages', function (Blueprint $table) {
      // Таблиця сторінок
      $table->id();
      $table->string('type');
      $table->timestamps();
      $table->softDeletes();
    });

    Schema::create('page_translations', function (Blueprint $table) {
      // Таблиця перекладів сторінок
      $table->increments('id');
      $table->bigInteger('page_id')->unsigned();
      $table->string('locale')->index();
      $table->string('name', 90)->comment('Поле назви');
      $table->string('title', 255)->comment('Поле тайтл');
      $table->string('keywords', 255)->nullable()->comment('Поле ключові слова');
      $table->string('description', 255)->nullable()->comment('Поле ключові слова');
      $table->json('value');
      $table->string('slug', 255)->comment('Url');

      $table->unique(['page_id', 'locale']);
      $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('page_translations');
    Schema::dropIfExists('pages');
  }
}
