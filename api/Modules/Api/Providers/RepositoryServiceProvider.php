<?php

namespace Modules\Api\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Api\ServicesDb\Article\Repositories\ArticleRepositoryInterface;
use Modules\Api\ServicesDb\Article\Repositories\EloquentArticleRepository;
use Modules\Api\ServicesDb\ArticlesCategory\Repositories\ArticlesCategoryRepositoryInterface;
use Modules\Api\ServicesDb\ArticlesCategory\Repositories\EloquentArticlesCategoryRepository;
use Modules\Api\ServicesDb\Banner\Repositories\BannerRepositoryInterface;
use Modules\Api\ServicesDb\Banner\Repositories\EloquentBannerRepository;
use Modules\Api\ServicesDb\Branch\Repositories\BranchRepositoryInterface;
use Modules\Api\ServicesDb\Branch\Repositories\EloquentBranchRepository;
use Modules\Api\ServicesDb\Brand\Repositories\BrandRepositoryInterface;
use Modules\Api\ServicesDb\Brand\Repositories\EloquentBrandRepository;
use Modules\Api\ServicesDb\Faq\Repositories\EloquentFaqRepository;
use Modules\Api\ServicesDb\Faq\Repositories\FaqRepositoryInterface;
use Modules\Api\ServicesDb\LangString\Repositories\EloquentLangStringRepository;
use Modules\Api\ServicesDb\LangString\Repositories\LangStringRepositoryInterface;
use Modules\Api\ServicesDb\Page\Repositories\EloquentPageRepository;
use Modules\Api\ServicesDb\Page\Repositories\PageRepositoryInterface;
use Modules\Api\ServicesDb\Product\Repositories\EloquentProductRepository;
use Modules\Api\ServicesDb\Product\Repositories\ProductRepositoryInterface;
use Modules\Api\ServicesDb\SeoPage\Repositories\EloquentSeoPageRepository;
use Modules\Api\ServicesDb\SeoPage\Repositories\SeoPageRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
  /**
   * Called before routes are registered.
   *
   * Register any model bindings or pattern based filters.
   *
   * @return void
   */
  public function boot(): void
  {
    $this->app->bind(
      BannerRepositoryInterface::class,
      EloquentBannerRepository::class
    );
    $this->app->bind(
      BranchRepositoryInterface::class,
      EloquentBranchRepository::class
    );
    $this->app->bind(
      LangStringRepositoryInterface::class,
      EloquentLangStringRepository::class
    );
    $this->app->bind(
      ProductRepositoryInterface::class,
      EloquentProductRepository::class
    );
    $this->app->bind(
      BrandRepositoryInterface::class,
      EloquentBrandRepository::class
    );
    $this->app->bind(
      SeoPageRepositoryInterface::class,
      EloquentSeoPageRepository::class
    );
    $this->app->bind(
      ArticleRepositoryInterface::class,
      EloquentArticleRepository::class
    );
    $this->app->bind(
      PageRepositoryInterface::class,
      EloquentPageRepository::class
    );
    $this->app->bind(
      FaqRepositoryInterface::class,
      EloquentFaqRepository::class
    );
    $this->app->bind(
      ArticlesCategoryRepositoryInterface::class,
      EloquentArticlesCategoryRepository::class
    );
  }
}
