<?php

namespace Modules\AdminPanel\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\AdminPanel\ServicesDb\Admin\Repositories\AdminRepositoryInterface;
use Modules\AdminPanel\ServicesDb\Admin\Repositories\EloquentAdminRepository;
use Modules\AdminPanel\ServicesDb\Article\Repositories\ArticleRepositoryInterface;
use Modules\AdminPanel\ServicesDb\Article\Repositories\EloquentArticleRepository;
use Modules\AdminPanel\ServicesDb\Banner\Repositories\BannerRepositoryInterface;
use Modules\AdminPanel\ServicesDb\Banner\Repositories\EloquentBannerRepository;
use Modules\AdminPanel\ServicesDb\CategoriesArticles\Repositories\CategoriesArticlesRepositoryInterface;
use Modules\AdminPanel\ServicesDb\CategoriesArticles\Repositories\EloquentCategoriesArticlesRepository;
use Modules\AdminPanel\ServicesDb\Faq\Repositories\EloquentFaqRepository;
use Modules\AdminPanel\ServicesDb\Faq\Repositories\FaqRepositoryInterface;
use Modules\AdminPanel\ServicesDb\LangString\Repositories\EloquentLangStringRepository;
use Modules\AdminPanel\ServicesDb\LangString\Repositories\LangStringRepositoryInterface;
use Modules\AdminPanel\ServicesDb\Media\Repositories\EloquentMediaRepository;
use Modules\AdminPanel\ServicesDb\Media\Repositories\MediaRepositoryInterface;
use Modules\AdminPanel\ServicesDb\Page\Repositories\EloquentPageRepository;
use Modules\AdminPanel\ServicesDb\Page\Repositories\PageRepositoryInterface;
use Modules\AdminPanel\ServicesDb\SeoPage\Repositories\EloquentSeoPageRepository;
use Modules\AdminPanel\ServicesDb\SeoPage\Repositories\SeoPageRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
  /**
   * Called before routes are registered.
   *
   * Register any model bindings or pattern based filters.
   *
   * @return void
   */
  public function boot(): void
  {
    $this->app->bind(
      AdminRepositoryInterface::class,
      EloquentAdminRepository::class
    );
    $this->app->bind(
      BannerRepositoryInterface::class,
      EloquentBannerRepository::class
    );
    $this->app->bind(
      MediaRepositoryInterface::class,
      EloquentMediaRepository::class
    );
    $this->app->bind(
      LangStringRepositoryInterface::class,
      EloquentLangStringRepository::class
    );
    $this->app->bind(
      SeoPageRepositoryInterface::class,
      EloquentSeoPageRepository::class
    );
    $this->app->bind(
      PageRepositoryInterface::class,
      EloquentPageRepository::class
    );
    $this->app->bind(
      CategoriesArticlesRepositoryInterface::class,
      EloquentCategoriesArticlesRepository::class
    );
    $this->app->bind(
      ArticleRepositoryInterface::class,
      EloquentArticleRepository::class
    );
    $this->app->bind(
      FaqRepositoryInterface::class,
      EloquentFaqRepository::class
    );
  }
}
