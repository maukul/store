<?php

namespace Modules\Api\Http\Requests\Email;

use Modules\Api\Http\Requests\FormRequest;

class EmailSendRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize(): bool
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array
  {
    return [
      'name' => [
        'required',
        'string',
        'min:2',
        'max:191',
      ],
      'phone' => [
        'required',
        'string',
      ],
      'email' => [
        'required',
        'email',
        'min:2',
        'max:191',
      ],
      'message' => [
        'required',
        'string',
        'min:2',
        'max:255',
      ],
    ];
  }

  /**
   * Get custom messages for validator errors.
   *
   * @return array
   */
  public function messages(): array
  {
    return [];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes(): array
  {
    return [
      'name' => trans('api::contact.form.name.title'),
      'phone' => trans('api::contact.form.phone.title'),
      'email' => trans('api::contact.form.email.title'),
      'message' => trans('api::contact.form.message.title'),
    ];
  }
}
