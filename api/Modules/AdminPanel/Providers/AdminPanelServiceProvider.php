<?php

namespace Modules\AdminPanel\Providers;

use Config;
use ExportLocalization;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App;

class AdminPanelServiceProvider extends ServiceProvider
{
  /**
   * @var string $moduleName
   */
  protected $moduleName = 'AdminPanel';
  /**
   * @var string $moduleNameLower
   */
  protected $moduleNameLower = 'adminpanel';

  /**
   * Boot the application events.
   *
   * @return void
   */
  public function boot(): void
  {
    App::setLocale('ru');
    $this->registerTranslations();
    $this->registerConfig();
    $this->registerViews();
    $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));
    $this->viewMessages();
  }

  /**
   * Register translations.
   *
   * @return void
   */
  public function registerTranslations(): void
  {
    $langPath = resource_path('lang/modules/'.$this->moduleNameLower);

    if (is_dir($langPath)) {
      $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
    } else {
      $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
    }
  }

  /**
   * Register config.
   *
   * @return void
   */
  protected function registerConfig(): void
  {
    $this->publishes([
      module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower.'.php'),
    ], 'config');
    $this->mergeConfigFrom(
      module_path($this->moduleName, 'Config/config.php'), $this->moduleNameLower
    );
  }

  /**
   * Register views.
   *
   * @return void
   */
  public function registerViews(): void
  {
    $viewPath = resource_path('views/modules/'.$this->moduleNameLower);

    $sourcePath = module_path($this->moduleName, 'Resources/views');

    $this->publishes([
      $sourcePath => $viewPath
    ], ['views', $this->moduleNameLower.'-module-views']);

    $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
  }

  /**
   * Get view paths modules.
   *
   * @return array
   */
  private function getPublishableViewPaths(): array
  {
    $paths = [];
    foreach (Config::get('view.paths') as $path) {
      if (is_dir($path.'/modules/'.$this->moduleNameLower)) {
        $paths[] = $path.'/modules/'.$this->moduleNameLower;
      }
    }
    return $paths;
  }

  /**
   * View messages
   *
   * @return void
   */
  public function viewMessages(): void
  {
    View::composer('adminpanel::app', function ($view) {
      config([
        'laravel-localization.paths.lang_dirs' => [
          resource_path('lang'), base_path('Modules/AdminPanel/Resources/lang')
        ]
      ]);
      return $view->with([
        'messages' => ExportLocalization::export()->toFlat(),
      ]);
    });
  }

  /**
   * Register the service provider.
   *
   * @return void
   */
  public function register(): void
  {
    $this->app->register(RepositoryServiceProvider::class);
    $this->app->register(RouteServiceProvider::class);
  }

  /**
   * Get the services provided by the provider.
   *
   * @return array
   */
  public function provides(): array
  {
    return [];
  }
}
