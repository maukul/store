<?php
return [
  'title' => [
    'general' => 'Aдмины',
    'index' => 'Список админов',
    'create' => 'Создать админа',
    'edit' => 'Редактировать админа',
  ],
  'form' => [
    'email' => [
      'title' => 'Почта (логин)',
      'placeholder' => 'example@example.com',
    ],
    'password' => [
      'title' => 'Пароль',
      'placeholder' => '****',
    ],
    'name' => [
      'title' => 'Имя',
      'placeholder' => 'Имя',
    ],
  ],
  'table' => [
    'columns' => [
      'name' => 'Имя',
      'email' => 'Почта',
      'date' => 'Дата',
    ],
  ],
  'message' => [
    'success' => [
      'store' => 'Создание админа прошло успешно.',
      'update' => 'Редактирование админа прошло успешно.',
      'destroy' => 'Удаление админа прошло успешно.',
    ],
  ],
  'text' => [
    'add_admins' => 'Добавить админа'
  ]
];
