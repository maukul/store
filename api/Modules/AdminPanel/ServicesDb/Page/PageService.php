<?php

namespace Modules\AdminPanel\ServicesDb\Page;

use App\Models\Page\Page;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Modules\AdminPanel\ServicesDb\Page\Repositories\PageRepositoryInterface;

class PageService
{
  /**
   * @var PageRepositoryInterface
   */
  protected $pageRepository;

  /**
   * PageService constructor.
   * @param  PageRepositoryInterface  $pageRepository
   */
  public function __construct(
    PageRepositoryInterface $pageRepository
  ) {
    $this->pageRepository = $pageRepository;
  }

  /**
   * Get in tables pages.
   *
   * @param  array  $params
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    return $this->pageRepository->search(
      array_merge(
        $params,
        []
      )
    );
  }

  /**
   * Get page by id.
   *
   * @param  int  $id
   * @return Page
   */
  public function getById(int $id): Page
  {
    return $this->pageRepository->getById($id);
  }

  /**
   * Get page all.
   *
   * @param  array|string[]  $column
   * @return Page
   */
  public function getAll(array $column = ['*']): Page
  {
    return $this->pageRepository->getAll($column);
  }

  /**
   * Store to DB if there are no errors.
   *
   * @param  array  $data
   * @return Page
   */
  public function create($data): Page
  {
    try {
      $create = [
        'type' => Str::slug($data['type'], '_'),
      ];
      foreach (config('translatable.locales') as $locale) {
        $create[$locale]['name'] = $data['name'][$locale];
        $create[$locale]['title'] = $data['title'][$locale];
        $create[$locale]['keywords'] = $data['keywords'][$locale];
        $create[$locale]['description'] = $data['description'][$locale];
        $create[$locale]['value'] = $data['value'][$locale];
        if (!empty($data['slug'][$locale])) {
          $create[$locale]['slug'] = Str::slug($data['slug'][$locale], '-');
        }
      }

      $result = $this->pageRepository->create($create, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to create page data');
      }
    }

    return $result;
  }

  /**
   * Update page data
   * Store to DB if there are no errors.
   *
   * @param $id
   * @param $data
   * @return bool
   */
  public function update($id, $data): bool
  {
    $page = false;
    DB::beginTransaction();

    try {
      $update = [
        'type' => Str::slug($data['type'], '_'),
      ];
      foreach (config('translatable.locales') as $locale) {
        $update[$locale]['name'] = $data['name'][$locale];
        $update[$locale]['title'] = $data['title'][$locale];
        $update[$locale]['keywords'] = $data['keywords'][$locale];
        $update[$locale]['description'] = $data['description'][$locale];
        $update[$locale]['value'] = $data['value'][$locale];
        if (!empty($data['slug'][$locale])) {
          $update[$locale]['slug'] = Str::slug($data['slug'][$locale], '-');
        }
      }

      $page = $this->pageRepository->update($id, $update, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to update page data');
      }
    }

    DB::commit();

    return $page;
  }

  /**
   * Delete page by id.
   *
   * @param $id
   * @return bool
   */
  public function deleteById($id): bool
  {
    $page = false;
    DB::beginTransaction();

    try {
      $page = $this->pageRepository->deleteById($id);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to delete page data');
      }
    }

    DB::commit();

    return $page;
  }
}
