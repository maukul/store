<?php

namespace Modules\Api\Http\Resources\Page;

use App\Models\Page\Page;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PagesResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $page Page */
    $page = $this->resource;

    return [
      'type' => $page->type,
      'name' => $page->name,
      'slug' => $page->slug,
    ];
  }
}
