<?php

namespace Modules\AdminPanel\Database\Seeders;

use App\Models\Faq\Faq;
use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faqCount = 10;
    $this->command->getOutput()->progressStart($faqCount);
    Faq::factory($faqCount)->create()->each(function () {
      $this->command->getOutput()->progressAdvance();
    });
  }
}
