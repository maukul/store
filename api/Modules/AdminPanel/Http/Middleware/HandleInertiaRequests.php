<?php

namespace Modules\AdminPanel\Http\Middleware;

use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use Inertia\Middleware;
use Auth;

class HandleInertiaRequests extends Middleware
{
  /**
   * The root template that's loaded on the first page visit.
   *
   * @see https://inertiajs.com/server-side-setup#root-template
   * @var string
   */
  protected $rootView = 'adminpanel::app';

  /**
   * Determines the current asset version.
   *
   * @see https://inertiajs.com/asset-versioning
   * @param  Request  $request
   * @return string|null
   */
  public function version(Request $request): string|null
  {
    if (file_exists($manifest = public_path('/admin/dist/mix-manifest.json'))) {
      return md5_file($manifest);
    }
    return null;
  }

  /**
   * Defines the props that are shared by default.
   *
   * @see https://inertiajs.com/shared-data
   * @param  Request  $request
   * @return array
   */
  public function share(Request $request): array
  {
    $shared = [
      'metaInfo' => fn() => [
        'title' => SEOMeta::getTitle()
      ],
      'locales' => fn() => config('translatable.locales'),
      'auth' => function () use ($request) {
        if (!Auth::guard('admins')->check()) {
          return;
        }

        $admin = $request->user('admins');
        return $admin->only(['id', 'name', 'email']);
      },
      'flash' => []
    ];

    if ($request->session()->has('message')) {
      $shared['flash']['message'] = fn() => $request->session()->get('message');
      $shared['flash']['type'] = fn() => $request->session()->get('type') ?? 'success';
    }
    if ($request->session()->has('error')) {
      $shared['flash']['error'] = fn() => $request->session()->get('error');
    }

    return array_merge(parent::share($request), $shared);
  }
}
