<?php

namespace App\Models\Article;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Casts\AsArrayObject;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Article\ArticleTranslation
 *
 * @property int $id
 * @property int $article_id
 * @property string $locale
 * @property string $name Поле назви
 * @property string $title Поле тайтл
 * @property string|null $keywords Поле ключові слова
 * @property string|null $description Поле ключові слова
 * @property string|null $content Поле контент
 * @property string $slug Url категорії для для статей блога
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleTranslation withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 * @mixin \Eloquent
 */
class ArticleTranslation extends Model
{
  use Sluggable;

  /**
   * The timestamps.
   *
   * @var bool
   */
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'article_translations';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'title',
    'keywords',
    'description',
    'content',
    'slug',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'name' => 'string',
    'title' => 'string',
    'keywords' => 'string',
    'description' => 'string',
    'content' => 'string',
    'slug' => 'string',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /**
   * Return the sluggable configuration array for this model.
   *
   * @return array [
   * 'slug' => [
   *      'source' => 'name'
   * ]
   */
  public function sluggable(): array
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
