<?php

namespace Modules\AdminPanel\ServicesDb\Admin\Repositories;

use App\Models\Admin;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface AdminRepositoryInterface
 * @package Modules\AdminPanel\ServicesDb\Admin\Repositories
 */
interface AdminRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return mixed
   */
  public function search(array $params = []): LengthAwarePaginator;

  /**
   * @param  int  $id
   * @return mixed
   */
  public function getById(int $id): Admin;

  /**
   * @param  array|string[]  $columns
   * @return mixed
   */
  public function getAll(array $columns = ['*']): Admin;

  /**
   * @param  array  $create
   * @return mixed
   */
  public function create(array $create, array $data): Admin;

  /**
   * @param  int  $id
   * @param  array  $update
   * @return mixed
   */
  public function update(int $id, array $update, array $data): bool;

  /**
   * @param  int  $id
   * @return mixed
   */
  public function deleteById(int $id): bool;
}
