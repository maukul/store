<?php

namespace Modules\AdminPanel\Http\Resources\CategoriesArticles;

use App\Models\CategoriesArticles\CategoriesArticles;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoriesArticlesResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $categoriesArticles CategoriesArticles */
    $categoriesArticles = $this->resource;

    return [
      'id' => $categoriesArticles->id,
      'position' => $categoriesArticles->position,
      'name' => $categoriesArticles->name,
      'translations' => $categoriesArticles->getTranslationsArray(),
      'date' => $categoriesArticles->created_at->format('d.m.Y'),
    ];
  }
}
