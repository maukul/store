<?php

namespace Modules\Api\ServicesDb\Brand\Repositories;

use App\Models\Brand\Brand;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\Api\ServicesDb\Brand\Repositories\BrandRepositoryInterface;

/**
 * Class EloquentBrandRepository.
 */
class EloquentBrandRepository implements BrandRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'show_main' => bool,
   *      ],
   *      'sort' => string
   * ]
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $params, array $columns = ['*']): Collection
  {
    $modelBrand = Brand::query();
    $filters = $params['filters'] ?? [];
    $sort = $params['sort'] ?? '';
    if (!empty($filters)) {
      if (!empty($filters['show_main'])) {
        $modelBrand->where('show_main', '=', $filters['show_main']);
      }
    }
    if (!empty($sort)) {
      if (in_array($sort, (new Brand)->translatedAttributes)) {
        $modelBrand->orderByTranslation($sort);
      } else {
        $modelBrand->orderBy($sort);
      }
    }
    $modelBrand->where('show', '=', true);
    return $modelBrand->get($columns);
  }
}
