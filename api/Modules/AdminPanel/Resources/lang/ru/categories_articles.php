<?php
return [
  'title' => [
    'general' => 'Категории блогу',
    'index' => 'Список категорий блогу',
    'create' => 'Создать категорию блогу',
    'edit' => 'Редактировать категорию блогу',
  ],
  'form' => [
    'position' => [
      'title' => 'Позиция',
      'placeholder' => 'Позиция',
    ],
    'name' => [
      'title' => 'Название',
      'placeholder' => 'Название',
    ],
    'title' => [
      'title' => 'Мета тайтл',
      'placeholder' => 'Мета тайтл',
    ],
    'keywords' => [
      'title' => 'Мета ключевые слова',
      'placeholder' => 'Мета ключевые слова',
    ],
    'description' => [
      'title' => 'Мета описание',
      'placeholder' => 'Мета описание',
    ],
    'slug' => [
      'title' => 'Url (ссылки)',
      'placeholder' => 'Url (ссылки)',
    ],
  ],
  'table' => [
    'columns' => [
      'name' => 'Название',
    ],
  ],
  'message' => [
    'success' => [
      'store' => 'Создание категории блогу прошло успешно.',
      'update' => 'Редактирование категории блогу прошло успешно.',
      'destroy' => 'Удаление категории блогу прошло успешно.',
    ],
  ],
  'text' => [
    'add_categories_articles' => 'Добавить категорию блогу'
  ]
];
