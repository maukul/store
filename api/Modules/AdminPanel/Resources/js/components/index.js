import Vue from 'vue'
import FeatherIcon from './FeatherIcon'

Vue.component(FeatherIcon.name, FeatherIcon)
