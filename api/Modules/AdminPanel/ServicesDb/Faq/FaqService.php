<?php

namespace Modules\AdminPanel\ServicesDb\Faq;

use App\Models\Faq\Faq;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use Modules\AdminPanel\ServicesDb\Faq\Repositories\FaqRepositoryInterface;

class FaqService
{
  /**
   * @var FaqRepositoryInterface
   */
  protected $faqRepository;

  /**
   * FaqService constructor.
   * @param  FaqRepositoryInterface  $faqRepository
   */
  public function __construct(
    FaqRepositoryInterface $faqRepository
  ) {
    $this->faqRepository = $faqRepository;
  }

  /**
   * Get in tables faqs.
   *
   * @param  array  $params
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    return $this->faqRepository->search(
      array_merge(
        $params,
        []
      )
    );
  }

  /**
   * Get faq by id.
   *
   * @param  int  $id
   * @return Faq
   */
  public function getById(int $id): Faq
  {
    return $this->faqRepository->getById($id);
  }

  /**
   * Get faq all.
   *
   * @param  array|string[]  $column
   * @return Collection
   */
  public function getAll(array $column = ['*']): Collection
  {
    return $this->faqRepository->getAll($column);
  }

  /**
   * Store to DB if there are no errors.
   *
   * @param  array  $data
   * @return Faq
   */
  public function create($data): Faq
  {
    try {
      $create = [
        'position' => $data['position']
      ];
      foreach (config('translatable.locales') as $locale) {
        $create[$locale]['name'] = $data['name'][$locale];
        $create[$locale]['question'] = $data['question'][$locale];
        $create[$locale]['reply'] = $data['reply'][$locale];
      }

      $result = $this->faqRepository->create($create, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to create faq data');
      }
    }

    return $result;
  }

  /**
   * Update faq data
   * Store to DB if there are no errors.
   *
   * @param $id
   * @param $data
   * @return bool
   */
  public function update($id, $data): bool
  {
    $faq = false;
    DB::beginTransaction();

    try {
      $update = [
        'position' => $data['position']
      ];
      foreach (config('translatable.locales') as $locale) {
        $update[$locale]['name'] = $data['name'][$locale];
        $create[$locale]['question'] = $data['question'][$locale];
        $create[$locale]['reply'] = $data['reply'][$locale];
      }

      $faq = $this->faqRepository->update($id, $update, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to update faq data');
      }
    }

    DB::commit();

    return $faq;
  }

  /**
   * Delete faq by id.
   *
   * @param $id
   * @return bool
   */
  public function deleteById($id): bool
  {
    $faq = false;
    DB::beginTransaction();

    try {
      $faq = $this->faqRepository->deleteById($id);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to delete faq data');
      }
    }

    DB::commit();

    return $faq;
  }
}
