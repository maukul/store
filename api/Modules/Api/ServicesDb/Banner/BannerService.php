<?php

namespace Modules\Api\ServicesDb\Banner;

use Illuminate\Support\Collection;
use Modules\Api\ServicesDb\Banner\Repositories\BannerRepositoryInterface;

class BannerService
{
  /**
   * @var BannerRepositoryInterface
   */
  protected $bannerRepository;

  /**
   * BannerService constructor.
   * @param  BannerRepositoryInterface  $bannerRepository
   */
  public function __construct(
    BannerRepositoryInterface $bannerRepository
  ) {
    $this->bannerRepository = $bannerRepository;
  }

  /**
   * Get banner all.
   *
   * @param  array|string[]  $column
   * @return Collection
   */
  public function getAll(array $column = ['*']): Collection
  {
    return $this->bannerRepository->getAll($column);
  }
}
