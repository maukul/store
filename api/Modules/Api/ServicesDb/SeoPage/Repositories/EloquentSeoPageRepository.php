<?php

namespace Modules\Api\ServicesDb\SeoPage\Repositories;

use App\Models\SeoPage\SeoPage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Modules\Api\ServicesDb\SeoPage\Repositories\SeoPageRepositoryInterface;

/**
 * Class EloquentSeoPageRepository.
 */
class EloquentSeoPageRepository implements SeoPageRepositoryInterface
{
  /**
   * @param  string  $systemName
   * @return SeoPage
   */
  public function getBySystemName(string $systemName): SeoPage
  {
    return SeoPage::where('system_name', $systemName)->first();
  }
}
