<?php

namespace Modules\AdminPanel\ServicesDb\CategoriesArticles\Repositories;

use App\Models\CategoriesArticles\CategoriesArticles;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Interface CategoriesArticlesRepositoryInterface
 * @package Modules\AdminPanel\ServicesDb\CategoriesArticles\Repositories
 */
interface CategoriesArticlesRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perCategoriesArticles' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator;

  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection;

  /**
   * @param  int  $id
   * @return CategoriesArticles
   */
  public function getById(int $id): CategoriesArticles;

  /**
   * @param  array  $create
   * @param  array  $data
   * @return CategoriesArticles
   */
  public function create(array $create, array $data): CategoriesArticles;

  /**
   * @param  int  $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update(int $id, array $update, array $data): bool;

  /**
   * @param  int  $id
   * @return bool
   */
  public function deleteById(int $id): bool;
}
