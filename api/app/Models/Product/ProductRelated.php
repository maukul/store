<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Product\ProductRelated
 *
 * @property int $id
 * @property int $product_id Поле яке посилається на товар
 * @property int $related_id Поле яке посилається на супутній в головній базі аналог procedure  IM_product_related(product).kodm
 * @property-read \App\Models\Product\Product $product
 * @property-read \App\Models\Product\Product $related
 * @method static \Illuminate\Database\Eloquent\Builder|ProductRelated newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductRelated newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductRelated query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductRelated whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductRelated whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductRelated whereRelatedId($value)
 * @mixin \Eloquent
 */
class ProductRelated extends Model
{
  /**
   * The timestamps.
   *
   * @var bool
   */
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'product_related';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_id',
    'related_id',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'product_id' => 'integer',
    'related_id' => 'integer',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /**
   * @return BelongsTo<Product>|Product
   */
  public function product(): BelongsTo
  {
    return $this->belongsTo(Product::class);
  }

  /**
   * @return BelongsTo<Product>|Product
   */
  public function related(): BelongsTo
  {
    return $this->belongsTo(Product::class, 'related_id');
  }

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
