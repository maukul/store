<?php

namespace Modules\AdminPanel\ServicesDb\LangString\Repositories;

use App\Models\LangString\LangString;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface LangStringRepositoryInterface
 * @package Modules\AdminPanel\ServicesDb\LangString\Repositories
 */
interface LangStringRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator;

  /**
   * @param  array|string[]  $columns
   * @return LangString
   */
  public function getAll(array $columns = ['*']): LangString;

  /**
   * @param  int  $id
   * @return LangString
   */
  public function getById(int $id): LangString;

  /**
   * @param  array  $create
   * @param  array  $data
   * @return LangString
   */
  public function create(array $create, array $data): LangString;

  /**
   * @param  int  $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update(int $id, array $update, array $data): bool;

  /**
   * @param  int  $id
   * @return bool
   */
  public function deleteById(int $id): bool;
}
