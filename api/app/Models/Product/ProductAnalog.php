<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Product\ProductAnalog
 *
 * @property int $id
 * @property int $product_id Поле яке посилається на товар
 * @property int $analog_id Поле яке посилається на аналог в головній базі аналог procedure IM_product_analog(product).kodm
 * @property-read \App\Models\Product\Product $analog
 * @property-read \App\Models\Product\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAnalog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAnalog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAnalog query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAnalog whereAnalogId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAnalog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductAnalog whereProductId($value)
 * @mixin \Eloquent
 */
class ProductAnalog extends Model
{
  /**
   * The timestamps.
   *
   * @var bool
   */
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'product_analogs';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_id',
    'analog_id',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'product_id' => 'integer',
    'analog_id' => 'integer',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /**
   * @return BelongsTo<Product>|Product
   */
  public function product(): BelongsTo
  {
    return $this->belongsTo(Product::class);
  }

  /**
   * @return BelongsTo<Product>|Product
   */
  public function analog(): BelongsTo
  {
    return $this->belongsTo(Product::class, 'analog_id');
  }

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
