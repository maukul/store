<?php

namespace Modules\Api\Http\Requests;

use Illuminate\Foundation\Http\FormRequest as BaseFormRequest;
use Illuminate\Support\Arr;

abstract class FormRequest extends BaseFormRequest
{
  /**
   * @return array
   */
  public function getFormData(): array
  {
    $data = request()->all();

    $data = Arr::except($data, [
      '_token',
      '_method',
    ]);
    return $data;
  }
}
