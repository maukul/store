<?php

namespace Modules\Api\ServicesDb\Branch;

use Illuminate\Support\Collection;
use Modules\Api\ServicesDb\Branch\Repositories\BranchRepositoryInterface;

class BranchService
{
  /**
   * @var BranchRepositoryInterface
   */
  protected $branchRepository;

  /**
   * BranchService constructor.
   * @param  BranchRepositoryInterface  $branchRepository
   */
  public function __construct(
    BranchRepositoryInterface $branchRepository
  ) {
    $this->branchRepository = $branchRepository;
  }

  /**
   * Get branch all.
   *
   * @param  array|string[]  $column
   * @return Collection
   */
  public function getAll(array $column = ['*']): Collection
  {
    return $this->branchRepository->getAll($column);
  }
}
