<?php

namespace Modules\AdminPanel\Http\Requests\CategoriesArticles;

use Modules\AdminPanel\Http\Requests\FormRequest;

class CategoriesArticlesEditRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize(): bool
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array
  {
    $rules = [
      'position' => [
        'required',
        'numeric',
      ],
      'name.*' => [
        'required',
        'string',
        'max:90',
      ],
      'title.*' => [
        'required',
        'string',
        'max:255',
      ],
      'keywords.*' => [
        'nullable',
        'string',
        'max:255',
      ],
      'description.*' => [
        'nullable',
        'string',
        'max:255',
      ],
    ];

    return $rules;
  }

  /**
   * Get custom messages for validator errors.
   *
   * @return array
   */
  public function messages(): array
  {
    return [];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes(): array
  {
    $return = [];

    $return['position'] = trans('adminpanel::categories_articles.form.position.title');

    foreach (config('translatable.locales') as $item) {
      $return['name.'.$item] = trans('adminpanel::categories_articles.form.name.title').' ('.$item.')';
      $return['title.'.$item] = trans('adminpanel::categories_articles.form.title.title').' ('.$item.')';
      $return['keywords.'.$item] = trans('adminpanel::categories_articles.form.keywords.title').' ('.$item.')';
      $return['description.'.$item] = trans('adminpanel::categories_articles.form.description.title').' ('.$item.')';
    }
    return $return;
  }
}
