<?php

namespace Modules\AdminPanel\ServicesDb\Faq\Repositories;

use App\Models\Faq\Faq;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Interface FaqRepositoryInterface
 * @package Modules\AdminPanel\ServicesDb\Faq\Repositories
 */
interface FaqRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator;

  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection;

  /**
   * @param  int  $id
   * @return Faq
   */
  public function getById(int $id): Faq;

  /**
   * @param  array  $create
   * @param  array  $data
   * @return Faq
   */
  public function create(array $create, array $data): Faq;

  /**
   * @param  int  $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update(int $id, array $update, array $data): bool;

  /**
   * @param  int  $id
   * @return bool
   */
  public function deleteById(int $id): bool;
}
