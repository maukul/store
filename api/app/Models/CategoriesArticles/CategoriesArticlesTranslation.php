<?php

namespace App\Models\CategoriesArticles;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CategoriesArticles\CategoriesArticlesTranslation
 *
 * @property int $id
 * @property int $category_id
 * @property string $locale
 * @property string $name Поле назви
 * @property string $title Поле тайтл
 * @property string|null $keywords Поле ключові слова
 * @property string|null $description Поле ключові слова
 * @property string|null $content Поле контент
 * @property string $slug Url категорії для для статей блога
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoriesArticlesTranslation withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 * @mixin \Eloquent
 */
class CategoriesArticlesTranslation extends Model
{
  use Sluggable;

  /**
   * The timestamps.
   *
   * @var bool
   */
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'categories_articles_translations';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'title',
    'keywords',
    'description',
    'slug',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'name' => 'string',
    'title' => 'string',
    'keywords' => 'string',
    'description' => 'string',
    'slug' => 'string',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /**
   * Return the sluggable configuration array for this model.
   *
   * @return array [
   * 'slug' => [
   *      'source' => 'name'
   * ]
   */
  public function sluggable(): array
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
