export default async (context, locale) => {
  const resourceMessages = await context.$axios.$get('string-translations', {
    params: {
      'locale': locale
    }
  })
  return resourceMessages.data
}
