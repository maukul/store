<?php

namespace Modules\Api\ServicesDb\ArticlesCategory;

use App\Models\CategoriesArticles\CategoriesArticles;
use Illuminate\Database\Eloquent\Collection;
use Modules\Api\ServicesDb\ArticlesCategory\Repositories\ArticlesCategoryRepositoryInterface;

class ArticlesCategoryService
{
  /**
   * @var ArticlesCategoryRepositoryInterface
   */
  protected $articlesCategoryRepository;

  /**
   * ArticlesCategoryService constructor.
   * @param  ArticlesCategoryRepositoryInterface  $articlesCategoryRepository
   */
  public function __construct(
    ArticlesCategoryRepositoryInterface $articlesCategoryRepository
  ) {
    $this->articlesCategoryRepository = $articlesCategoryRepository;
  }

  /**
   * @param  array  $params  = [
   *      'sort' => string,
   * ]
   * @return Collection
   */
  public function getAll(array $params = []): Collection
  {
    return $this->articlesCategoryRepository->getAll($params);
  }

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'type' => string,
   *      ],
   * ]
   * @return CategoriesArticles
   */
  public function slug(array $params = []): CategoriesArticles
  {
    return $this->articlesCategoryRepository->slug($params);
  }
}
