<?php

namespace Modules\AdminPanel\Http\Resources\Page;

use App\Models\Page\Page;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $pages Page */
    $pages = $this->resource;

    $images = [];
    foreach ($pages->getMedia('gallery') as $image) {
      $images[] = [
        'id' => $image->id,
        'url' => $image->getFullUrl(),
        'name' => $image->file_name,
      ];
    }

    return [
      'id' => $pages->id,
      'type' => $pages->type,
      'name' => $pages->name,
      'translations' => $pages->getTranslationsArray(),
      'images' => $images,
      'date' => $pages->created_at->format('d.m.Y'),
    ];
  }
}
