<?php

use Modules\Api\Http\Middleware\LocaleMiddleware;
use \Modules\Api\Http\Controllers\{
  BannersController,
  BranchesController,
  LangStringsController,
  ProductsController,
  BrandsController,
  SeoPagesController,
  ArticlesCategoryController,
  ArticlesController,
  PagesController,
  FaqsController,
  ContactController,
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api.')->group(function () {
  Route::domain(config('api.domain'))->group(function () {
    Route::middleware([LocaleMiddleware::class])->group(function () {
      Route::get('sliders', [BannersController::class, 'index']);
      Route::get('branches', [BranchesController::class, 'index'])->name('branches.index');
      Route::get('seo-page/{systemName}', [SeoPagesController::class, 'one'])->name('seo_pages.one');
      Route::get('string-translations', [LangStringsController::class, 'index'])->name('string_translations.index');
      Route::get('string-translations/hash', [LangStringsController::class, 'hash'])->name('string_translations.hash');
      Route::get('product', [ProductsController::class, 'slug'])->name('products.slug');
      Route::get('products/count', [ProductsController::class, 'count'])->name('products.count');
      Route::get('products', [ProductsController::class, 'index'])->name('products.index');

      Route::get('products/type', [ProductsController::class, 'type'])->name('products.type');
      Route::get('brands', [BrandsController::class, 'index'])->name('brands.index');

      Route::get('articles-category',
        [ArticlesCategoryController::class, 'index'])->name('articles_category.index');
      Route::get('articles', [ArticlesController::class, 'index'])->name('articles.index');
      Route::get('article', [ArticlesController::class, 'slug'])->name('articles.slug');

      Route::get('pages', [PagesController::class, 'index'])->name('pages.index');
      Route::get('page', [PagesController::class, 'slug'])->name('pages.slug');

      Route::get('faqs', [FaqsController::class, 'index'])->name('faqs.index');

      Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');
    });
  });
});
