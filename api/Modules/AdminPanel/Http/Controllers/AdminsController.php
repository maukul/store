<?php

namespace Modules\AdminPanel\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Modules\AdminPanel\Http\Requests\Admins\AdminsCreateRequest;
use Modules\AdminPanel\Http\Requests\Admins\AdminsEditRequest;
use Inertia\Response as InertiaResponse;
use Illuminate\Http\RedirectResponse;
use Modules\AdminPanel\Http\Resources\Admin\AdminResource;
use Modules\AdminPanel\ServicesDb\Admin\AdminService;
use Route;

class AdminsController extends BaseController
{
  /**
   * @var AdminService
   */
  protected $admins;

  /**
   * AdminsController constructor.
   *
   * @param  AdminService  $admins
   */
  public function __construct(
    AdminService $admins
  ) {
    parent::__construct();
    $this->admins = $admins;

    $this->seo()->setTitle(trans('adminpanel::admins.title.'.(optional(\Route::getCurrentRoute())->getActionMethod() ?? 'general')));
  }

  /**
   * Display a listing of the resource.
   *
   * @param  Request  $request
   * @return InertiaResponse
   */
  public function index(Request $request): InertiaResponse
  {
    $adminsData = $this->admins->search($request->all());
    $tableData = AdminResource::collection($adminsData);
    return Inertia::render('Admins/Index', [
      'tableData' => $tableData,
      'request' => $request->all(),
      'tableParams' => [
        'columnFilters' => $request->input('columnFilters', []),
        'sort' => $request->input('sort', ['field' => '', 'type' => '']),
        'page' => $tableData->resource->currentPage() ?? 1,
        'perPage' => $tableData->resource->perPage() ?? 10,
      ],
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return InertiaResponse
   */
  public function create(): InertiaResponse
  {
    return Inertia::render('Admins/Create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  AdminsCreateRequest  $request
   * @return RedirectResponse
   */
  public function store(AdminsCreateRequest $request): RedirectResponse
  {
    $model = $this->admins->create($request->getFormData());
    return redirect()->route('admin-panel.admins.edit', $model->id)
      ->with('message', trans('adminpanel::admins.message.success.store'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param $id
   * @return InertiaResponse
   */
  public function edit($id): InertiaResponse
  {
    $admin = $this->admins->getById((int) $id);
    $adminData = new AdminResource($admin);
    return Inertia::render('Admins/Edit', [
      'data' => $adminData,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  AdminsEditRequest  $request
   * @param $id
   * @return RedirectResponse
   */
  public function update(AdminsEditRequest $request, $id): RedirectResponse
  {
    $this->admins->update((int) $id, $request->getFormData());
    return redirect()->back()->with('message', trans('adminpanel::admins.message.success.update'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param $id
   * @return RedirectResponse
   */
  public function destroy($id): RedirectResponse
  {
    $this->admins->deleteById((int) $id);
    return redirect()->route('admin-panel.admins.index')
      ->with('message', trans('adminpanel::admins.message.success.destroy'));
  }
}
