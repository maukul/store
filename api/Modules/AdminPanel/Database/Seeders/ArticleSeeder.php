<?php

namespace Modules\AdminPanel\Database\Seeders;

use App\Models\Article\Article;
use App\Models\CategoriesArticles\CategoriesArticles;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $adminCount = 50;
    $this->command->getOutput()->progressStart($adminCount);
    Article::factory($adminCount)->create()->each(function (Article $article) {
      $article->categories()->sync(CategoriesArticles::inRandomOrder()->limit(random_int(1, 3))->pluck('id'));
      $article->recommend()->sync(Article::inRandomOrder()->limit(random_int(0, 5))->pluck('id'));
      $article->addMediaFromUrl('https://picsum.photos/500/500')->toMediaCollection('image_thumb', 'media');
      for ($i = 0; $i < random_int(1, 6); $i++) {
        $article->addMediaFromUrl('https://picsum.photos/700/700')->toMediaCollection('gallery', 'media');
      }
      $this->command->getOutput()->progressAdvance();
    });

    $this->command->getOutput()->progressFinish();
  }
}
