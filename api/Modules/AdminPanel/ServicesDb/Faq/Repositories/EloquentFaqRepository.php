<?php

namespace Modules\AdminPanel\ServicesDb\Faq\Repositories;

use App\Models\Faq\Faq;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\AdminPanel\ServicesDb\Faq\Repositories\FaqRepositoryInterface;
use Throwable;

/**
 * Class EloquentFaqRepository.
 */
class EloquentFaqRepository implements FaqRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    $sortType = $params['sort']['type'] ?? 'desc';
    if (!in_array($sortType, ['asc', 'desc'])) {
      $sortType = 'asc';
    }
    return Faq::where(function ($query) use ($params) {
      foreach ($params['columnFilters'] ?? [] as $name => $value) {
        $query->where($name, 'like', '%'.$value.'%');
      }
    })->orderByTranslation($params['sort']['field'] ?? 'name', $sortType)->paginate($params['perPage'] ?? 10);
  }

  /**
   * @param  int  $id
   * @return Faq
   */
  public function getById(int $id): Faq
  {
    return Faq::findOrFail($id);
  }

  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection
  {
    return Faq::get($columns);
  }

  /**
   * @param  array  $create
   * @param  array  $data
   * @return Faq
   * @throws Throwable
   */
  public function create(array $create, array $data = []): Faq
  {
    return DB::transaction(function () use ($create) {
      return Faq::create($create);
    });
  }

  /**
   * @param $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update($id, array $update, array $data): bool
  {
    $model = Faq::findOrFail($id);
    $model->update($update);

    return $model->save();
  }

  /**
   * @param  int  $id
   * @return bool
   * @throws Exception
   */
  public function deleteById($id): bool
  {
    $model = Faq::findOrFail($id);
    $model->delete();

    return true;
  }
}
