<?php

namespace Modules\Api\ServicesDb\Faq\Repositories;

use App\Models\Faq\Faq;
use Illuminate\Support\Collection;
use Modules\Api\ServicesDb\Faq\Repositories\FaqRepositoryInterface;

/**
 * Class EloquentFaqRepository.
 */
class EloquentFaqRepository implements FaqRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => string
   * ]
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $params, array $columns = ['*']): Collection
  {
    $modelFaq = Faq::query();
    $sort = $params['sort'] ?? '';
    if (!empty($sort)) {
      if (in_array($sort, (new Faq)->translatedAttributes)) {
        $modelFaq->orderByTranslation($sort);
      } else {
        $modelFaq->orderBy($sort);
      }
    }
    return $modelFaq->get($columns);
  }
}
