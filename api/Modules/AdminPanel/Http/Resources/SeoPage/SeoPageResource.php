<?php

namespace Modules\AdminPanel\Http\Resources\SeoPage;

use App\Models\SeoPage\SeoPage;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SeoPageResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $seoPages SeoPage */
    $seoPages = $this->resource;

    return [
      'id' => $seoPages->id,
      'system_name' => $seoPages->system_name,
      'name' => $seoPages->name,
      'translations' => $seoPages->getTranslationsArray(),
      'date' => $seoPages->created_at->format('d.m.Y'),
    ];
  }
}
