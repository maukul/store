<?php

namespace Modules\Api\Http\Resources\Faq;

use App\Models\Faq\Faq;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FaqResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $faq Faq */
    $faq = $this->resource;

    return [
      'id' => $faq->id,
      'position' => $faq->position,
      'name' => $faq->name,
      'question' => $faq->question,
      'reply' => $faq->reply,
    ];
  }
}
