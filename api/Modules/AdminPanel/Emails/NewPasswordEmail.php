<?php

namespace Modules\AdminPanel\Emails;

use App\Models\Admin;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewPasswordEmail extends Mailable implements ShouldQueue
{
  use Queueable, SerializesModels;

  /**
   * @var Admin
   */
  public $admin;
  public $password;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct(Admin $admin, string $password)
  {
    $this->admin = $admin;
    $this->password = $password;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build(): self
  {
    return $this->markdown('adminpanel::emails.new_password_email')
      ->with([
        'adminName' => $this->admin->name,
        'password' => $this->password,
      ]);
  }
}
