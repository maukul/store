<?php

namespace Modules\Api\ServicesDb\Banner\Repositories;

use Illuminate\Support\Collection;

/**
 * Interface BannerRepositoryInterface
 * @package Modules\Api\ServicesDb\Banner\Repositories
 */
interface BannerRepositoryInterface
{
  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection;
}
