<?php

namespace Modules\AdminPanel\ServicesDb\Banner\Repositories;

use App\Models\Banner\Banner;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface BannerRepositoryInterface
 * @package Modules\AdminPanel\ServicesDb\Banner\Repositories
 */
interface BannerRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator;

  /**
   * @param  int  $id
   * @return Banner
   */
  public function getById(int $id): Banner;

  /**
   * @param  array|string[]  $columns
   * @return Banner
   */
  public function getAll(array $columns = ['*']): Banner;

  /**
   * @param  array  $create
   * @return Banner
   */
  public function create(array $create, array $data): Banner;

  /**
   * @param  int  $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update(int $id, array $update, array $data): bool;

  /**
   * @param  int  $id
   * @return bool
   */
  public function deleteById(int $id): bool;
}
