<?php

namespace Modules\AdminPanel\ServicesDb\SeoPage\Repositories;

use App\Models\SeoPage\SeoPage;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface SeoPageRepositoryInterface
 * @package Modules\AdminPanel\ServicesDb\SeoPage\Repositories
 */
interface SeoPageRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator;

  /**
   * @param  array|string[]  $columns
   * @return SeoPage
   */
  public function getAll(array $columns = ['*']): SeoPage;

  /**
   * @param  int  $id
   * @return SeoPage
   */
  public function getById(int $id): SeoPage;

  /**
   * @param  array  $create
   * @param  array  $data
   * @return SeoPage
   */
  public function create(array $create, array $data): SeoPage;

  /**
   * @param  int  $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update(int $id, array $update, array $data): bool;

  /**
   * @param  int  $id
   * @return bool
   */
  public function deleteById(int $id): bool;
}
