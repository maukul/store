<?php

namespace Modules\AdminPanel\ServicesDb\Media\Repositories;

use Exception;
use Modules\AdminPanel\ServicesDb\Media\Repositories\MediaRepositoryInterface;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class EloquentMediaRepository.
 */
class EloquentMediaRepository implements MediaRepositoryInterface
{
  /**
   * @param  int  $id
   * @return bool
   * @throws Exception
   */
  public function deleteById(int $id): bool
  {
    $model = Media::findOrFail($id);
    $model->delete();

    return true;
  }
}
