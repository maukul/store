<?php

namespace Modules\AdminPanel\Http\Resources\LangString;

use App\Models\LangString\LangString;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LangStringResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $langStrings LangString */
    $langStrings = $this->resource;

    return [
      'id' => $langStrings->id,
      'system_name' => $langStrings->system_name,
      'value' => $langStrings->value,
      'translations' => $langStrings->getTranslationsArray(),
      'date' => $langStrings->created_at->format('d.m.Y'),
    ];
  }
}
