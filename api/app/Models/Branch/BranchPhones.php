<?php

namespace App\Models\Branch;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Branch\BranchPhones
 *
 * @property int $id
 * @property int $branch_id
 * @property string $phone Поле яке використовується в головній базі аналог dc000084.{IPN_1, IPN_2, IPN_3, IPN_4}
 * @property-read \App\Models\Branch\Branch $branch
 * @method static \Illuminate\Database\Eloquent\Builder|BranchPhones newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchPhones newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchPhones query()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchPhones whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchPhones whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchPhones wherePhone($value)
 * @mixin \Eloquent
 */
class BranchPhones extends Model
{
  /**
   * The timestamps.
   *
   * @var bool
   */
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'branch_phones';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'branch_id',
    'phone',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'branch_id' => 'integer',
    'phone' => 'string',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /**
   * @return BelongsTo<Branch>|Branch
   */
  public function branch(): BelongsTo
  {
    return $this->belongsTo(Branch::class);
  }

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
