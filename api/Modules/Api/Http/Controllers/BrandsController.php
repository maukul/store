<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Api\Http\Resources\Brand\BrandResource;
use Modules\Api\ServicesDb\Brand\BrandService;

class BrandsController extends BaseController
{
  /**
   * @var BrandService
   */
  protected $brands;

  /**
   * BrandsController constructor.
   * @param  BrandService  $brands
   */
  public function __construct(
    BrandService $brands
  ) {
    parent::__construct();
    $this->brands = $brands;
  }

  /**
   * @return AnonymousResourceCollection
   */
  public function index(Request $request): AnonymousResourceCollection
  {
    $brandsData = $this->brands->getAll($request->all());
    return BrandResource::collection($brandsData);
  }
}
