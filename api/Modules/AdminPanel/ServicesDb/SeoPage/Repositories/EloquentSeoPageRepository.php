<?php

namespace Modules\AdminPanel\ServicesDb\SeoPage\Repositories;

use App\Models\SeoPage\SeoPage;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Modules\AdminPanel\ServicesDb\SeoPage\Repositories\SeoPageRepositoryInterface;
use Throwable;

/**
 * Class EloquentSeoPageRepository.
 */
class EloquentSeoPageRepository implements SeoPageRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    $sortType = $params['sort']['type'] ?? 'desc';
    if (!in_array($sortType, ['asc', 'desc'])) {
      $sortType = 'asc';
    }
    return SeoPage::where(function ($query) use ($params) {
      foreach ($params['columnFilters'] ?? [] as $name => $value) {
        $query->where($name, 'like', '%'.$value.'%');
      }
    })->orderBy($params['sort']['field'] ?? 'system_name', $sortType)->paginate($params['perPage'] ?? 10);
  }

  /**
   * @param  int  $id
   * @return SeoPage
   */
  public function getById(int $id): SeoPage
  {
    return SeoPage::findOrFail($id);
  }

  /**
   * @param  array|string[]  $columns
   * @return SeoPage
   */
  public function getAll(array $columns = ['*']): SeoPage
  {
    return SeoPage::get($columns);
  }

  /**
   * @param  array  $create
   * @param  array  $data
   * @return SeoPage
   * @throws Throwable
   */
  public function create(array $create, array $data = []): SeoPage
  {
    return DB::transaction(function () use ($create) {
      return SeoPage::create($create);
    });
  }

  /**
   * @param $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update($id, array $update, array $data): bool
  {
    $model = SeoPage::findOrFail($id);
    $model->update($update);

    return $model->save();
  }

  /**
   * @param  int  $id
   * @return bool
   * @throws Exception
   */
  public function deleteById($id): bool
  {
    $model = SeoPage::findOrFail($id);
    $model->delete();

    return true;
  }
}
