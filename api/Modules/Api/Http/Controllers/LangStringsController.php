<?php

namespace Modules\Api\Http\Controllers;

use Modules\Api\ServicesDb\LangString\LangStringService;
use Illuminate\Http\JsonResponse;

class LangStringsController extends BaseController
{
  /**
   * @var LangStringService
   */
  protected $langStrings;

  /**
   * LangStringsController constructor.
   * @param  LangStringService  $langStrings
   */
  public function __construct(
    LangStringService $langStrings
  ) {
    parent::__construct();
    $this->langStrings = $langStrings;
  }

  /**
   * @return JsonResponse
   */
  public function index(): JsonResponse
  {
    $result = [];
    $langStringsData = $this->langStrings->getAll();
    foreach ($langStringsData as $langString) {
      $result[$langString->system_name] = $langString->value ?? '';
    }
    return response()->json(['data' => $result]);
  }

  /**
   * @return JsonResponse
   */
  public function hash(): JsonResponse
  {
    $hash = $this->langStrings->hash();

    return response()->json(['hash' => $hash]);
  }
}
