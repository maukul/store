<?php

namespace Modules\AdminPanel\ServicesDb\LangString;

use App\Models\LangString\LangString;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Modules\AdminPanel\ServicesDb\LangString\Repositories\LangStringRepositoryInterface;

class LangStringService
{
  /**
   * @var LangStringRepositoryInterface
   */
  protected $langStringRepository;

  /**
   * LangStringService constructor.
   * @param  LangStringRepositoryInterface  $langStringRepository
   */
  public function __construct(
    LangStringRepositoryInterface $langStringRepository
  ) {
    $this->langStringRepository = $langStringRepository;
  }

  /**
   * Get in tables langStrings.
   *
   * @param  array  $params
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    return $this->langStringRepository->search(
      array_merge(
        $params,
        []
      )
    );
  }

  /**
   * Get langString by id.
   *
   * @param  int  $id
   * @return LangString
   */
  public function getById(int $id): LangString
  {
    return $this->langStringRepository->getById($id);
  }

  /**
   * Get langString all.
   *
   * @param  array|string[]  $column
   * @return LangString
   */
  public function getAll(array $column = ['*']): LangString
  {
    return $this->langStringRepository->getAll($column);
  }

  /**
   * Store to DB if there are no errors.
   *
   * @param  array  $data
   * @return LangString
   */
  public function create($data): LangString
  {
    try {
      $create = [
        'system_name' => Str::slug($data['system_name'], '_'),
      ];
      foreach (config('translatable.locales') as $locale) {
        $create[$locale]['value'] = $data['value'][$locale];
      }

      $result = $this->langStringRepository->create($create, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to create langString data');
      }
    }

    return $result;
  }

  /**
   * Update langString data
   * Store to DB if there are no errors.
   *
   * @param $id
   * @param $data
   * @return bool
   */
  public function update($id, $data): bool
  {
    $langString = false;
    DB::beginTransaction();

    try {
      $update = [
        'system_name' => Str::slug($data['system_name'], '_'),
      ];
      foreach (config('translatable.locales') as $locale) {
        $update[$locale]['value'] = $data['value'][$locale];
      }

      $langString = $this->langStringRepository->update($id, $update, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to update langString data');
      }
    }

    DB::commit();

    return $langString;
  }

  /**
   * Delete langString by id.
   *
   * @param $id
   * @return bool
   */
  public function deleteById($id): bool
  {
    $langString = false;
    DB::beginTransaction();

    try {
      $langString = $this->langStringRepository->deleteById($id);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to delete langString data');
      }
    }

    DB::commit();

    return $langString;
  }
}
