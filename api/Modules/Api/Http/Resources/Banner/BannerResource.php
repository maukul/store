<?php

namespace Modules\Api\Http\Resources\Banner;

use App;
use App\Models\Banner\Banner;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $banner Banner */
    $banner = $this->resource;

    return [
      'type' => $banner->type,
      'position' => $banner->position,
      'name' => $banner->name,
      'image_url' => $banner->getFirstMediaUrl('main-'.App::getLocale()),
    ];
  }
}
