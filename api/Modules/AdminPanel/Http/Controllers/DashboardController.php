<?php

namespace Modules\AdminPanel\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response as InertiaResponse;
use Route;

class DashboardController extends BaseController
{
  /**
   * DashboardController constructor.
   */
  public function __construct()
  {
    parent::__construct();

    $this->seo()->setTitle(trans('adminpanel::dashboard.title.'.(optional(Route::getCurrentRoute())->getActionMethod() ?? 'general')));
  }

  /**
   * Display a listing of the resource.
   *
   * @param  Request  $request
   * @return InertiaResponse
   */
  public function index(Request $request): InertiaResponse
  {
    return Inertia::render('Dashboard/Index');
  }
}
