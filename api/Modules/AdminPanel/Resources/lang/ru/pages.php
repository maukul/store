<?php
return [
  'title' => [
    'general' => 'Cтраницы',
    'index' => 'Список страниц',
    'create' => 'Создать страницу',
    'edit' => 'Редактировать страницу',
  ],
  'form' => [
    'type' => [
      'title' => 'Системная название',
      'placeholder' => 'Системная название',
    ],
    'name' => [
      'title' => 'Название',
      'placeholder' => 'Название',
    ],
    'title' => [
      'title' => 'Мета тайтл',
      'placeholder' => 'Мета тайтл',
    ],
    'keywords' => [
      'title' => 'Мета ключевые слова',
      'placeholder' => 'Мета ключевые слова',
    ],
    'description' => [
      'title' => 'Мета описание',
      'placeholder' => 'Мета описание',
    ],
    'slug' => [
      'title' => 'Url',
      'placeholder' => 'Url',
    ],
    'value' => [
      'description' => [
        'title' => 'Описание',
        'placeholder' => 'Описание',
      ],
      'phones' => [
        'title' => 'Телефон',
        'placeholder' => 'Телефон',
      ],
      'email' => [
        'title' => 'E-mail',
        'placeholder' => 'E-mail',
      ],
      'address' => [
        'title' => 'Адрес',
        'placeholder' => 'Адрес',
      ],
      'time_work' => [
        'title' => 'Час роботи',
        'time' => [
          'title' => 'Час',
          'placeholder' => 'Час',
        ],
        'day' => [
          'title' => 'Дні',
          'placeholder' => 'Дні',
        ],
      ],
      'map' => [
        'title' => 'Карта',
        'placeholder' => 'Карта',
      ],
      'bloc-statistics' => [
        'title' => 'Блок статистики',
        'placeholder' => 'Блок статистики',
      ],
    ],
  ],
  'table' => [
    'columns' => [
      'name' => 'Название',
    ],
  ],
  'message' => [
    'success' => [
      'store' => 'Создание страницу прошло успешно.',
      'update' => 'Редактирование страницу прошло успешно.',
      'destroy' => 'Удаление страницы прошло успешно.',
    ],
  ],
  'text' => [
    'help-bloc-statistics' => 'Добавлення блоку з статистикою можна через якір ##bloc-statistics##'
  ]
];
