<?php

namespace Modules\AdminPanel\ServicesDb\Article;

use App\Models\Article\Article;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Modules\AdminPanel\ServicesDb\Article\Repositories\ArticleRepositoryInterface;

class ArticleService
{
  /**
   * @var ArticleRepositoryInterface
   */
  protected $articleRepository;

  /**
   * ArticleService constructor.
   * @param  ArticleRepositoryInterface  $articleRepository
   */
  public function __construct(
    ArticleRepositoryInterface $articleRepository
  ) {
    $this->articleRepository = $articleRepository;
  }

  /**
   * Get in tables articles.
   *
   * @param  array  $params
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    return $this->articleRepository->search(
      array_merge(
        $params,
        []
      )
    );
  }

  /**
   * Get article by id.
   *
   * @param  int  $id
   * @return Article
   */
  public function getById(int $id): Article
  {
    return $this->articleRepository->getById($id);
  }

  /**
   * Get article all.
   *
   * @param  array|string[]  $column
   * @return Collection
   */
  public function getAll(array $column = ['*']): Collection
  {
    return $this->articleRepository->getAll($column);
  }

  /**
   * Store to DB if there are no errors.
   *
   * @param  array  $data
   * @return Article
   */
  public function create($data): Article
  {
    try {
      $create = [];
      foreach (config('translatable.locales') as $locale) {
        $create[$locale]['name'] = $data['name'][$locale];
        $create[$locale]['title'] = $data['title'][$locale];
        $create[$locale]['keywords'] = $data['keywords'][$locale];
        $create[$locale]['description'] = $data['description'][$locale];
        $create[$locale]['content'] = $data['content'][$locale];
        if (!empty($data['slug'][$locale])) {
          $create[$locale]['slug'] = Str::slug($data['slug'][$locale], '-');
        }
      }

      $result = $this->articleRepository->create($create, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to create article data');
      }
    }

    return $result;
  }

  /**
   * Update article data
   * Store to DB if there are no errors.
   *
   * @param $id
   * @param $data
   * @return bool
   */
  public function update($id, $data): bool
  {
    $article = false;
    DB::beginTransaction();

    try {
      $update = [];
      foreach (config('translatable.locales') as $locale) {
        $update[$locale]['name'] = $data['name'][$locale];
        $update[$locale]['title'] = $data['title'][$locale];
        $update[$locale]['keywords'] = $data['keywords'][$locale];
        $update[$locale]['description'] = $data['description'][$locale];
        $update[$locale]['content'] = $data['content'][$locale];
        if (!empty($data['slug'][$locale])) {
          $update[$locale]['slug'] = Str::slug($data['slug'][$locale], '-');
        }
      }

      $article = $this->articleRepository->update($id, $update, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to update article data');
      }
    }

    DB::commit();

    return $article;
  }

  /**
   * Delete article by id.
   *
   * @param $id
   * @return bool
   */
  public function deleteById($id): bool
  {
    $article = false;
    DB::beginTransaction();

    try {
      $article = $this->articleRepository->deleteById($id);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to delete article data');
      }
    }

    DB::commit();

    return $article;
  }
}
