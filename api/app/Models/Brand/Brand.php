<?php

namespace App\Models\Brand;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Brand\Brand
 *
 * @property int $id
 * @property int $out_id Id яке використовується в головній базі аналог dc000091.kodkli
 * @property bool $show Поле яке показує чи показувати бренд в магазині в головній базі аналог dc000091.inet_shop
 * @property bool $show_label Поле яке показує чи показувати бренд (лого) на товарі в головній базі аналог dc000091.ntm
 * @property int $main_position Поле яке показує позицію бренда на головній сторінці в головній базі аналог dc000091.fix
 * @property bool $show_main Поле яке показує чи показувати бренд на головній сторінці в головній базі аналог dc000091.main
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Brand\BrandTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Brand\BrandTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Brand listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Brand query()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand translated()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereMainPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereOutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereShowLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereShowMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand withTranslation()
 * @mixin \Eloquent
 */
class Brand extends Model implements TranslatableContract
{
  use Translatable;

  /**
   * The translated attributes that are mass assignable.
   *
   * @var array
   */
  public $translatedAttributes = [
    'name',
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'brands';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'out_id',
    'show',
    'show_label',
    'main_position',
    'show_main',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'out_id' => 'integer',
    'show' => 'boolean',
    'show_label' => 'boolean',
    'main_position' => 'integer',
    'show_main' => 'boolean',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
