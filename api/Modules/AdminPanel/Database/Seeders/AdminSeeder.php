<?php

namespace Modules\AdminPanel\Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    if (Admin::find(1) === null) {
      $admin = new Admin([
        'name' => env('INITIAL_USER_NAME'),
        'email' => env('INITIAL_USER_EMAIL'),
        'password' => env('INITIAL_USER_PASSWORDHASH'),
      ]);
      $admin->save();
    }
    $adminCount = 10;
    $this->command->getOutput()->progressStart($adminCount);
    Admin::factory($adminCount)->create()->each(function () {
      $this->command->getOutput()->progressAdvance();
    });

    $this->command->getOutput()->progressFinish();
  }
}
