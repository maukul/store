<?php

namespace Modules\AdminPanel\ServicesDb\CategoriesArticles\Repositories;

use App\Models\CategoriesArticles\CategoriesArticles;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\AdminPanel\ServicesDb\CategoriesArticles\Repositories\CategoriesArticlesRepositoryInterface;
use Throwable;

/**
 * Class EloquentCategoriesArticlesRepository.
 */
class EloquentCategoriesArticlesRepository implements CategoriesArticlesRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    $sortType = $params['sort']['type'] ?? 'desc';
    if (!in_array($sortType, ['asc', 'desc'])) {
      $sortType = 'asc';
    }
    return CategoriesArticles::where(function ($query) use ($params) {
      foreach ($params['columnFilters'] ?? [] as $name => $value) {
        $query->where($name, 'like', '%'.$value.'%');
      }
    })->orderByTranslation($params['sort']['field'] ?? 'name', $sortType)->paginate($params['perPage'] ?? 10);
  }

  /**
   * @param  int  $id
   * @return CategoriesArticles
   */
  public function getById(int $id): CategoriesArticles
  {
    return CategoriesArticles::findOrFail($id);
  }

  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection
  {
    return CategoriesArticles::get($columns);
  }

  /**
   * @param  array  $create
   * @param  array  $data
   * @return CategoriesArticles
   * @throws Throwable
   */
  public function create(array $create, array $data = []): CategoriesArticles
  {
    return DB::transaction(function () use ($create) {
      return CategoriesArticles::create($create);
    });
  }

  /**
   * @param $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update($id, array $update, array $data): bool
  {
    $model = CategoriesArticles::findOrFail($id);
    $model->update($update);

    return $model->save();
  }

  /**
   * @param  int  $id
   * @return bool
   * @throws Exception
   */
  public function deleteById($id): bool
  {
    $model = CategoriesArticles::findOrFail($id);
    $model->delete();

    return true;
  }
}
