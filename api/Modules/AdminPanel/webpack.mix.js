const mix = require('laravel-mix')
const ChunkRenamePlugin = require('webpack-chunk-rename-plugin')
const path = require('path')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    output: {
        publicPath: '/admin/dist/',
        filename: '[name].js',
        chunkFilename: 'js/chunks/[name].js?id=[chunkhash]',
    },
    plugins: [
        new ChunkRenamePlugin({
            initialChunksWithEntry: true,
            '/admin/dist/js': '/admin/dist/js'
        }),
    ],
    devtool: 'source-map',
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'Resources'),
      }
    },
  })
  .setPublicPath('../../public/admin/dist/')

mix.js('Resources/js/app.js', 'js/app.js').vue().extract()

mix.combine([
    __dirname + '/Resources/assets/app/vendors/css/vendors.min.css',
    __dirname + '/Resources/assets/app/css/bootstrap.css',
    __dirname + '/Resources/assets/app/css/bootstrap-extended.css',
    __dirname + '/Resources/assets/app/css/colors.css',
    __dirname + '/Resources/assets/app/css/components.css',
    __dirname + '/Resources/assets/app/css/themes/dark-layout.css',
    __dirname + '/Resources/assets/app/css/themes/bordered-layout.css',
    __dirname + '/Resources/assets/app/css/themes/semi-dark-layout.css',

    __dirname + '/Resources/assets/app/css/core/menu/menu-types/vertical-menu.css',

    __dirname + '/Resources/assets/css/style.css',
], '../../public/admin/dist/css/all.css')

mix.combine([
    __dirname + '/Resources/assets/app/vendors/js/vendors.min.js',

    __dirname + '/Resources/assets/app/js/core/app-menu.js',
    __dirname + '/Resources/assets/app/js/core/app.js',
], '../../public/admin/dist/js/all.js')

// ------------------------------------------------
// If you are deploying on subdomain/subfolder. Uncomment below code before running 'yarn prod' or 'npm run production' command.
// Please Change below 'publicPath' and 'setResourceRoot' options as per your sub-directory path. We have kept our current live demo options which is deployed in sub-folder.
// ------------------------------------------------

// mix.version()
// mix.sourceMaps()
mix.disableNotifications();
