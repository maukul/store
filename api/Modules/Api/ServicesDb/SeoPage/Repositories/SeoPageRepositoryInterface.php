<?php

namespace Modules\Api\ServicesDb\SeoPage\Repositories;

use App\Models\SeoPage\SeoPage;

/**
 * Interface SeoPageRepositoryInterface
 * @package Modules\Api\ServicesDb\SeoPage\Repositories
 */
interface SeoPageRepositoryInterface
{
  /**
   * @param  string  $systemName
   * @return SeoPage
   */
  public function getBySystemName(string $systemName): SeoPage;
}
