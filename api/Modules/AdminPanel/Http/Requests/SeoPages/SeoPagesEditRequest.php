<?php

namespace Modules\AdminPanel\Http\Requests\SeoPages;

use Illuminate\Validation\Rule;
use Modules\AdminPanel\Http\Requests\FormRequest;

class SeoPagesEditRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize(): bool
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array
  {
    return [
      'system_name' => [
        'required',
        'string',
      ],
      'name.*' => [
        'required',
        'string',
        'max:90',
      ],
      'title.*' => [
        'required',
        'string',
        'max:255',
      ],
      'keywords.*' => [
        'nullable',
        'string',
        'max:255',
      ],
      'description.*' => [
        'nullable',
        'string',
        'max:255',
      ],
    ];
  }

  /**
   * Get custom messages for validator errors.
   *
   * @return array
   */
  public function messages(): array
  {
    return [];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes(): array
  {
    $return = [];

    $return['system_name'] = trans('adminpanel::seo_pages.form.system_name.title');

    foreach (config('translatable.locales') as $item) {
      $return['name.'.$item] = trans('adminpanel::seo_pages.form.name.title').' ('.$item.')';
      $return['title.'.$item] = trans('adminpanel::seo_pages.form.title.title').' ('.$item.')';
      $return['keywords.'.$item] = trans('adminpanel::seo_pages.form.keywords.title').' ('.$item.')';
      $return['description.'.$item] = trans('adminpanel::seo_pages.form.description.title').' ('.$item.')';
    }
    return $return;
  }
}
