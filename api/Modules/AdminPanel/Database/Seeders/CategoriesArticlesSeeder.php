<?php

namespace Modules\AdminPanel\Database\Seeders;

use App\Models\CategoriesArticles\CategoriesArticles;
use Illuminate\Database\Seeder;

class CategoriesArticlesSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $categoriesArticlesCount = 5;
    $this->command->getOutput()->progressStart($categoriesArticlesCount);
    CategoriesArticles::factory($categoriesArticlesCount)->create()->each(function () {
      $this->command->getOutput()->progressAdvance();
    });
    $this->command->getOutput()->progressFinish();
  }
}
