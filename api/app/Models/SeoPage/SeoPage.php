<?php

namespace App\Models\SeoPage;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SeoPage\SeoPage
 *
 * @property int $id
 * @property string $system_name Поле системної назви
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\SeoPage\SeoPageTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SeoPage\SeoPageTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage query()
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage translated()
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage whereSystemName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPage withTranslation()
 * @mixin \Eloquent
 */
class SeoPage extends Model implements TranslatableContract
{
  use Translatable;

  /**
   * The translated attributes that are mass assignable.
   *
   * @var array
   */
  public $translatedAttributes = [
    'name',
    'title',
    'keywords',
    'description',
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'seo_pages';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'system_name',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'system_name' => 'string',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
