<?php

namespace Modules\AdminPanel\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Modules\AdminPanel\Http\Requests\Faqs\FaqsCreateRequest;
use Modules\AdminPanel\Http\Requests\Faqs\FaqsEditRequest;
use Inertia\Response as InertiaResponse;
use Illuminate\Http\RedirectResponse;
use Modules\AdminPanel\Http\Resources\Faq\FaqResource;
use Modules\AdminPanel\ServicesDb\Faq\FaqService;
use Route;

class FaqsController extends BaseController
{
  /**
   * @var FaqService
   */
  protected $faqs;

  /**
   * FaqsController constructor.
   *
   * @param  FaqService  $faqs
   */
  public function __construct(
    FaqService $faqs
  ) {
    parent::__construct();
    $this->faqs = $faqs;

    $this->seo()->setTitle(trans('adminpanel::faqs.title.'.(optional(Route::getCurrentRoute())->getActionMethod() ?? 'general')));
  }

  /**
   * Display a listing of the resource.
   *
   * @param  Request  $request
   * @return InertiaResponse
   */
  public function index(Request $request): InertiaResponse
  {
    $faqsData = $this->faqs->search($request->all());
    $tableData = FaqResource::collection($faqsData);
    return Inertia::render('Faqs/Index', [
      'tableData' => $tableData,
      'request' => $request->all(),
      'tableParams' => [
        'columnFilters' => $request->input('columnFilters', []),
        'sort' => $request->input('sort', ['field' => '', 'type' => '']),
        'page' => $tableData->resource->currentPage() ?? 1,
        'perPage' => $tableData->resource->perPage() ?? 10,
      ],
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return InertiaResponse
   */
  public function create(): InertiaResponse
  {
    return Inertia::render('Faqs/Create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  FaqsCreateRequest  $request
   * @return RedirectResponse
   */
  public function store(FaqsCreateRequest $request): RedirectResponse
  {
    $model = $this->faqs->create($request->getFormData());
    return redirect()->route('admin-panel.page-faqs.edit', $model->id)
      ->with('message', trans('adminpanel::faqs.message.success.store'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param $id
   * @return InertiaResponse
   */
  public function edit($id): InertiaResponse
  {
    $faqs = $this->faqs->getById((int) $id);
    $faqsData = new FaqResource($faqs);
    return Inertia::render('Faqs/Edit', [
      'data' => $faqsData,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  FaqsEditRequest  $request
   * @param $id
   * @return RedirectResponse
   */
  public function update(FaqsEditRequest $request, $id): RedirectResponse
  {
    $this->faqs->update((int) $id, $request->getFormData());
    return redirect()->back()->with('message', trans('adminpanel::faqs.message.success.update'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param $id
   * @return RedirectResponse
   */
  public function destroy($id): RedirectResponse
  {
    $this->faqs->deleteById((int) $id);
    return redirect()->route('admin-panel.page-faqs.index')
      ->with('message', trans('adminpanel::faqs.message.success.destroy'));
  }
}
