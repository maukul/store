<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLangStringsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('lang_strings', function (Blueprint $table) {
      $table->id();
      $table->string('system_name');
      $table->timestamps();
    });

    Schema::create('lang_string_translations', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('lang_string_id')->unsigned();
      $table->string('locale')->index();
      $table->string('value');

      $table->unique(['lang_string_id', 'locale']);
      $table->foreign('lang_string_id')->references('id')->on('lang_strings')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('lang_string_translations');
    Schema::dropIfExists('lang_strings');
  }
}
