<?php

return [
  'name' => 'AdminPanel',
  'domain' => env('APP_ADMIN_PANEL_DOMAIN')
];
