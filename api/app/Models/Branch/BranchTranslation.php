<?php

namespace App\Models\Branch;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Branch\BranchTranslation
 *
 * @property int $id
 * @property int $branch_id
 * @property string $locale
 * @property string $name Поле яке використовується в головній базі аналог dc000084.{ukrname, runame, engname} в залежності від поля locale
 * @method static \Illuminate\Database\Eloquent\Builder|BranchTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchTranslation whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchTranslation whereName($value)
 * @mixin \Eloquent
 */
class BranchTranslation extends Model
{
  /**
   * The timestamps.
   *
   * @var bool
   */
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'branch_translations';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
