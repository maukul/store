<?php

namespace Modules\Api\ServicesDb\SeoPage;

use App\Models\SeoPage\SeoPage;
use Modules\Api\ServicesDb\SeoPage\Repositories\SeoPageRepositoryInterface;

class SeoPageService
{
  /**
   * @var SeoPageRepositoryInterface
   */
  protected $seoPageRepository;

  /**
   * SeoPageService constructor.
   * @param  SeoPageRepositoryInterface  $seoPageRepository
   */
  public function __construct(
    SeoPageRepositoryInterface $seoPageRepository
  ) {
    $this->seoPageRepository = $seoPageRepository;
  }

  /**
   * Get SeoPage get system name.
   *
   * @param  string  $systemName
   * @return SeoPage
   */
  public function getBySystemName(string $systemName): SeoPage
  {
    return $this->seoPageRepository->getBySystemName($systemName);
  }
}
