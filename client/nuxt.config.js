require('dotenv').config()

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'client',
    htmlAttrs: {
      lang: 'ua'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no'},
      {name: 'msapplication-TileColor', content: '#da532c'},
      {name: 'msapplication-config', content: 'favicon/browserconfig.xml'},
      {name: 'theme-color', content: '#24ABE2'},
    ],
    link: [
      {rel: 'apple-touch-icon', sizes: '180x180', href: '/favicon/apple-touch-icon.png'},
      {rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon/favicon-32x32.png'},
      {rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon/favicon-16x16.png'},
      {rel: 'manifest', href: '/favicon/site.webmanifest'},
      {rel: 'mask-icon', href: '/favicon/safari-pinned-tab.svg', color: '#5bbad5'},
      {rel: 'shortcut icon', href: '/favicon/favicon.ico'},

    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/scss/main.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/vue-bus.js',
    '~/plugins/main.js',
    '~/plugins/bootstrap-vue.js',
    '~/plugins/vue-mask.js',
    '~/plugins/vue-scrollto.js',
    '~/plugins/vue-scrollactive.js',
    {src: '~/plugins/vue-form-slider.js', mode: 'client'},

    {src: '~/plugins/vue-tiny-slider.js', mode: 'client'},
    '~/plugins/vue-slick-carousel.js',

    '~/plugins/i18n.js',
    '~/plugins/axios.js',
    '~/plugins/lodash.js',
    '~/plugins/vue-backtotop-ssr.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/composition-api/module',
    '@nuxtjs/google-analytics',
    '@nuxtjs/recaptcha', {
      // hideBadge: Boolean, // Hide badge element (v3 & v2 via size=invisible)
      // language: String,   // Recaptcha language (v2)
      siteKey: '6Lc4CwMcAAAAAAdMFbKoREVD_WIwXdJTLuaWt-JC',    // Site key for requests
      // version: Number,     // Version
      // size: String        // Size: 'compact', 'normal', 'invisible' (v2)
    }
  ],

  googleAnalytics: {
    id: 'G-1TJSVPN7P1'
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    //https://i18n.nuxtjs.org/options-reference/
    'nuxt-i18n',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',

    // 'bootstrap-vue/nuxt',

    'cookie-universal-nuxt',
    'nuxt-leaflet',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.API_URL,
    debug: true,
  },

  // I18n module configuration: https://i18n.nuxtjs.org
  i18n: {
    locales: [
      {
        name: 'Рус',
        code: 'ru',
        file: 'ru.js'
      },
      {
        name: 'Укр',
        code: 'ua',
        file: 'ua.js'
      },
      {
        name: 'Eng',
        code: 'en',
        file: 'en.js'
      },
    ],
    lazy: true,
    langDir: 'lang/',
    defaultLocale: 'ua'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      '@vueform/slider'
    ],
  },
}
