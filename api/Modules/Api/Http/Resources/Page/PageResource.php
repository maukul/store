<?php

namespace Modules\Api\Http\Resources\Page;

use App\Models\Page\Page;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $page Page */
    $page = $this->resource;

    $result = [
      'type' => $page->type,
      'name' => $page->name,
      'title' => $page->title,
      'keywords' => $page->keywords,
      'description' => $page->description,
      'slug' => $page->slug,
    ];
    return $this->getContentByType($result, $page->type);
  }

  private function getContentByType($result, $type)
  {
    $page = $this->resource;

    if ($type == 'about') {
      $imagesGallery = [];
      foreach ($page->getMedia('images_gallery') as $image) {
        $imagesGallery[] = [
          'url_original' => $image->getFullUrl(),
        ];
      }
      $result['imagesGallery'] = $imagesGallery;
      $result['blocStatistics'] = $page->value['bloc-statistics'];

      $blocHtml = '<div class="row about-images">';
      foreach ($page->value['bloc-statistics'] as $item) {
        $blocHtml .= '<div class="col-6 numbers-list__item">';
        $blocHtml .= '<div class="number-block">';
        $blocHtml .= '<div class="number-block__item number-block__name">'.$item[0].'</div>';
        $blocHtml .= '<div class="number-block__item number-block__value">'.$item[1].'</div>';
        $blocHtml .= '</div>';
        $blocHtml .= '</div>';
      }
      $blocHtml .= '</dev>';

      $result['content'] = str_replace("", $blocHtml, $page->value['description']);
    } elseif ($type == 'home') {
      $result['content'] = $page->value['description'];
    } elseif ($type == 'blogs') {
      $result['content'] = $page->value['description'];
    } elseif ($type == 'contact') {
      $result['phones'] = $page->value['phones'];
      $result['emails'] = $page->value['email'];
      $result['address'] = $page->value['address'];
      $result['time_work'] = $page->value['time_work'];
      $result['map'] = $page->value['map'];
    }

    return $result;
  }
}
