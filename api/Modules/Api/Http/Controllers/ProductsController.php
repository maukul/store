<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Api\Http\Resources\Product\ProductResource;
use Modules\Api\ServicesDb\Product\ProductService;

class ProductsController extends BaseController
{
  /**
   * @var ProductService
   */
  protected $products;

  /**
   * ProductsController constructor.
   * @param  ProductService  $products
   */
  public function __construct(
    ProductService $products
  ) {
    parent::__construct();
    $this->products = $products;
  }

  /**
   * @param  Request  $request
   * @return AnonymousResourceCollection
   */
  public function index(Request $request): AnonymousResourceCollection
  {
    $productsData = $this->products->search($request->all());
    return ProductResource::collection($productsData);
  }

  /**
   * @param  Request  $request
   * @return JsonResponse
   */
  public function count(Request $request): JsonResponse
  {
    $productsData = $this->products->count($request->all());
    return response()->json(['data' => $productsData]);
  }

  /**
   * @param  Request  $request
   * @return ProductResource
   */
  public function slug(Request $request): ProductResource
  {
    $productsData = $this->products->slug($request->all());
    return new ProductResource($productsData);
  }

  /**
   * @param  Request  $request
   * @return AnonymousResourceCollection
   */
  public function type(Request $request): AnonymousResourceCollection
  {
    $productsData = $this->products->type($request->all());
    return ProductResource::collection($productsData);
  }
}
