<?php

namespace Modules\AdminPanel\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Modules\AdminPanel\ServicesDb\Media\MediaService;
use Illuminate\Http\Request;
use Storage;

class MediaController extends BaseController
{
  /**
   * @var MediaService
   */
  protected $media;

  /**
   * MediaController constructor.
   *
   * @param  MediaService  $media
   */
  public function __construct(
    MediaService $media
  ) {
    parent::__construct();

    $this->media = $media;
  }

  /**
   * Add the specified resource from storage.
   *
   * @param  Request  $request
   * @return JsonResponse
   */
  public function store(Request $request): JsonResponse
  {
    $this->validate($request, [
      'image' => ['required', 'image', 'mimes:jpg,jpeg,bmp,png'],
    ]);

    $image = $request->file('image');
    $file = $image->storeAs('public/uploads', $image->hashName());

    return response()->json(['location' => Storage::url($file)]);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param $id
   * @return RedirectResponse
   */
  public function destroy($id): RedirectResponse
  {
    $this->media->deleteById((int) $id);
    return redirect()->back()->with('message', trans('adminpanel::media.message.success.destroy'));
  }
}
