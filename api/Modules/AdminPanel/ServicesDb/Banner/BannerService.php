<?php

namespace Modules\AdminPanel\ServicesDb\Banner;

use App\Models\Banner\Banner;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use Modules\AdminPanel\ServicesDb\Banner\Repositories\BannerRepositoryInterface;

class BannerService
{
  /**
   * @var BannerRepositoryInterface
   */
  protected $bannerRepository;

  /**
   * BannerService constructor.
   * @param  BannerRepositoryInterface  $bannerRepository
   */
  public function __construct(
    BannerRepositoryInterface $bannerRepository
  ) {
    $this->bannerRepository = $bannerRepository;
  }

  /**
   * Get in tables banners.
   * @param  array  $params
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    return $this->bannerRepository->search(
      array_merge(
        $params,
        []
      )
    );
  }

  /**
   * Get banner by id.
   *
   * @param $id
   * @return Banner
   */
  public function getById($id): Banner
  {
    return $this->bannerRepository->getById($id);
  }

  /**
   * Get banner all.
   *
   * @param $id
   * @return Banner
   */
  public function getAll(array $column = ['*']): Banner
  {
    return $this->bannerRepository->getAll($column);
  }

  /**
   * Store to DB if there are no errors.
   *
   * @param  array  $data
   * @return Banner
   */
  public function create($data): Banner
  {
    $result = false;
    try {
      $create = [
        'type' => $data['type'],
        'position' => $data['position'],
      ];
      foreach (config('translatable.locales') as $locale) {
        $create[$locale]['name'] = $data['name'][$locale];
        $create[$locale]['url'] = $data['url'][$locale];
      }

      $result = $this->bannerRepository->create($create, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to create banner data');
      }
    }

    return $result;
  }

  /**
   * Update banner data
   * Store to DB if there are no errors.
   *
   * @param  array  $data
   * @return bool
   */
  public function update($id, $data): bool
  {
    $banner = false;

    DB::beginTransaction();

    try {
      $update = [
        'type' => $data['type'],
        'position' => $data['position'],
      ];
      foreach (config('translatable.locales') as $locale) {
        $update[$locale]['name'] = $data['name'][$locale];
        $update[$locale]['url'] = $data['url'][$locale];
      }

      $banner = $this->bannerRepository->update($id, $update, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to update banner data');
      }
    }

    DB::commit();

    return $banner;
  }

  /**
   * Delete banner by id.
   *
   * @param $id
   * @return bool
   */
  public function deleteById($id): bool
  {
    $banner = false;

    DB::beginTransaction();

    try {
      $banner = $this->bannerRepository->deleteById($id);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to delete banner data');
      }
    }

    DB::commit();

    return $banner;
  }
}
