<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesInRecommendedArticlesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('articles_in_recommended_articles', function (Blueprint $table) {
      // Таблиця категорий для статей блога
      $table->id();
      $table->bigInteger('article_id')->unsigned();
      $table->bigInteger('recommended_article_id')->unsigned();
      $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
      $table->foreign('recommended_article_id')->references('id')->on('articles')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('articles_in_recommended_articles');
  }
}
