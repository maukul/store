<?php

namespace App\Services\MainDatabase\Tables;

use App\Models\Product\Product;
use App\Services\MainDatabase\BaseMainDatabase;
use Illuminate\Support\Facades\Http;

/**
 * Class ProductRelatedTable
 * @package App\Services\MainDatabase\Tables
 */
final class ProductRelatedTable extends BaseMainDatabase
{
  /**
   * ProductAnalogTable constructor.
   *
   * @param  int  $product_id
   * @param  int|null  $product_out_id
   */
  public function __construct(int $product_id, int $product_out_id = null)
  {
    $this->select([
      'product_id' => $product_id,
      'product_out_id' => $product_out_id,
    ]);
  }

  /**
   * The function select
   *
   * @param  array  $params  = [
   *     'product_id' => $product_id,
   *     'product_out_id' => $product_out_id,
   * ]
   * @return bool
   */
  protected function select(array $params = []): bool
  {
    $product_id = $params['product_id'] ?? null;
    $product_out_id = $params['product_out_id'] ?? null;

    if (empty($product_out_id)) {
      $product = Product::find($product_id);
      $product_out_id = $product->out_id;
    }

    $response = Http::withBasicAuth(config('main_database.username'), config('main_database.password'))
      ->get(config('main_database.url').'statement=execute procedure IM_product_related('.$product_out_id.')');

    $this->originalDate = $response->json()['d']['results'];
    $data = [];
    foreach ($response->json()['d']['results'] as $item) {
      $return = [];

      $return['product_id'] = $product_id;
      $option = Product::where('out_id', '=', $item['kodkli'])->first();
      $return['related_id'] = $option->id ?? null;

      if (!empty($return['product_id']) && !empty($return['related_id'])) {
        $data[] = $return;
      }
    }
    $this->data = $data;
    return true;
  }
}
