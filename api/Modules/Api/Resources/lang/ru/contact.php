<?php
return [
  'form' => [
    'name' => [
      'title' => 'Имя',
    ],
    'phone' => [
      'title' => 'Телефон',
    ],
    'email' => [
      'title' => 'Почта',
    ],
    'message' => [
      'title' => 'Повідомлення',
    ],
  ]
];
