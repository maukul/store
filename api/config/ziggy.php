<?php

return [
  'only' => ['admin-panel.*'],
  'except' => ['_debugbar.*', 'api.*'],
  'groups' => [
    'admin' => ['admin-panel.*'],
  ],
];
