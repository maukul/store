<?php

namespace App\Services\MainDatabase\Tables;

use App\Models\Brand\Brand;
use App\Services\MainDatabase\BaseMainDatabase;
use Illuminate\Support\Facades\Http;

/**
 * Class ProductTable
 * @package App\Services\MainDatabase\Tables
 */
final class ProductTable extends BaseMainDatabase
{
  /**
   * ProductTable constructor.
   *
   * @param  bool  $image
   */
  public function __construct(bool $image = false)
  {
    $this->select([
      'image' => false
    ]);
  }

  /**
   * The function select
   *
   * @param  array  $params  = [
   *     'image' => true|false,
   * ]
   * @return bool
   */
  protected function select(array $params = []): bool
  {
    $response = Http::withBasicAuth(config('main_database.username'), config('main_database.password'))
      ->get(config('main_database.url').'statement=select kodkli, brand, vkod ,name_ru, name_ua, name_eng, snabg, colour, tip, kodtnved, dekol, closed, art_post, strih_p, ediz, odoped, ob, ves, opis, imp from dc000089');

    $this->originalDate = $response->json()['d']['results'];
    $data = [];
    foreach ($response->json()['d']['results'] as $item) {
      $return = [];
      $return['out_id'] = $item['kodkli'];

      $brand = Brand::where('out_id', '=', $item['brand'])->first();
      $return['brand_id'] = $brand->id ?? null;

      $return['serial_number'] = $item['vkod'];
      $return['manager_code'] = $item['snabg'];
      $return['color'] = $item['colour'];
      $return['type'] = $item['tip'];
      $return['tn_foreign'] = $item['kodtnved'];
      $return['decal_code'] = $item['dekol'];
      $return['closed'] = $item['closed'] == 1;
      $return['supplier_sku'] = $item['art_post'];
      $return['barcode'] = $item['strih_p'];
      $return['unit'] = $item['ediz'];
      $return['vacation'] = $item['odoped'] == 1;
      $return['volume'] = $item['ob'];
      $return['weight'] = $item['ves'];
      $return['import'] = $item['imp'];

      if (!empty($item['name_ua'])) {
        $return['translations']['uk']['name'] = $item['name_ua'];
        $return['translations']['uk']['description'] = 'description_ua';
      }
      if (!empty($item['name_ru'])) {
        $return['translations']['ru']['name'] = $item['name_ru'];
        $return['translations']['ru']['description'] = 'description_ru';
      }
      if (!empty($item['name_eng'])) {
        $return['translations']['en']['name'] = $item['name_eng'];
        $return['translations']['en']['description'] = 'description_en';
      }

      $images = [];
      if (!empty($item['vkod']) && $params['image']) {
        $imagePath = $item['vkod'].'.jpg';
        $image = Storage::disk('original_media')->exists($imagePath);
        if ($image) {
          $images[] = $imagePath;
        }
      }
      $return['images'] = $images;

      if (!empty($return['translations'])) {
        $data[] = $return;
      }
    }
    $this->data = $data;
    return true;
  }
}
