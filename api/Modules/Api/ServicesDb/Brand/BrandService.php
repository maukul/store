<?php

namespace Modules\Api\ServicesDb\Brand;

use Illuminate\Support\Collection;
use Modules\Api\ServicesDb\Brand\Repositories\BrandRepositoryInterface;

class BrandService
{
  /**
   * @var BrandRepositoryInterface
   */
  protected $brandRepository;

  /**
   * BrandService constructor.
   * @param  BrandRepositoryInterface  $brandRepository
   */
  public function __construct(
    BrandRepositoryInterface $brandRepository
  ) {
    $this->brandRepository = $brandRepository;
  }

  /**
   * Get brand all.
   *
   * @param  array  $params  = [
   *      'filters' => [
   *          'show_main' => bool,
   *      ],
   *      'sort' => string
   * ]
   * @param  array|string[]  $column
   * @return Collection
   */
  public function getAll(array $params, array $column = ['*']): Collection
  {
    return $this->brandRepository->getAll($params, $column);
  }
}
