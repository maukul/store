<?php

namespace Modules\AdminPanel\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response as InertiaResponse;
use Illuminate\Http\RedirectResponse;
use Modules\AdminPanel\Http\Controllers\BaseController;
use Modules\AdminPanel\Http\Requests\Auth\LoginRequest;
use Modules\AdminPanel\Providers\RouteServiceProvider;

class AuthenticatedSessionController extends BaseController
{
  /**
   * Display the login view.
   *
   * @return InertiaResponse
   */
  public function create(): InertiaResponse
  {
    $this->seo()->setTitle(trans('adminpanel::login.title.general'));

    return Inertia::render('Auth/Login');
  }

  /**
   * Handle an incoming authentication request.
   *
   * @param  LoginRequest  $request
   * @return RedirectResponse
   */
  public function store(LoginRequest $request): RedirectResponse
  {
    $request->authenticate();

    $request->session()->regenerate();

    return redirect()->intended(RouteServiceProvider::HOME);
  }

  /**
   * Destroy an authenticated session.
   *
   * @param  Request  $request
   * @return RedirectResponse
   */
  public function destroy(Request $request): RedirectResponse
  {
    Auth::guard('admins')->logout();

    $request->session()->invalidate();

    $request->session()->regenerateToken();

    return redirect(RouteServiceProvider::HOME.'/login');
  }
}
