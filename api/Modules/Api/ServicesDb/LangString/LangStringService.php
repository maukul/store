<?php

namespace Modules\Api\ServicesDb\LangString;

use Illuminate\Support\Collection;
use Modules\Api\ServicesDb\LangString\Repositories\LangStringRepositoryInterface;

class LangStringService
{
  /**
   * @var LangStringRepositoryInterface
   */
  protected $langStringRepository;

  /**
   * LangStringService constructor.
   * @param  LangStringRepositoryInterface  $langStringRepository
   */
  public function __construct(
    LangStringRepositoryInterface $langStringRepository
  ) {
    $this->langStringRepository = $langStringRepository;
  }

  /**
   * @return string
   */
  public function hash(): string
  {
    return $this->langStringRepository->hash();
  }

  /**
   * Get langString all.
   *
   * @param $id
   * @return Collection
   */
  public function getAll(array $column = ['*']): Collection
  {
    return $this->langStringRepository->getAll($column);
  }
}
