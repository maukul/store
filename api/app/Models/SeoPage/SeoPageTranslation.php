<?php

namespace App\Models\SeoPage;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SeoPage\SeoPageTranslation
 *
 * @property int $id
 * @property int $seo_page_id
 * @property string $locale
 * @property string $name Поле назви
 * @property string $title Поле тайтл
 * @property string|null $keywords Поле ключові слова
 * @property string|null $description Поле ключові слова
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPageTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPageTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPageTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPageTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPageTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPageTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPageTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPageTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPageTranslation whereSeoPageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SeoPageTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class SeoPageTranslation extends Model
{
  /**
   * The timestamps.
   *
   * @var bool
   */
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'seo_page_translations';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'title',
    'keywords',
    'description',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'name' => 'string',
    'title' => 'string',
    'keywords' => 'string',
    'description' => 'string',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
