<?php

namespace Modules\AdminPanel\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Modules\AdminPanel\Providers\RouteServiceProvider;

class RedirectIfAuthenticated
{
  /**
   * Handle an incoming request.
   *
   * @param  Request  $request
   * @param  Closure  $next
   * @return Application|RedirectResponse|Redirector|mixed
   */
  public function handle(Request $request, Closure $next)
  {
    if (Auth::guard('admins')->check()) {
      return redirect(RouteServiceProvider::HOME);
    }

    return $next($request);
  }
}
