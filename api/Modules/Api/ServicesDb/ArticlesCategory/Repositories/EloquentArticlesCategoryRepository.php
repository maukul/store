<?php

namespace Modules\Api\ServicesDb\ArticlesCategory\Repositories;

use App\Models\CategoriesArticles\CategoriesArticles;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Api\ServicesDb\ArticlesCategory\Repositories\ArticlesCategoryRepositoryInterface;

/**
 * Class EloquentArticlesCategoryRepository.
 */
class EloquentArticlesCategoryRepository implements ArticlesCategoryRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => string,
   * ]
   * @return Collection
   */
  public function getAll(array $params = []): Collection
  {
    $modelArticlesCategory = CategoriesArticles::query();
    $sort = $params['sort'] ?? '';
    if (!empty($sort)) {
      if (in_array($sort, (new CategoriesArticles)->translatedAttributes)) {
        $modelArticlesCategory->orderByTranslation($sort);
      } else {
        $modelArticlesCategory->orderBy($sort);
      }
    }
    return $modelArticlesCategory->get();
  }

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'type' => string,
   *      ],
   * ]
   * @return CategoriesArticles
   */
  public function slug(array $params = []): CategoriesArticles
  {
    $modelArticlesCategory = CategoriesArticles::query();

    $filters = $params['filters'] ?? [];
    if (!empty($filters)) {
      if (!empty($filters['type'])) {
        $modelArticlesCategory->where('type', '=', $filters['type']);
      }
    }

    return $modelArticlesCategory->first();
  }
}
