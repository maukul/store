<?php

namespace Modules\AdminPanel\ServicesDb\CategoriesArticles;

use App\Models\CategoriesArticles\CategoriesArticles;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Modules\AdminPanel\ServicesDb\CategoriesArticles\Repositories\CategoriesArticlesRepositoryInterface;

class CategoriesArticlesService
{
  /**
   * @var CategoriesArticlesRepositoryInterface
   */
  protected $categoriesArticlesRepository;

  /**
   * CategoriesArticlesService constructor.
   * @param  CategoriesArticlesRepositoryInterface  $categoriesArticlesRepository
   */
  public function __construct(
    CategoriesArticlesRepositoryInterface $categoriesArticlesRepository
  ) {
    $this->categoriesArticlesRepository = $categoriesArticlesRepository;
  }

  /**
   * Get in tables categoriesArticles.
   *
   * @param  array  $params
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    return $this->categoriesArticlesRepository->search(
      array_merge(
        $params,
        []
      )
    );
  }

  /**
   * Get categoriesArticles by id.
   *
   * @param  int  $id
   * @return CategoriesArticles
   */
  public function getById(int $id): CategoriesArticles
  {
    return $this->categoriesArticlesRepository->getById($id);
  }

  /**
   * Get categoriesArticles all.
   *
   * @param  array|string[]  $column
   * @return Collection
   */
  public function getAll(array $column = ['*']): Collection
  {
    return $this->categoriesArticlesRepository->getAll($column);
  }

  /**
   * Store to DB if there are no errors.
   *
   * @param  array  $data
   * @return CategoriesArticles
   */
  public function create($data): CategoriesArticles
  {
    try {
      $create = [
        'position' => $data['position'],
      ];
      foreach (config('translatable.locales') as $locale) {
        $create[$locale]['name'] = $data['name'][$locale];
        $create[$locale]['title'] = $data['title'][$locale];
        $create[$locale]['keywords'] = $data['keywords'][$locale];
        $create[$locale]['description'] = $data['description'][$locale];
        if (!empty($data['slug'][$locale])) {
          $create[$locale]['slug'] = Str::slug($data['slug'][$locale], '-');
        }
      }

      $result = $this->categoriesArticlesRepository->create($create, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to create categoriesArticles data');
      }
    }

    return $result;
  }

  /**
   * Update categoriesArticles data
   * Store to DB if there are no errors.
   *
   * @param $id
   * @param $data
   * @return bool
   */
  public function update($id, $data): bool
  {
    $categoriesArticles = false;
    DB::beginTransaction();

    try {
      $update = [
        'position' => $data['position'],
      ];
      foreach (config('translatable.locales') as $locale) {
        $update[$locale]['name'] = $data['name'][$locale];
        $update[$locale]['title'] = $data['title'][$locale];
        $update[$locale]['keywords'] = $data['keywords'][$locale];
        $update[$locale]['description'] = $data['description'][$locale];
        if (!empty($data['slug'][$locale])) {
          $update[$locale]['slug'] = Str::slug($data['slug'][$locale], '-');
        }
      }

      $categoriesArticles = $this->categoriesArticlesRepository->update($id, $update, $data);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to update categoriesArticles data');
      }
    }

    DB::commit();

    return $categoriesArticles;
  }

  /**
   * Delete categoriesArticles by id.
   *
   * @param $id
   * @return bool
   */
  public function deleteById($id): bool
  {
    $categoriesArticles = false;
    DB::beginTransaction();

    try {
      $categoriesArticles = $this->categoriesArticlesRepository->deleteById($id);
    } catch (Exception $e) {
      DB::rollBack();
      Log::info($e->getMessage());

      if (App::environment('production')) {
        throw new InvalidArgumentException('Unable to delete categoriesArticles data');
      }
    }

    DB::commit();

    return $categoriesArticles;
  }
}
