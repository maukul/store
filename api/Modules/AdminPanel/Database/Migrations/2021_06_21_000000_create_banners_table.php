<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('banners', function (Blueprint $table) {
      $table->id();
      $table->string('type', 20);
      $table->integer('position')->default(20);
      $table->timestamps();
    });

    Schema::create('banner_translations', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('banner_id')->unsigned();
      $table->string('locale')->index();
      $table->string('name');
      $table->string('url')->nullable();

      $table->unique(['banner_id', 'locale']);
      $table->foreign('banner_id')->references('id')->on('banners')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('banner_translations');
    Schema::dropIfExists('banners');
  }
}
