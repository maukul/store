<?php

namespace Modules\AdminPanel\Database\factories;

use App\Models\CategoriesArticles\CategoriesArticles;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = CategoriesArticles::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    $create = [];
    foreach (config('translatable.locales') as $locale) {
      $create[$locale]['name'] = $this->faker->text(90);
      $create[$locale]['title'] = $this->faker->text(90);
      $create[$locale]['keywords'] = $this->faker->text(90);
      $create[$locale]['description'] = $this->faker->text(90);
      $create[$locale]['content'] = $this->faker->sentence(900);
    }
    return $create;
  }
}
