<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Api\Http\Resources\ArticlesCategory\ArticlesCategoryResource;
use Modules\Api\Http\Resources\ArticlesCategory\ArticlesCategoryOneResource;
use Modules\Api\ServicesDb\ArticlesCategory\ArticlesCategoryService;

class ArticlesCategoryController extends BaseController
{
  /**
   * @var ArticlesCategoryService
   */
  protected $articlesCategory;

  /**
   * ArticlesCategoryController constructor.
   * @param  ArticlesCategoryService  $articlesCategory
   */
  public function __construct(
    ArticlesCategoryService $articlesCategory
  ) {
    parent::__construct();
    $this->articlesCategory = $articlesCategory;
  }

  /**
   * @param  Request  $request
   * @return AnonymousResourceCollection
   */
  public function index(Request $request): AnonymousResourceCollection
  {
    $articlesCategoryData = $this->articlesCategory->getAll($request->all());
    return ArticlesCategoryResource::collection($articlesCategoryData);
  }

  /**
   * @param  Request  $request
   * @return ArticlesCategoryOneResource
   */
  public function slug(Request $request): ArticlesCategoryOneResource
  {
    $articlesCategoryData = $this->articlesCategory->slug($request->all());
    return new ArticlesCategoryOneResource($articlesCategoryData);
  }
}
