<?php

namespace Modules\Api\ServicesDb\Page\Repositories;

use App\Models\Page\Page;
use Illuminate\Database\Eloquent\Collection;
use Modules\Api\ServicesDb\Page\Repositories\PageRepositoryInterface;

/**
 * Class EloquentPageRepository.
 */
class EloquentPageRepository implements PageRepositoryInterface
{
  /**
   * @param  array  $params
   * @return Collection
   */
  public function getAll(array $params = []): Collection
  {
    $modelPage = Page::query();
    return $modelPage->get();
  }

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'type' => string,
   *      ],
   * ]
   * @return Page
   */
  public function one(array $params = []): Page
  {
    $modelPage = Page::query();

    $filters = $params['filters'] ?? [];
    if (!empty($filters)) {
      if (!empty($filters['type'])) {
        $modelPage->where('type', '=', $filters['type']);
      }
    }
    return $modelPage->first();
  }
}
