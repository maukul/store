<?php

namespace Modules\AdminPanel\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response as InertiaResponse;
use Modules\AdminPanel\Http\Requests\SeoPages\SeoPagesEditRequest;
use Illuminate\Http\RedirectResponse;
use Modules\AdminPanel\Http\Resources\SeoPage\SeoPageResource;
use Modules\AdminPanel\ServicesDb\SeoPage\SeoPageService;
use Route;

class SeoPagesController extends BaseController
{
  /**
   * @var SeoPageService
   */
  protected $seoPages;

  /**
   * SeoPagesController constructor.
   *
   * @param  SeoPageService  $seoPages
   */
  public function __construct(
    SeoPageService $seoPages
  ) {
    parent::__construct();
    $this->seoPages = $seoPages;

    $this->seo()->setTitle(trans('adminpanel::seo_pages.title.'.(optional(Route::getCurrentRoute())->getActionMethod() ?? 'general')));
  }

  /**
   * Display a listing of the resource.
   *
   * @param  Request  $request
   * @return InertiaResponse
   */
  public function index(Request $request): InertiaResponse
  {
    $seoPagesData = $this->seoPages->search($request->all());
    $tableData = SeoPageResource::collection($seoPagesData);
    return Inertia::render('SeoPages/Index', [
      'tableData' => $tableData,
      'request' => $request->all(),
      'tableParams' => [
        'columnFilters' => $request->input('columnFilters', []),
        'sort' => $request->input('sort', ['field' => '', 'type' => '']),
        'page' => $tableData->resource->currentPage() ?? 1,
        'perPage' => $tableData->resource->perPage() ?? 10,
      ],
    ]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param $id
   * @return InertiaResponse
   */
  public function edit($id): InertiaResponse
  {
    $seoPage = $this->seoPages->getById((int) $id);
    $seoPageData = new SeoPageResource($seoPage);
    return Inertia::render('SeoPages/Edit', [
      'data' => $seoPageData,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  SeoPagesEditRequest  $request
   * @param $id
   * @return RedirectResponse
   */
  public function update(SeoPagesEditRequest $request, $id): RedirectResponse
  {
    $this->seoPages->update((int) $id, $request->getFormData());
    return redirect()->back()->with('message', trans('adminpanel::seo_pages.message.success.update'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param $id
   * @return RedirectResponse
   */
  public function destroy($id): RedirectResponse
  {
    $this->seoPages->deleteById($id);
    return redirect()->route('admin-panel.seo-pages.index')
      ->with('message', trans('adminpanel::seo_pages.message.success.destroy'));
  }
}
