<?php

namespace Modules\AdminPanel\Database\Seeders;

use App\Models\Page\Page;
use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $about = Page::firstOrCreate(['type' => 'about'], [
      'type' => 'about',
      'ru' => [
        'name' => 'test',
        'title' => 'test',
        'keywords' => 'test',
        'description' => 'test',
        'value' => [
          'description' => 'test',
          'bloc-statistics' => []
        ],
      ],
      'uk' => [
        'name' => 'test',
        'title' => 'test',
        'keywords' => 'test',
        'description' => 'test',
        'value' => [
          'description' => 'test',
          'bloc-statistics' => []
        ],
      ],
      'en' => [
        'name' => 'test',
        'title' => 'test',
        'keywords' => 'test',
        'description' => 'test',
        'value' => [
          'description' => 'test',
          'bloc-statistics' => []
        ],
      ],
    ]);
    if ($about->getMedia()->count() == 0) {
      for ($i = 0; $i < random_int(1, 6); $i++) {
        $about->addMediaFromUrl('https://picsum.photos/700/700')->toMediaCollection('gallery', 'media');
      }
    }
  }
}
