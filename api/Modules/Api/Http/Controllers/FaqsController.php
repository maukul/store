<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\Api\Http\Resources\Faq\FaqResource;
use Modules\Api\ServicesDb\Faq\FaqService;

class FaqsController extends BaseController
{
  /**
   * @var FaqService
   */
  protected $faqs;

  /**
   * FaqsController constructor.
   * @param  FaqService  $faqs
   */
  public function __construct(
    FaqService $faqs
  ) {
    parent::__construct();
    $this->faqs = $faqs;
  }

  /**
   * @return AnonymousResourceCollection
   */
  public function index(Request $request): AnonymousResourceCollection
  {
    $faqsData = $this->faqs->getAll($request->all());
    return FaqResource::collection($faqsData);
  }
}
