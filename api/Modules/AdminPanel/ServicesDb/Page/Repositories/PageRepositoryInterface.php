<?php

namespace Modules\AdminPanel\ServicesDb\Page\Repositories;

use App\Models\Page\Page;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface PageRepositoryInterface
 * @package Modules\AdminPanel\ServicesDb\Page\Repositories
 */
interface PageRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator;

  /**
   * @param  array|string[]  $columns
   * @return Page
   */
  public function getAll(array $columns = ['*']): Page;

  /**
   * @param  int  $id
   * @return Page
   */
  public function getById(int $id): Page;

  /**
   * @param  array  $create
   * @param  array  $data
   * @return Page
   */
  public function create(array $create, array $data): Page;

  /**
   * @param  int  $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update(int $id, array $update, array $data): bool;

  /**
   * @param  int  $id
   * @return bool
   */
  public function deleteById(int $id): bool;
}
