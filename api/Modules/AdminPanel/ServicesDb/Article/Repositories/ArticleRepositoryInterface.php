<?php

namespace Modules\AdminPanel\ServicesDb\Article\Repositories;

use App\Models\Article\Article;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Interface ArticleRepositoryInterface
 * @package Modules\AdminPanel\ServicesDb\Article\Repositories
 */
interface ArticleRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => [
   *          'type' => asc|desc,
   *          'field' => field
   *      ],
   *      'columnFilters' => column,
   *      'perPage' => 10,
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator;

  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection;

  /**
   * @param  int  $id
   * @return Article
   */
  public function getById(int $id): Article;

  /**
   * @param  array  $create
   * @param  array  $data
   * @return Article
   */
  public function create(array $create, array $data): Article;

  /**
   * @param  int  $id
   * @param  array  $update
   * @param  array  $data
   * @return bool
   */
  public function update(int $id, array $update, array $data): bool;

  /**
   * @param  int  $id
   * @return bool
   */
  public function deleteById(int $id): bool;
}
