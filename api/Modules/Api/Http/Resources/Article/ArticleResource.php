<?php

namespace Modules\Api\Http\Resources\Article;

use App\Models\Article\Article;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Modules\Api\Http\Resources\Product\ProductDataResource;

class ArticleResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $article Article */
    $article = $this->resource;

    $imageThumbMedia = $article->getFirstMedia('image_thumb');
    $imageThumb = [
      'url' => $imageThumbMedia->getFullUrl('thumb'),
    ];
    $imagesGallery = [];
    foreach ($article->getMedia('gallery') as $image) {
      $imagesGallery[] = [
        'url_original' => $image->getFullUrl(),
        'url_main' => $image->getUrl('gallery_main'),
        'url_thumb' => $image->getUrl('gallery_thumb'),
      ];
    }

    $result = [
      'id' => $article->id,
      'name' => $article->name,
      'slug' => $article->slug,
      'content' => $article->content,
      'image' => $imageThumb,
      'images' => $imagesGallery,
      'date' => $article->created_at->format('d/m/y'),
    ];

    if ($request->has('relationships.recommend')) {
      $result['relationships']['recommend']['data'] = ArticleDataResource::collection($article->recommend()->get());
      $result['relationships']['recommend']['meta']['count'] = $article->recommend()->count();
    }

    return $result;
  }
}
