<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Modules\Api\Http\Requests\Email\EmailSendRequest;
use Illuminate\Support\Facades\Mail;
use Modules\Api\Mail\ContactForm;

class ContactController extends BaseController
{
  public function send(EmailSendRequest $request): JsonResponse
  {
    Mail::to(config('api.email_organization'))->send(new ContactForm([
      trans('api::contact.form.name.title') => $request->input('name'),
      trans('api::contact.form.phone.title') => $request->input('phone'),
      trans('api::contact.form.email.title') => $request->input('email'),
      trans('api::contact.form.message.title') => $request->input('message'),
    ]));

    return response()->json(['status' => true]);
  }
}
