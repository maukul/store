<?php
return [
  'title' => [
    'general' => 'Сео страницы',
    'index' => 'Список сео страниц',
    'create' => 'Создать сео страницу',
    'edit' => 'Редактировать сео страницу',
  ],
  'form' => [
    'system_name' => [
      'title' => 'Системная название',
      'placeholder' => 'Системная название',
    ],
    'name' => [
      'title' => 'Название',
      'placeholder' => 'Название',
    ],
    'title' => [
      'title' => 'Мета тайтл',
      'placeholder' => 'Мета тайтл',
    ],
    'keywords' => [
      'title' => 'Мета ключевые слова',
      'placeholder' => 'Мета ключевые слова',
    ],
    'description' => [
      'title' => 'Мета описание',
      'placeholder' => 'Мета описание',
    ],
  ],
  'table' => [
    'columns' => [
      'system_name' => 'Системная название',
      'name' => 'Название',
    ],
  ],
  'message' => [
    'success' => [
      'store' => 'Создание сео страницу прошло успешно.',
      'update' => 'Редактирование сео страницу прошло успешно.',
      'destroy' => 'Удаление сео страницы прошло успешно.',
    ],
  ],
  'text' => [
  ]
];
