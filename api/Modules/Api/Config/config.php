<?php

return [
  'name' => 'Api',
  'domain' => env('APP_API_DOMAIN'),
  'email_organization' => env('EMAIL_ORGANIZATION'),
];
