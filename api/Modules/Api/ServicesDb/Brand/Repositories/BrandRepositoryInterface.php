<?php

namespace Modules\Api\ServicesDb\Brand\Repositories;

use Illuminate\Support\Collection;

/**
 * Interface BrandRepositoryInterface
 * @package Modules\Api\ServicesDb\Brand\Repositories
 */
interface BrandRepositoryInterface
{
  /**
   * @param  array|string[]  $columns  = [
   *      'filters' => [
   *          'show_main' => bool,
   *      ],
   *      'sort' => string
   * ]
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $params, array $columns = ['*']): Collection;
}
