<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Inertia\Response;
use Modules\Api\Http\Resources\Branch\BranchResource;
use Modules\Api\ServicesDb\Branch\BranchService;

class BranchesController extends BaseController
{
  /**
   * @var BranchService
   */
  protected $branches;

  /**
   * BranchesController constructor.
   * @param  BranchService  $branches
   */
  public function __construct(
    BranchService $branches
  ) {
    parent::__construct();
    $this->branches = $branches;
  }

  /**
   * @return AnonymousResourceCollection
   */
  public function index(): AnonymousResourceCollection
  {
    $branchesData = $this->branches->getAll();
    return BranchResource::collection($branchesData);
  }
}
