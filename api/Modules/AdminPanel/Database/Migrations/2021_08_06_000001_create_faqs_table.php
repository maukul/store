<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('faqs', function (Blueprint $table) {
      // Таблиця вопросов и ответов
      $table->id();
      $table->bigInteger('position');
      $table->timestamps();
      $table->softDeletes();
    });

    Schema::create('faq_translations', function (Blueprint $table) {
      // Таблиця перекладів вопросов и ответов
      $table->increments('id');
      $table->bigInteger('faq_id')->unsigned();
      $table->string('locale')->index();
      $table->string('name', 90)->comment('Поле назви');
      $table->string('question', 255)->comment('Поле вопросов');
      $table->text('reply')->comment('Поле ответ');

      $table->unique(['faq_id', 'locale']);
      $table->foreign('faq_id')->references('id')->on('faqs')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('faq_translations');
    Schema::dropIfExists('faqs');
  }
}
