<?php

namespace App\Models\Product;

use App\Models\Branch\BranchesInProductsInformation;
use App\Models\Branch\BranchesInProductsLeftover;
use App\Models\Branch\BranchesInProductsType;
use App\Models\Brand\Brand;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * App\Models\Product\Product
 *
 * @property int $id
 * @property int $out_id Id яке використовується в головній базі аналог dc000089.kodkli
 * @property int $brand_id Поле яке посилається на бренд в головній базі аналог dc000089.brand
 * @property string $serial_number Поле яке показує внутрішній номер товару головній базі аналог dc000089.vkod
 * @property string $manager_code Поле яке показує код категорийного менеджера в головній базі аналог dc000089.snabg
 * @property string $color Поле яке показує код кольору в головній базі аналог dc000089.colour
 * @property string $type Поле яке показує тип товару в головній базі аналог dc000089.tip
 * @property string $tn_foreign Поле яке показує код тн вэд в головній базі аналог dc000089.kodtnved
 * @property string|null $decal_code Поле яке показує код серии деколи в головній базі аналог dc000089.dekol
 * @property bool $closed Поле яке показує закрити від відображення товар чи ні в головній базі аналог dc000089.closed
 * @property string $supplier_sku Поле яке показує артикул поставщика в головній базі аналог dc000089.art_post
 * @property string $barcode Поле яке показує штрих код в головній базі аналог dc000089.strih_p
 * @property string $unit Поле яке показує штрих код в головній базі аналог dc000089.ediz
 * @property bool $vacation Поле яке показує отпуск кратно доп единицам в головній базі аналог dc000089.odoped
 * @property string $volume Поле яке показує обєм в головній базі аналог dc000089.ob
 * @property string $weight Поле яке показує обєм в головній базі аналог dc000089.ves
 * @property int $import Поле яке показує признак імпорта в головній базі аналог dc000089.imp
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $analogs
 * @property-read int|null $analogs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|BranchesInProductsInformation[] $branchInfomations
 * @property-read int|null $branch_infomations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|BranchesInProductsLeftover[] $branchLeftovers
 * @property-read int|null $branch_leftovers_count
 * @property-read Brand $brand
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $options
 * @property-read int|null $options_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Product[] $related
 * @property-read int|null $related_count
 * @property-read \App\Models\Product\ProductTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product\ProductTranslation[] $translations
 * @property-read int|null $translations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|BranchesInProductsType[] $types
 * @property-read int|null $types_count
 * @method static Builder|Product listsTranslations(string $translationField)
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product notTranslatedIn(?string $locale = null)
 * @method static Builder|Product orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static Builder|Product orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static Builder|Product orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static Builder|Product query()
 * @method static Builder|Product translated()
 * @method static Builder|Product translatedIn(?string $locale = null)
 * @method static Builder|Product whereBarcode($value)
 * @method static Builder|Product whereBrandId($value)
 * @method static Builder|Product whereClosed($value)
 * @method static Builder|Product whereColor($value)
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereDecalCode($value)
 * @method static Builder|Product whereDeletedAt($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereImport($value)
 * @method static Builder|Product whereManagerCode($value)
 * @method static Builder|Product whereOutId($value)
 * @method static Builder|Product whereSerialNumber($value)
 * @method static Builder|Product whereSupplierSku($value)
 * @method static Builder|Product whereTnForeign($value)
 * @method static Builder|Product whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static Builder|Product whereTranslationIlike(string $translationField, $value, ?string $locale = null)
 * @method static Builder|Product whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static Builder|Product whereType($value)
 * @method static Builder|Product whereUnit($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @method static Builder|Product whereVacation($value)
 * @method static Builder|Product whereVolume($value)
 * @method static Builder|Product whereWeight($value)
 * @method static Builder|Product withTranslation()
 * @mixin \Eloquent
 */
class Product extends Model implements TranslatableContract, HasMedia
{
  use Translatable,
    InteractsWithMedia;

  /**
   * The translated attributes that are mass assignable.
   *
   * @var array
   */
  public $translatedAttributes = [
    'slug',
    'name',
    'description',
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'products';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'out_id',
    'brand_id',
    'serial_number',
    'manager_code',
    'color',
    'type',
    'tn_foreign',
    'decal_code',
    'closed',
    'supplier_sku',
    'barcode',
    'vacation',
    'volume',
    'weight',
    'import',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'out_id' => 'integer',
    'brand_id' => 'integer',
    'serial_number' => 'string',
    'manager_code' => 'string',
    'color' => 'string',
    'type' => 'string',
    'tn_foreign',
    'decal_code',
    'closed',
    'supplier_sku',
    'barcode',
    'vacation',
    'volume',
    'weight',
    'import',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * @param  Media|null  $media
   * @throws \League\Flysystem\FileNotFoundException
   * @throws \Spatie\Image\Exceptions\InvalidManipulation
   */
  public function registerMediaConversions(Media $media = null): void
  {
    $this->addMediaConversion('thumb')
      ->fit(Manipulations::FIT_MAX, 217, 164)
      ->watermark(storage_path('app/logo-watermark.png'))
      ->watermarkOpacity(20)
      ->watermarkPosition(Manipulations::POSITION_CENTER)
      ->watermarkFit(Manipulations::FIT_FILL);

    $this->addMediaConversion('main')
      ->fit(Manipulations::FIT_CROP, 720, 478)
      ->watermark(storage_path('app/logo-watermark.png'))
      ->watermarkOpacity(20)
      ->watermarkPosition(Manipulations::POSITION_CENTER)
      ->watermarkFit(Manipulations::FIT_STRETCH);

    $this->addMediaConversion('original')
      ->watermark(storage_path('app/logo-watermark.png'))
      ->watermarkOpacity(20)
      ->watermarkPosition(Manipulations::POSITION_CENTER)
      ->watermarkFit(Manipulations::FIT_STRETCH);
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /**
   *  Add scope
   *
   * @param  Builder  $query
   * @param  string  $translationField
   * @param $value
   * @param  string|null  $locale
   * @return mixed
   */
  public function scopeWhereTranslationIlike(
    Builder $query,
    string $translationField,
    $value,
    ?string $locale = null
  ): mixed {
    return $this->scopeWhereTranslation($query, $translationField, $value, $locale, 'whereHas', 'ILIKE');
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /**
   * @return BelongsTo<Brand>|Brand
   */
  public function brand(): BelongsTo
  {
    return $this->belongsTo(Brand::class);
  }

  /**
   * @return HasMany<BranchesInProductsLeftover>|BranchesInProductsLeftover
   */
  public function branchLeftovers(): HasMany
  {
    return $this->hasMany(BranchesInProductsLeftover::class, 'product_id', 'id');
  }

  /**
   * @return HasMany<BranchesInProductsInformation>|BranchesInProductsInformation
   */
  public function branchInfomations(): HasMany
  {
    return $this->hasMany(BranchesInProductsInformation::class, 'product_id', 'id');
  }

  /**
   * @return HasMany<BranchesInProductsType>|BranchesInProductsType
   */
  public function types(): HasMany
  {
    return $this->hasMany(BranchesInProductsType::class, 'product_id', 'id');
  }

  /**
   * @return BelongsToMany<Product>|Product
   */
  public function options(): BelongsToMany
  {
    return $this->belongsToMany(Product::class, 'product_options', 'product_id', 'option_id');
  }

  /**
   * @return BelongsToMany<Product>|Product
   */
  public function analogs(): BelongsToMany
  {
    return $this->belongsToMany(Product::class, 'product_analogs', 'product_id', 'analog_id');
  }

  /**
   * @return BelongsToMany<Product>|Product
   */
  public function related(): BelongsToMany
  {
    return $this->belongsToMany(Product::class, 'product_related', 'product_id', 'related_id');
  }


  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
