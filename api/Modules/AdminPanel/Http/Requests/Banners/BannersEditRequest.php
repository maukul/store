<?php

namespace Modules\AdminPanel\Http\Requests\Banners;

use Modules\AdminPanel\Http\Requests\FormRequest;

class BannersEditRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize(): bool
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array
  {
    return [
      'type' => [
        'required',
        'string',
      ],
      'position' => [
        'required',
        'numeric',
      ],
      'name.*' => [
        'required',
        'string',
        'min:2',
        'max:200',
      ],
//            'url.*' => [
//                'required',
//                'string',
//                'min:2',
//                'max:200',
//            ],
      'image.*' => [
        'nullable',
      ],
    ];
  }

  /**
   * Get custom messages for validator errors.
   *
   * @return array
   */
  public function messages(): array
  {
    return [];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes(): array
  {
    $return = [];

    $return['type'] = trans('adminpanel::banners.form.type.title');
    $return['position'] = trans('adminpanel::banners.form.position.title');

    foreach (config('translatable.locales') as $item) {
      $return['name.'.$item] = trans('adminpanel::banners.form.name.title').' ('.$item.')';
      $return['url.'.$item] = trans('adminpanel::banners.form.url.title').' ('.$item.')';
      $return['image.'.$item] = trans('adminpanel::banners.form.image.title').' ('.$item.')';
    }
    return $return;
  }
}
