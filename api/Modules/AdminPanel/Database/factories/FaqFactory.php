<?php

namespace Modules\AdminPanel\Database\factories;

use App\Models\Faq\Faq;
use Illuminate\Database\Eloquent\Factories\Factory;

class FaqFactory extends Factory
{
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = Faq::class;

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition()
  {
    $create = [
      'position' => $this->faker->randomNumber(),
    ];
    foreach (config('translatable.locales') as $locale) {
      $create[$locale]['name'] = $this->faker->text(90);
      $create[$locale]['question'] = $this->faker->text();
      $create[$locale]['reply'] = $this->faker->realText();
    }
    return $create;
  }
}
