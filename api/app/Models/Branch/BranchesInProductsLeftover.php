<?php

namespace App\Models\Branch;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Branch\BranchesInProductsLeftover
 *
 * @property int $id
 * @property int $product_id Поле яке посилається на товар в головній базі аналог procedure im_rests(filial).kodm
 * @property int $branch_id Поле яке посилається на філіал в головній базі аналог procedure im_rests(filial).filial
 * @property float $total_remainder Поле яке показує загальний остаток товарів в головній базі аналог dc000085.ost
 * @property float $remainder Поле яке остаток товарів в головній базі аналог dc000089.ostBO
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Branch\Branch|null $branch
 * @property-read Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover query()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover whereRemainder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover whereTotalRemainder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsLeftover whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BranchesInProductsLeftover extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'branches_in_products_leftovers';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_id',
    'branch_id',
    'total_remainder',
    'remainder',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'product_id' => 'integer',
    'branch_id' => 'integer',
    'total_remainder' => 'double',
    'remainder' => 'double',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /**
   * @return HasOne<Product>|Product
   */
  public function product(): HasOne
  {
    return $this->hasOne(Product::class);
  }

  /**
   * @return HasOne<Branch>|Branch
   */
  public function branch(): HasOne
  {
    return $this->hasOne(Branch::class);
  }


  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
