<?php

namespace App\Models\Branch;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models\Product\Product;

/**
 * App\Models\Branch\BranchesInProductsInformation
 *
 * @property int $id
 * @property int $product_id Поле яке посилається на товар в головній базі аналог dc000085.kodkli
 * @property int $branch_id Поле яке посилається на філіал в головній базі аналог dc000085.filial
 * @property float $sum Поле яке показує сумму в головній базі аналог dc000089.summa
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\Branch\Branch|null $branch
 * @property-read Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsInformation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsInformation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsInformation query()
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsInformation whereBranchId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsInformation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsInformation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsInformation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsInformation whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsInformation whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BranchesInProductsInformation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BranchesInProductsInformation extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'branches_in_products_information';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'product_id',
    'branch_id',
    'sum',
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'product_id' => 'integer',
    'branch_id' => 'integer',
    'sum' => 'double',
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /**
   * @return HasOne<Product>|Product
   */
  public function product(): HasOne
  {
    return $this->hasOne(Product::class);
  }

  /**
   * @return HasOne<Branch>|Branch
   */
  public function branch(): HasOne
  {
    return $this->hasOne(Branch::class);
  }


  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
