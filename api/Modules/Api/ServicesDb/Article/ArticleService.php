<?php

namespace Modules\Api\ServicesDb\Article;

use App\Models\Article\Article;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Modules\Api\ServicesDb\Article\Repositories\ArticleRepositoryInterface;

class ArticleService
{
  /**
   * @var ArticleRepositoryInterface
   */
  protected $ArticleRepository;

  /**
   * ArticleService constructor.
   * @param  ArticleRepositoryInterface  $ArticleRepository
   */
  public function __construct(
    ArticleRepositoryInterface $ArticleRepository
  ) {
    $this->ArticleRepository = $ArticleRepository;
  }

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'cat_id' => int,
   *      ],
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    return $this->ArticleRepository->search($params);
  }

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'slug' => string,
   *      ],
   * ]
   * @return Article
   */
  public function one(array $params = []): Article
  {
    return $this->ArticleRepository->one($params);
  }
}
