<?php
return [
  'form' => [
    'email' => [
      'label' => 'Логин',
      'placeholder' => 'Введите эл. адрес',
    ],
    'password' => [
      'label' => 'Пароль',
      'placeholder' => 'Введите пароль',
    ],
    'remember_me' => [
      'label' => 'Запомнить меня',
    ],
  ],
  'text' => [
    'login' => 'Вход',
    'cancel' => 'Отмена',
    'admin_panel' => 'Панель администратора',
    'password_recovery' => 'Восстановление пароля',
    'remember' => 'Запомнить меня',
  ],
  'title' => [
    'general' => 'Вход',
  ],
  'messages' => [
  ],
];
