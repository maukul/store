<?php

namespace Modules\AdminPanel\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Modules\AdminPanel\Http\Requests\Articles\ArticlesCreateRequest;
use Modules\AdminPanel\Http\Requests\Articles\ArticlesEditRequest;
use Inertia\Response as InertiaResponse;
use Illuminate\Http\RedirectResponse;
use Modules\AdminPanel\Http\Resources\Article\ArticleResource;
use Modules\AdminPanel\Http\Resources\CategoriesArticles\CategoriesArticlesResource;
use Modules\AdminPanel\ServicesDb\Article\ArticleService;
use Modules\AdminPanel\ServicesDb\CategoriesArticles\CategoriesArticlesService;
use Route;

class ArticlesController extends BaseController
{
  /**
   * @var ArticleService
   */
  protected $articles;
  protected $categoriesArticles;

  /**
   * ArticlesController constructor.
   *
   * @param  ArticleService  $articles
   * @param  CategoriesArticlesService  $categoriesArticles
   */
  public function __construct(
    ArticleService $articles,
    CategoriesArticlesService $categoriesArticles
  ) {
    parent::__construct();
    $this->articles = $articles;
    $this->categoriesArticles = $categoriesArticles;

    $this->seo()->setTitle(trans('adminpanel::articles.title.'.(optional(Route::getCurrentRoute())->getActionMethod() ?? 'general')));
  }

  /**
   * Display a listing of the resource.
   *
   * @param  Request  $request
   * @return InertiaResponse
   */
  public function index(Request $request): InertiaResponse
  {
    $articlesData = $this->articles->search($request->all());
    $tableData = ArticleResource::collection($articlesData);
    return Inertia::render('Articles/Index', [
      'tableData' => $tableData,
      'request' => $request->all(),
      'tableParams' => [
        'columnFilters' => $request->input('columnFilters', []),
        'sort' => $request->input('sort', ['field' => '', 'type' => '']),
        'page' => $tableData->resource->currentPage() ?? 1,
        'perPage' => $tableData->resource->perPage() ?? 10,
      ],
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return InertiaResponse
   */
  public function create(): InertiaResponse
  {
    return Inertia::render('Articles/Create', [
      'categories' => CategoriesArticlesResource::collection($this->categoriesArticles->getAll()),
      'recommend' => ArticleResource::collection($this->articles->getAll())
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  ArticlesCreateRequest  $request
   * @return RedirectResponse
   */
  public function store(ArticlesCreateRequest $request): RedirectResponse
  {
    $model = $this->articles->create($request->getFormData());
    return redirect()->route('admin-panel.blog-articles.edit', $model->id)
      ->with('message', trans('adminpanel::articles.message.success.store'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param $id
   * @return InertiaResponse
   */
  public function edit($id): InertiaResponse
  {
    $articles = $this->articles->getById((int) $id);
    $articlesData = new ArticleResource($articles);
    return Inertia::render('Articles/Edit', [
      'data' => $articlesData,
      'categories' => CategoriesArticlesResource::collection($this->categoriesArticles->getAll()),
      'recommend' => ArticleResource::collection($this->articles->getAll())
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  ArticlesEditRequest  $request
   * @param $id
   * @return RedirectResponse
   */
  public function update(ArticlesEditRequest $request, $id): RedirectResponse
  {
    $this->articles->update((int) $id, $request->getFormData());
    return redirect()->back()->with('message', trans('adminpanel::articles.message.success.update'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param $id
   * @return RedirectResponse
   */
  public function destroy($id): RedirectResponse
  {
    $this->articles->deleteById((int) $id);
    return redirect()->route('admin-panel.blog-articles.index')
      ->with('message', trans('adminpanel::articles.message.success.destroy'));
  }
}
