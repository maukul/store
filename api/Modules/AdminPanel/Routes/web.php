<?php

use \Modules\AdminPanel\Http\Middleware\{
  HandleInertiaRequests,
  RedirectIfAuthenticated,
  Authenticated,
};

use \Modules\AdminPanel\Http\Controllers\Auth\{
  AuthenticatedSessionController,
  NewPasswordController,
};
use Modules\AdminPanel\Http\Controllers\AdminsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('admin-panel.')->group(function () {
  Route::domain(config('adminpanel.domain'))->group(function () {
    Route::middleware([HandleInertiaRequests::class])->group(function () {

      Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware(RedirectIfAuthenticated::class)
        ->name('login');

      Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware(RedirectIfAuthenticated::class)
        ->name('login.store');

      Route::get('/reset-password', [NewPasswordController::class, 'create'])
        ->middleware(RedirectIfAuthenticated::class)
        ->name('password.reset');

      Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware(RedirectIfAuthenticated::class)
        ->name('password.update');

      Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware([Authenticated::class])
        ->name('logout');

      Route::middleware([Authenticated::class])->group(function () {
        Route::resource('admins', AdminsController::class, ['except' => ['show']]);
        Route::resource('banners', BannersController::class, ['except' => ['show']]);
        Route::resource('lang-strings', LangStringsController::class, ['except' => ['show', 'creat']]);
        Route::resource('seo-pages', SeoPagesController::class, ['except' => ['show', 'creat']]);
        Route::resource('categories-articles', CategoriesArticlesController::class, ['except' => ['show',]]);
        Route::resource('static-pages', PagesController::class, ['except' => ['show', 'creat']]);
        Route::resource('blog-articles', ArticlesController::class, ['except' => ['show',]]);
        Route::resource('page-faqs', FaqsController::class, ['except' => ['show',]]);

        Route::resource('media', MediaController::class, ['only' => ['store', 'destroy']]);
        Route::get('/', [AdminsController::class, 'index'])->name('dashboard.index');
      });
    });
  });
});


