<?php

namespace App\Services\MainDatabase\Tables;

use App\Models\Branch\Branch;
use App\Models\Product\Product;
use App\Services\MainDatabase\BaseMainDatabase;
use Illuminate\Support\Facades\Http;

/**
 * Class BranchesInProductsInformationTable
 * @package App\Services\MainDatabase\Tables
 */
final class BranchesInProductsInformationTable extends BaseMainDatabase
{
  /**
   * BrandTable constructor.
   */
  public function __construct()
  {
    $this->select();
  }

  /**
   * The function select
   *
   * @param  array  $params
   * @return bool
   */
  protected function select(array $params = []): bool
  {
    $response = Http::withBasicAuth(config('main_database.username'), config('main_database.password'))
      ->get(config('main_database.url').'statement=select kodkli, filial,top,summa from dc000085 where priznak=776');

    $this->originalDate = $response->json()['d']['results'];
    $data = [];
    foreach ($response->json()['d']['results'] as $item) {
      $return = [];

      $product = Product::where('out_id', '=', $item['kodkli'])->first();
      $return['product_id'] = $product->id ?? null;

      $branch = Branch::where('out_id', '=', $item['filial'])->first();
      $return['branch_id'] = $branch->id ?? null;

      $return['sum'] = $item['summa'];

      if (!empty($return['product_id']) && !empty($return['branch_id'])) {
        $data[] = $return;
      }
    }
    $this->data = $data;
    return true;
  }
}
