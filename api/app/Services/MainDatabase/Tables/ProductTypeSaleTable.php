<?php

namespace App\Services\MainDatabase\Tables;

use App\Models\Branch\Branch;
use App\Models\Product\Product;
use App\Services\MainDatabase\BaseMainDatabase;
use Illuminate\Support\Facades\Http;

/**
 * Class ProductTypeSaleTable
 * @package App\Services\MainDatabase\Tables
 */
final class ProductTypeSaleTable extends BaseMainDatabase
{
  /**
   * ProductTypeSaleTable constructor.
   *
   * @param  int  $branch_id
   * @param  int|null  $branch_out_id
   */
  public function __construct(int $branch_id, int $branch_out_id = null)
  {
    $this->select([
      'branch_id' => $branch_id,
      'branch_out_id' => $branch_out_id,
    ]);
  }

  /**
   * The function select
   *
   * @param  array  $params  = [
   *     'branch_id' => $branch_id,
   *     'branch_out_id' => $branch_out_id,
   * ]
   * @return bool
   */
  protected function select(array $params = []): bool
  {
    $branch_id = $params['branch_id'] ?? null;
    $branch_out_id = $params['branch_out_id'] ?? null;

    if (empty($branch_out_id)) {
      $branch = Branch::find($branch_id);
      $branch_out_id = $branch->out_id;
    }

    $response = Http::withBasicAuth(config('main_database.username'), config('main_database.password'))
      ->get(config('main_database.url').'statement=execute procedure IM_main_sale('.$branch_out_id.')');

    $this->originalDate = $response->json()['d']['results'];
    $data = [];
    foreach ($response->json()['d']['results'] as $item) {
      $return = [];

      $product = Product::where('out_id', '=', $item['kodm'])->first();
      $return['product_id'] = $product->id ?? null;

      $return['branch_id'] = $branch_id;
      $return['type'] = 'sale';

      if (!empty($return['product_id']) && !empty($return['branch_id']) && !empty($return['type'])) {
        $data[] = $return;
      }
    }
    $this->data = $data;
    return true;
  }
}
