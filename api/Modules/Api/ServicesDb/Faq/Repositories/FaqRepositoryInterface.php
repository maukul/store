<?php

namespace Modules\Api\ServicesDb\Faq\Repositories;

use Illuminate\Support\Collection;

/**
 * Interface FaqRepositoryInterface
 * @package Modules\Api\ServicesDb\Faq\Repositories
 */
interface FaqRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'sort' => string
   * ]
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $params, array $columns = ['*']): Collection;
}
