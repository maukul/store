<?php

namespace Modules\Api\Http\Resources\Brand;

use App\Models\Brand\Brand;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BrandResource extends JsonResource
{
  /**
   * @var bool
   */
  public $preserveKeys = true;

  /**
   * @param  Request  $request
   * @return array
   */
  public function toArray($request): array
  {
    /* @var $brand Brand */
    $brand = $this->resource;

    return [
      'id' => $brand->id,
      'name' => $brand->name,
      'slug' => '', // TODO brand slug
      'images' => [
        'url_thumb' => '' // TODO brand image
      ],
    ];
  }
}
