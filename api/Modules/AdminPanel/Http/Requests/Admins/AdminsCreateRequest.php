<?php

namespace Modules\AdminPanel\Http\Requests\Admins;

use Illuminate\Validation\Rule;
use Modules\AdminPanel\Http\Requests\FormRequest;

class AdminsCreateRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize(): bool
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array
  {
    return [
      'email' => [
        'required',
        'email',
        'min:2',
        'max:191',
        Rule::unique('admins', 'email'),
      ],
      'password' => [
        'required',
        'string',
        'min:2',
        'max:191',
      ],
      'name' => [
        'required',
        'string',
        'min:2',
        'max:191',
      ],
    ];
  }

  /**
   * Get custom messages for validator errors.
   *
   * @return array
   */
  public function messages(): array
  {
    return [];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes(): array
  {
    return [
      'name' => trans('adminpanel::admins.form.name.title'),
      'email' => trans('adminpanel::admins.form.email.title'),
      'password' => trans('adminpanel::admins.form.password.title'),
    ];
  }
}
