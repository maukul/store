<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesArticlesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('categories_articles', function (Blueprint $table) {
      // Таблиця категорий для статей блога
      $table->id();
      $table->integer('position');
      $table->timestamps();
      $table->softDeletes();
    });

    Schema::create('categories_articles_translations', function (Blueprint $table) {
      // Таблиця перекладів категорий для статей блога
      $table->increments('id');
      $table->bigInteger('category_id')->unsigned();
      $table->string('locale')->index();
      $table->string('name', 90)->comment('Поле назви');
      $table->string('title', 255)->comment('Поле тайтл');
      $table->string('keywords', 255)->nullable()->comment('Поле ключові слова');
      $table->string('description', 255)->nullable()->comment('Поле ключові слова');
      $table->text('content')->nullable()->comment('Поле контент');
      $table->string('slug', 90)->comment('Url категорії для для статей блога');

      $table->unique(['category_id', 'locale']);
      $table->foreign('category_id')->references('id')->on('categories_articles')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('categories_articles_translations');
    Schema::dropIfExists('categories_articles');
  }
}
