<?php

namespace Modules\Api\ServicesDb\Banner\Repositories;

use App\Models\Banner\Banner;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Modules\Api\ServicesDb\Banner\Repositories\BannerRepositoryInterface;

/**
 * Class EloquentBannerRepository.
 */
class EloquentBannerRepository implements BannerRepositoryInterface
{
  /**
   * @param  array|string[]  $columns
   * @return Collection
   */
  public function getAll(array $columns = ['*']): Collection
  {
    return Banner::get($columns);
  }
}
