<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Inertia\Response;
use Modules\Api\Http\Resources\Banner\BannerResource;
use Modules\Api\ServicesDb\Banner\BannerService;

class BannersController extends BaseController
{
  /**
   * @var BannerService
   */
  protected $banners;

  /**
   * BannersController constructor.
   * @param  BannerService  $banners
   */
  public function __construct(
    BannerService $banners
  ) {
    parent::__construct();
    $this->banners = $banners;
  }

  /**
   * @return AnonymousResourceCollection
   */
  public function index(): AnonymousResourceCollection
  {
    $bannersData = $this->banners->getAll();
    return BannerResource::collection($bannersData);
  }
}
