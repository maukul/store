<?php

namespace Modules\Api\ServicesDb\Product\Repositories;

use App\Models\Product\Product;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App;
use Modules\Api\ServicesDb\Product\Repositories\ProductRepositoryInterface;

/**
 * Class EloquentProductRepository.
 */
class EloquentProductRepository implements ProductRepositoryInterface
{
  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'id' => int,
   *          'branch' => int,
   *          'types' => array,
   *          'name' => string,
   *      ],
   * ]
   * @return LengthAwarePaginator
   */
  public function search(array $params = []): LengthAwarePaginator
  {
    $modelProduct = Product::query();

    $filters = $params['filters'] ?? [];
    if (!empty($filters)) {
      if (!empty($filters['branch'])) {
        $modelProduct->whereHas('branchLeftovers', function ($q) use ($filters) {
          $q->where('branch_id', '=', $filters['branch'])
            ->where('total_remainder', '>', 0);
        });
      }
      if (!empty($filters['brands'])) {
        $modelProduct->whereIn('brand_id', $filters['brands']);
      }
      if (!empty($filters['types'])) {
        foreach ($filters['types'] as $type) {
          if (in_array($type, ['top', 'season', 'new', 'again', 'sale']) && !empty($filters['branch'])) {
            $modelProduct->whereHas('types', function ($q) use ($filters, $type) {
              $q->where('branch_id', '=', $filters['branch'])
                ->where('type', '=', $type);
            });
          }
        }
      }
      if (!empty($filters['id'])) {
        $inId = $filters['id'];
        if (!is_array($filters['id'])) {
          $inId = [$filters['id']];
        }
        $modelProduct->whereIn('id', $inId);
      }
    }

    $search = $params['search'] ?? null;
    if (!empty($search)) {
      $modelProduct->whereTranslationIlike('name', '%'.Str::lower($search).'%', App::getLocale());
    }

    $modelProduct->where('closed', '=', false);

    return $modelProduct->paginate(16);
  }

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'id' => int,
   *          'branch' => int,
   *          'types' => array,
   *          'name' => string,
   *      ],
   * ]
   * @return array
   */
  public function count(array $params = []): array
  {

    $countColumns = [
      'products_count' => 'products'
    ];
    $modelProduct = Product::query();
    $modelProduct->select([
      DB::raw('count(distinct products.id) as products_count'),
    ]);

    $filters = $params['filters'] ?? [];
    if (!empty($filters)) {
      if (!empty($filters['branch'])) {
        $modelProduct->whereHas('branchLeftovers', function ($q) use ($filters) {
          $q->where('branch_id', '=', $filters['branch'])
            ->where('total_remainder', '>', 0);
        });
      }
      if (!empty($filters['brands'])) {
        $modelProduct->whereIn('brand_id', $filters['brands']);
        foreach ($filters['brands'] as $brand) {
          $modelProduct->addSelect(DB::raw('SUM(CASE WHEN brand_id = '.$brand.' THEN 1 ELSE 0 END) AS brand_'.$brand));
          $countColumns['brand_'.$brand] = 'brand_'.$brand;
        }
      }
      if (!empty($filters['types'])) {
        $modelProduct->leftJoin('branches_in_products_types', 'products.id', '=',
          'branches_in_products_types.product_id');
        $modelProduct->where('branches_in_products_types.branch_id', '=', $filters['branch']);
        $modelProduct->whereIn('branches_in_products_types.type', $filters['types']);
        foreach ($filters['types'] as $type) {
          if (in_array($type, ['top', 'season', 'new', 'again', 'sale']) && !empty($filters['branch'])) {
            $modelProduct->addSelect(DB::raw('SUM(CASE WHEN branches_in_products_types.branch_id = '.$filters['branch'].' and branches_in_products_types.type =  \''.$type.'\' THEN 1 ELSE 0 END) AS '.$type.' '));
            $countColumns[$type] = 'type_'.$type;
          }
        }
      }
    }
    $search = $params['search'] ?? null;
    if (!empty($search)) {
      $modelProduct->whereTranslationIlike('name', '%'.Str::lower($search).'%');
    }
    $modelProduct->where('closed', '=', false);
    $count = $modelProduct->first()->toArray();

    $result = [];
    foreach ($countColumns as $columnOriginal => $column) {
      $result[$column] = $count[$columnOriginal] ?? 0;
    }
    return $result;
  }

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'branch' => int,
   *          'slug' => string,
   *      ],
   * ]
   * @return Product
   */
  public function slug(array $params = []): Product
  {
    $modelProduct = Product::query();

    $filters = $params['filters'] ?? [];
    if (!empty($filters)) {
      if (!empty($filters['branch'])) {
        $modelProduct->whereHas('branchLeftovers', function ($q) use ($filters) {
          $q->where('branch_id', '=', $filters['branch'])
            ->where('total_remainder', '>', 0);
        });
      }
      if (!empty($filters['slug'])) {
        $modelProduct->whereTranslation('slug', $filters['slug'], App::getLocale());
      }
    }
    $modelProduct->where('closed', '=', false);

    return $modelProduct->first();
  }

  /**
   * @param  array  $params  = [
   *      'filters' => [
   *          'branch' => int,
   *          'types' => array,
   *      ],
   * ]
   * @return Collection
   */
  public function type(array $params = []): Collection
  {
    $modelProduct = Product::query();
    $modelProduct->select(['id', 'brand_id']);

    $filters = $params['filters'] ?? [];
    if (!empty($filters)) {
      if (!empty($filters['branch'])) {
        $modelProduct->whereHas('branchLeftovers', function ($q) use ($filters) {
          $q->where('branch_id', '=', $filters['branch'])
            ->where('total_remainder', '>', 0);
        });
      }
      if (!empty($filters['type'])) {
        $filterType = $filters['type'];
        if ($filterType == 'rand') {
          $modelProduct->inRandomOrder();
        }
        if (in_array($filterType, ['top', 'season', 'new', 'again', 'sale']) && !empty($filters['branch'])) {
          $modelProduct->whereHas('types', function ($q) use ($filters, $filterType) {
            $q->where('branch_id', '=', $filters['branch'])
              ->where('type', '=', $filterType);
          });

          $modelProduct->distinct(['brand_id']);
        }
      }
    }
    $modelProduct->where('closed', '=', false);
    $modelProduct->has('media');

    $limit = $params['limit'] ?? 10;
    $modelProduct->limit($limit);

    return $modelProduct->get();
  }
}
