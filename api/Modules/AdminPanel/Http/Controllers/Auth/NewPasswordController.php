<?php

namespace Modules\AdminPanel\Http\Controllers\Auth;

use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Inertia\Response as InertiaResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;
use Modules\AdminPanel\Emails\NewPasswordEmail;
use Modules\AdminPanel\Http\Controllers\BaseController;
use Modules\AdminPanel\Http\Requests\Auth\NewPasswordRequest;

class NewPasswordController extends BaseController
{
  /**
   * Display the password reset view.
   *
   * @return InertiaResponse
   */
  public function create(): InertiaResponse
  {
    $this->seo()->setTitle(trans('adminpanel::reset_password.title.general'));

    return Inertia::render('Auth/ResetPassword');
  }

  /**
   * Send email new password.
   *
   * @param  NewPasswordRequest  $request
   * @return RedirectResponse
   */
  public function store(NewPasswordRequest $request): RedirectResponse
  {
    $request->validate([
      'email' => 'required|email',
    ]);

    $password = Str::random(8);

    $status = false;

    $admin = Admin::where('email', '=', $request->email)->first();

    if ($admin) {
      $admin->update([
        'password' => Hash::make($password),
        'remember_token' => Str::random(60),
      ]);

      Mail::to([$admin->email])->send(new NewPasswordEmail($admin, $password));

      $status = true;
    }

    // If the password was successfully reset, we will redirect the user back to
    // the application's home authenticated view. If there is an error we can
    // redirect them back to where they came from with their error message.
    return $status
      ? redirect()->route('admin-panel.login')->with('message',
        trans('adminpanel::reset_password.message.success'))->with('type', 'success')
      : back()->withInput($request->only('email'))
        ->withErrors(['email' => __($status)]);
  }
}
