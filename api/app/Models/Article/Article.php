<?php

namespace App\Models\Article;

use App\Models\CategoriesArticles\CategoriesArticles;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * App\Models\Article\Article
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|CategoriesArticles[] $categories
 * @property-read int|null $categories_count
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|Article[] $recommend
 * @property-read int|null $recommend_count
 * @property-read \App\Models\Article\ArticleTranslation|null $translation
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Article\ArticleTranslation[] $translations
 * @property-read int|null $translations_count
 * @method static \Illuminate\Database\Eloquent\Builder|Article listsTranslations(string $translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article notTranslatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Article orWhereTranslation(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Article orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Article orderByTranslation(string $translationField, string $sortMethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|Article translated()
 * @method static \Illuminate\Database\Eloquent\Builder|Article translatedIn(?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereTranslation(string $translationField, $value, ?string $locale = null, string $method = 'whereHas', string $operator = '=')
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereTranslationLike(string $translationField, $value, ?string $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article withTranslation()
 * @mixin \Eloquent
 */
class Article extends Model implements TranslatableContract, HasMedia
{
  use Translatable,
    InteractsWithMedia;

  /**
   * The translated attributes that are mass assignable.
   *
   * @var array
   */
  public $translatedAttributes = [
    'name',
    'title',
    'keywords',
    'description',
    'content',
    'slug',
  ];
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'articles';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
  ];
  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
  ];
  /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */

  /**
   * Bootstrap the model
   *
   * @return void
   */
  protected static function boot(): void
  {
    parent::boot();
  }

  /**
   * @param  Media|null  $media
   * @throws \League\Flysystem\FileNotFoundException
   * @throws \Spatie\Image\Exceptions\InvalidManipulation
   */
  public function registerMediaConversions(Media $media = null): void
  {
    $this->addMediaConversion('thumb')
      ->fit(Manipulations::FIT_MAX, 420, 270);
    $this->addMediaConversion('gallery_main')
      ->fit(Manipulations::FIT_MAX, 645, 410);
    $this->addMediaConversion('gallery_thumb')
      ->fit(Manipulations::FIT_MAX, 130, 100);
  }

  /**
   * Get the route key for the model.
   *
   * @return string
   */
  public function getRouteKeyName(): string
  {
    return 'id';
  }

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */

  /**
   * @return BelongsToMany<CategoriesArticles>|CategoriesArticles
   */
  public function categories(): BelongsToMany
  {
    return $this->belongsToMany(CategoriesArticles::class, 'categories_in_articles', 'article_id', 'category_id');
  }

  /**
   * @return BelongsToMany<Article>|Article
   */
  public function recommend(): BelongsToMany
  {
    return $this->belongsToMany(Article::class, 'articles_in_recommended_articles', 'article_id',
      'recommended_article_id');
  }

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESSORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
}
