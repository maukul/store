<?php

namespace App\Services\MainDatabase\Tables;

use App\Services\MainDatabase\BaseMainDatabase;
use Illuminate\Support\Facades\Http;

/**
 * Class BranchTable
 * @package App\Services\MainDatabase\Tables
 */
final class BranchTable extends BaseMainDatabase
{
  /**
   * BranchTable constructor.
   */
  public function __construct()
  {
    $this->select();
  }

  /**
   * The function select
   *
   * @param  array  $params
   * @return bool
   */
  protected function select(array $params = []): bool
  {
    $response = Http::withBasicAuth(config('main_database.username'), config('main_database.password'))
      ->get(config('main_database.url').'statement=select subdiv, inet_shop, ukrname, runame, engname, ipn_1, ipn_2, ipn_3, ipn_4 from dc000084 where  priznak=104');

    $this->originalDate = $response->json()['d']['results'];
    $data = [];
    foreach ($response->json()['d']['results'] as $item) {
      $return = [];
      $return['out_id'] = $item['subdiv'];
      $return['use_shop'] = $item['inet_shop'];

      if (!empty($item['ukrname'])) {
        $return['translations']['uk']['name'] = $item['ukrname'];
      }
      if (!empty($item['runame'])) {
        $return['translations']['ru']['name'] = $item['runame'];
      }
      if (!empty($item['engname'])) {
        $return['translations']['en']['name'] = $item['engname'];
      }

      if (!empty($item['ipn_1'])) {
        $return['phones'][] = $item['ipn_1'];
      }
      if (!empty($item['ipn_2'])) {
        $return['phones'][] = $item['ipn_2'];
      }
      if (!empty($item['ipn_3'])) {
        $return['phones'][] = $item['ipn_3'];
      }
      if (!empty($item['ipn_4'])) {
        $return['phones'][] = $item['ipn_4'];
      }
      if (!empty($return['translations'])) {
        $data[] = $return;
      }
    }
    $this->data = $data;
    return true;
  }
}
