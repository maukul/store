<?php

namespace Modules\AdminPanel\Http\Requests\Pages;

use Modules\AdminPanel\Http\Requests\FormRequest;

class PagesEditRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize(): bool
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array
  {
    $rules = [
      'type' => [
        'required',
        'string',
      ],
      'name.*' => [
        'required',
        'string',
        'max:90',
      ],
      'title.*' => [
        'required',
        'string',
        'max:255',
      ],
      'keywords.*' => [
        'nullable',
        'string',
        'max:255',
      ],
      'description.*' => [
        'nullable',
        'string',
        'max:255',
      ],
    ];
    $rules = $this->rulesType($rules, request()->input('type'));

    return $rules;
  }

  /**
   * Get the validation rules type.
   *
   * @param  array  $rules
   * @param  string  $type
   * @return array
   */
  protected function rulesType(array $rules, string $type): array
  {

    if ($type == 'home') {
      $rules['value.description.*'] = [
        'nullable',
        'string',
        'max:150000',
      ];
    } elseif ($type == 'contact') {
      $rules['value.phones.*'] = [
        'required',
      ];
      $rules['value.phones.*.*'] = [
        'nullable',
        'string',
        'max:15',
      ];

      $rules['value.email.*'] = [
        'required',
      ];
      $rules['value.email.*.*'] = [
        'nullable',
        'string',
        'max:30',
      ];

      $rules['value.address.*'] = [
        'nullable',
        'string',
        'max:50',
      ];

      $rules['value.time_work.*.time'] = [
        'nullable',
        'string',
        'max:50',
      ];
      $rules['value.time_work.*.day'] = [
        'nullable',
        'string',
        'max:50',
      ];

      $rules['value.map.*'] = [
        'required',
      ];
      $rules['value.map.*.*'] = [
        'required',
        'numeric',
      ];
    } elseif ($type == 'faq') {
      $rules['value.description.*'] = [
        'nullable',
        'string',
        'max:150000',
      ];
    } elseif ($type == 'about') {
      $rules['value.description.*'] = [
        'nullable',
        'string',
        'max:150000',
      ];
      $rules['value.bloc-statistics.*.*'] = [
        'nullable',
        'string',
        'max:35',
      ];
    }

    return $rules;
  }

  /**
   * Get custom messages for validator errors.
   *
   * @return array
   */
  public function messages(): array
  {
    return [];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes(): array
  {
    $return = [];

    $return['type'] = trans('adminpanel::pages.form.type.title');

    foreach (config('translatable.locales') as $item) {
      $return['name.'.$item] = trans('adminpanel::pages.form.name.title').' ('.$item.')';
      $return['title.'.$item] = trans('adminpanel::pages.form.title.title').' ('.$item.')';
      $return['keywords.'.$item] = trans('adminpanel::pages.form.keywords.title').' ('.$item.')';
      $return['description.'.$item] = trans('adminpanel::pages.form.description.title').' ('.$item.')';

      $return['value.description.'.$item] = trans('adminpanel::pages.form.value.description.title').' ('.$item.')';
      $return['value.phones.'.$item] = trans('adminpanel::pages.form.value.phones.title').' ('.$item.')';
      $return['value.phones.'.$item.'.*'] = trans('adminpanel::pages.form.value.phones.title').' ('.$item.')';
      $return['value.email.'.$item] = trans('adminpanel::pages.form.value.email.title').' ('.$item.')';
      $return['value.email.'.$item.'.*'] = trans('adminpanel::pages.form.value.email.title').' ('.$item.')';
      $return['value.address.'.$item] = trans('adminpanel::pages.form.value.address.title').' ('.$item.')';
      $return['value.time_work.'.$item.'.time'] = trans('adminpanel::pages.form.value.time_work.time.title').' ('.$item.')';
      $return['value.time_work.'.$item.'.day'] = trans('adminpanel::pages.form.value.time_work.day.title').' ('.$item.')';
      $return['value.map.'.$item] = trans('adminpanel::pages.form.value.map.title').' ('.$item.')';
      $return['value.map.'.$item.'.*'] = trans('adminpanel::pages.form.value.map.title').' ('.$item.')';

      $return['value.bloc-statistics.'.$item.'.*'] = trans('adminpanel::pages.form.value.bloc-statistics.title').' ('.$item.')';
    }
    return $return;
  }
}
