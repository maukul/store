<?php

namespace Modules\AdminPanel\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Modules\AdminPanel\Http\Requests\Banners\BannersCreateRequest;
use Modules\AdminPanel\Http\Requests\Banners\BannersEditRequest;
use Inertia\Response as InertiaResponse;
use Illuminate\Http\RedirectResponse;
use Modules\AdminPanel\Http\Resources\Banner\BannerResource;
use Modules\AdminPanel\ServicesDb\Banner\BannerService;
use Route;

class BannersController extends BaseController
{
  /**
   * @var BannerService
   */
  protected $banners;

  /**
   * BannersController constructor.
   *
   * @param  BannerService  $banners
   */
  public function __construct(
    BannerService $banners
  ) {
    parent::__construct();
    $this->banners = $banners;

    $this->seo()->setTitle(trans('adminpanel::banners.title.'.(optional(Route::getCurrentRoute())->getActionMethod() ?? 'general')));
  }

  /**
   * Display a listing of the resource.
   *
   * @param  Request  $request
   * @return InertiaResponse
   */
  public function index(Request $request): InertiaResponse
  {
    $bannersData = $this->banners->search($request->all());
    $tableData = BannerResource::collection($bannersData);
    return Inertia::render('Banners/Index', [
      'tableData' => $tableData,
      'request' => $request->all(),
      'tableParams' => [
        'columnFilters' => $request->input('columnFilters', []),
        'sort' => $request->input('sort', ['field' => '', 'type' => '']),
        'page' => $tableData->resource->currentPage() ?? 1,
        'perPage' => $tableData->resource->perPage() ?? 10,
      ],
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return InertiaResponse
   */
  public function create(): InertiaResponse
  {
    return Inertia::render('Banners/Create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  BannersCreateRequest  $request
   * @return RedirectResponse
   */
  public function store(BannersCreateRequest $request): RedirectResponse
  {
    $model = $this->banners->create($request->getFormData());
    return redirect()->route('admin-panel.banners.edit', $model->id)
      ->with('message', trans('adminpanel::banners.message.success.store'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param $id
   * @return InertiaResponse
   */
  public function edit($id): InertiaResponse
  {
    $banner = $this->banners->getById((int) $id);
    $bannerData = new BannerResource($banner);
    return Inertia::render('Banners/Edit', [
      'data' => $bannerData,
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  BannersEditRequest  $request
   * @param $id
   * @return RedirectResponse
   */
  public function update(BannersEditRequest $request, $id): RedirectResponse
  {
    $this->banners->update((int) $id, $request->getFormData());
    return redirect()->back()->with('message', trans('adminpanel::banners.message.success.update'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param $id
   * @return RedirectResponse
   */
  public function destroy($id): RedirectResponse
  {
    $this->banners->deleteById((int) $id);
    return redirect()->route('admin-panel.banners.index')
      ->with('message', trans('adminpanel::banners.message.success.destroy'));
  }
}
